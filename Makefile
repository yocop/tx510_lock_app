CPRE := @
ifeq ($(V),1)
CPRE :=
VERB := --verbose
endif

MK_GENERATED_IMGS_PATH:=generated
PRODUCT_BIN:=product

.PHONY:startup
startup: all

all:
	@echo "Build Solution by $(BOARD) "
	$(CPRE) scons $(VERB) --board=$(BOARD) -j4
	@echo YoC SDK Done

	@echo [INFO] Create bin files
	$(CPRE) csky-abiv2-elf-objcopy -O binary yoc.elf yoc.bin

	# $(CPRE) ./script/csky_pack_linux/Image -i ALGO_V1.4_D01W13_210202.elf -c script/csky_pack_linux/prim.ini -o generated/data/prim
	# $(CPRE) rm ck805_ddr.bin ck805_dtcm.bin
	$(CPRE) $(PRODUCT_BIN) image $(MK_GENERATED_IMGS_PATH)/images.zip -i $(MK_GENERATED_IMGS_PATH)/data -l -p
	$(CPRE) $(PRODUCT_BIN) image $(MK_GENERATED_IMGS_PATH)/images.zip -e $(MK_GENERATED_IMGS_PATH) -x -tb

.PHONY:flash
flash:
	$(CPRE) scons --flash=prim --board=$(BOARD) --sdk=$(SDK)

.PHONY:fatfs
fatfs:
	$(CPRE) scons --flash=fatfs --board=$(BOARD) --sdk=$(SDK)

.PHONY:flashboot
flashboot:
	$(CPRE) scons --flash=boot --board=$(BOARD) --sdk=$(SDK)
	$(CPRE) scons --flash=bparam --board=$(BOARD) --sdk=$(SDK)

.PHONY:flashall
flashall:
	$(CPRE) scons --flash=all --board=$(BOARD) --sdk=$(SDK)

sdk:
	$(CPRE) yoc sdk

.PHONY:clean
clean:
	$(CPRE) scons -c
	$(CPRE) find . -name "*.[od]" -delete
	$(CPRE) rm yoc_sdk yoc.* generated out -rf
