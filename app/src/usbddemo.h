/*
 * Copyright (C) 2017-2019 Alibaba Group Holding Limited
 */

/******************************************************************************
 * @file     usbddemo.h
 * @brief    usb example
 * @version  V1.0
 * @date     23. April 2019
 ******************************************************************************/

#ifndef _USBDDEMO_H_
#define _USBDDEMO_H_
#define SMXUSBD
#define SMXUSBD_DEMO
#define SMXUSBD_ON 1

#define USB_UVC_MODULE        "usb_uvc_task"
#define USB_UVC_TASK_PRI      31


typedef enum  USB_IMAGE_TYPE_
{
	USB_IMAGE_TYPE_NONE = 0,
    USB_IMAGE_TYPE_RGB,
    USB_IMAGE_TYPE_IR,
    USB_IMAGE_TYPE_DEPTH,
}USB_IMAGE_TYPE;
	
typedef enum _IMAGE_SOURCE_TYPE{	
	RGB =1,
	IR,
	DEPTH,
	ABGR
}IMAGE_SOURCE_TYPE_E;

typedef struct _IMAGE_SOURCE{
	enum _IMAGE_SOURCE_TYPE 	type;
}__attribute__ ((packed)) _IMAGE_SOURCE_S;

extern void VideoDo(void);
extern int USBDDemo(void);
extern int USBDDemoStart(void);
extern int USBDDemoStop(void);
void usb_uvc_init(void);
void usb_uvc_task(void *paras);
extern void vi_isp1_switch2ir_mode();
extern void vi_isp1_switch2spk2_mode();
extern void vi_isp1_switch2ir_mode();

#endif
