/*
 * Copyright (C) 2019 C-SKY Microsystems Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************
 * @file     camera_ov5647.h
 * @brief    head file for camera_ov5647
 * @version  V1.0
 * @date     13. february 2019
 ******************************************************************************/
#ifndef _CAMERA_OV5647_H_
#define _CAMERA_OV5647_H_

#include "stdio.h"
//#include <string.h>
//// #include <csi_config.h>
//#include "soc.h"
//#include "pin.h"
#include "sensor_common.h"


//#define OV5647_REG_CHIPID_H 0x300A
//#define OV5647_REG_CHIPID_L 0x300B




//define the voltage level of control signal
//#define CSI_STBY_ON     1
//#define CSI_STBY_OFF    0
//#define CSI_RST_ON      0
//#define CSI_RST_OFF     1
//#define CSI_PWR_ON      1
//#define CSI_PWR_OFF     0
//#define CSI_AF_PWR_ON   1
//#define CSI_AF_PWR_OFF  0
//
//#define OV5647_WINDOW_HEIGHT        0x03
//#define OV5647_WINDOW_HEIGHT_MIN    2
//#define OV5647_WINDOW_HEIGHT_MAX    800     //2006
//#define OV5647_WINDOW_HEIGHT_DEF    800     //1944
//#define OV5647_WINDOW_WIDTH         0x04
//#define OV5647_WINDOW_WIDTH_MIN     2
//#define OV5647_WINDOW_WIDTH_MAX     1280    //2752
//#define OV5647_WINDOW_WIDTH_DEF     1280    //2592

enum power_seq_cmd{
    CSI_SUBDEV_PWR_OFF = 0x00,
    CSI_SUBDEV_PWR_ON  = 0x01,
};

typedef enum isp1_buffer_id_e
{
    ISP1_BUFFER_GROUP_0       = 0,
    ISP1_BUFFER_GROUP_1       = 1,
    ISP1_BUFFER_GROUP_NUM     = 2,
} isp1_buffer_id_t;

//
//const struct regval_list sensor_oe_disable_regs[3] = {
//    {0x3000,0x00},
//    {0x3001,0x00},
//    {0x3002,0x00},
//};
//
//const struct regval_list sensor_oe_enable_regs[3] = {
//    {0x3000,0x0f},
//    {0x3001,0xff},
//    {0x3002,0xe4},
//};





#endif
