#ifndef QUEUE_H
#define QUEUE_H

typedef struct _Link
{
	unsigned char *buf;
	int 	len;	//this node valid size
	int 	dirty;//dirty=1 buffer is full. dirty=0 buffer is empty.
}LinkNode;

typedef struct _LinkQueue
{
	unsigned int node_num_;
	unsigned int link_size_;
    int front;
    int rear;
    int count;
	char *headptr;
	LinkNode *link;
}LinkQueue;


int QueueInit(LinkQueue *Q, int node_num, int link_size);
int QueueFull(LinkQueue *Q);
int QueueEmpty(LinkQueue *Q);
LinkNode* GetEmptyNode(LinkQueue *Q);
LinkNode* GetFullNode(LinkQueue *Q);
int QueueSendFull(LinkQueue *Q, LinkNode *Node, int len);
int QueueSendEmpty(LinkQueue *Q, LinkNode *Node, int len);
void QueueDestroy(LinkQueue *Q);




#endif
