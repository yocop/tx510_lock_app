#ifndef PUSHBUTTON_H
#define PUSHBUTTON_H

#ifdef __cplusplus
extern "C"
{
#endif

int check_intercom_button_status(void);

#ifdef __cplusplus
}
#endif

#endif