#ifndef AUDIO_PROCESS_H
#define AUDIO_PROCESS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef enum {
    CODING_TYPE_RAW = 1,
    CODING_TYPE_G711A,
    CODING_TYPE_G711U,
} audio_coding_type_e;

int app_audio_init(void);
int app_audio_record_start(void);
int app_audio_record_stop(void);
int app_audio_record_get(uint8_t *data, int size, uint32_t *time_stamp);
int app_audio_play(uint8_t *g711_data, int size, int sample_rate);

int app_audio_record_uint_test(void);


#ifdef __cplusplus
}
#endif

#endif