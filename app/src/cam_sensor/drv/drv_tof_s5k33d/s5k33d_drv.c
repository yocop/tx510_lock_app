#include <stdio.h>
#include <ulog/ulog.h>
#include "iic_function.h"
#include "camera_config.h"
#include "drv_isp.h"
#include "soc.h"
#include "pin_name.h"
#include "s5k33d_drv.h"
#include "s5k33d_config.h"
#include "drv_gpio.h"

static i2c_dev_t i2c_s5k33d;

extern gpio_pin_handle_t gpio_pin_handle;

static int s5k33d_check_id(i2c_dev_t *i2c)
{
    int ret = 0;
    uint8_t data[2] = {0};

	ret = iic_read16bit(i2c,  S5K33D_ADDR_CHIPID, data);//
	printf("s5k33d: chipID = 0x%02x, 0x%02x \n", data[0], data[1]);
	
   	//if (ret < 0 || (data[0] != 0x96) || (data[1] != 0xFD))
    //    return -1;

    return ret;
}

int s5k33d_hw_reset(int bitn)
{
	// volatile uint32_t *base;
	// base = (uint32_t *)(CSKY_AOGPIO_BASE + 0x04); //gpio dicrection
	// *base |= (0x1 << bitn);
	// base = (uint32_t *)(CSKY_AOGPIO_BASE + 0x00); //output
	// *base |= (0x0 << bitn);
	// aos_msleep(10);
	// *base |= (0x1 << bitn);

    return 0;
}

int s5k33d_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
   	memcpy(&i2c_s5k33d, i2c, sizeof(i2c_dev_t));
    i2c_s5k33d.config.dev_addr = dev_addr;
    //Check the id of camera
   	int ret = s5k33d_check_id(&i2c_s5k33d);
    if (ret < 0) {
        printf("s5k33d check id fail\n");
        return ret;
    }
	
    return ret;
}

int32_t s5k33d_mode_config(s5k33d_mode_e mode)
{
	int ret = 0;
	//printf("%s >>>>  %d, mode %d \n",__func__, __LINE__, mode);

	if (S5K33D_STANDBY == mode) {
		ret = iic_write16bit_array(&i2c_s5k33d, (struct regval_list *)s5k33d_standby_reglist, ARRAY_SIZE(s5k33d_standby_reglist));
		if (ret < 0) {
			return -1;
		}
	} 
	else if(S5K33D_STREAM == mode)
	{
		ret = iic_write16bit_array(&i2c_s5k33d, (struct regval_list *)s5k33d_stream_reglist, ARRAY_SIZE(s5k33d_stream_reglist));
		if (ret < 0) {
			return -1;
		}
	}
	else {}


	return 0;
}

int32_t s5k33d_config(imageFormat_t cfg)
{
	int32_t ret = 0;

	if( R_1280x962 == cfg.res && RAW10 == cfg.format)
	{
		ret = iic_write16bit_array(&i2c_s5k33d, (struct regval_list *)s5k33d_config_reglist, ARRAY_SIZE(s5k33d_config_reglist));
		if (ret < 0) {
			return -1;
		}

		
#if 0
		
//		ret = iic_write16bit_array(&i2c_s5k33d, (struct regval_list *)s5k33d_640x241x5_stream, ARRAY_SIZE(s5k33d_640x241x5_stream));
//		if (ret < 0) {
//			return -1;
//		}

//		uint32_t num0 = 0;
//		uint32_t size0 = ARRAY_SIZE(s5k33d_640x241x5_stream);
//		uint8_t reg_dat0[2] = {0};
//		
//		while(num0 < size0)
//		{
//			iic_read16bit(&i2c_s5k33d, s5k33d_640x241x5_stream[num0].addr, reg_dat0);
//			printf("s5k33d addr = 0x%04x, d[0] = 0x%02x, d[1] = 0x%02x, \n", s5k33d_640x241x5_stream[num0].addr, reg_dat0[0], reg_dat0[1]);
//			num0++;
//		}

		uint32_t num = 0;
		uint32_t size = ARRAY_SIZE(s5k33d_config_reglist);
		uint8_t reg_dat[2] = {0};
		
		while(num < size)
		{
			iic_read16bit(&i2c_s5k33d, s5k33d_config_reglist[num].addr, reg_dat);
			printf("s5k33d addr = 0x%04x, d[0] = 0x%02x, d[1] = 0x%02x, \n", s5k33d_config_reglist[num].addr, reg_dat[0], reg_dat[1]);
			num++;
		}

		//while(1) {aos_msleep(500);}
#endif
		
		printf("s5k33d reg init done. \n");
	}

	return ret;
}



