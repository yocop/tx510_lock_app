#ifndef _S5K33D_DRV_H_
#define _S5K33D_DRV_H_
#include <stdio.h>
#include "pin_name.h"
#include "iic_function.h"


#define S5K33D_ADDR_CHIPID		0x0
#define S5K33D_DATA_CHIPID		0x303d

typedef enum
{
    S5K33D_STANDBY = 0,
    S5K33D_STREAM  = 1,
}s5k33d_mode_e;



int s5k33d_hw_reset(int bitn);
int s5k33d_init(i2c_dev_t *i2c, uint32_t dev_addr);
int32_t s5k33d_mode_config(s5k33d_mode_e mode);
int32_t s5k33d_config(imageFormat_t cfg);



#endif
