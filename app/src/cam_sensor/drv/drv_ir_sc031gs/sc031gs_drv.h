#ifndef _IRS2877C_DRV_H_
#define _IRS2877C_DRV_H_
#include <stdio.h>
#include "pin_name.h"
#include "iic_function.h"
#include "sensor_common.h"


#define SC031GS_ADDR_CHIPID_H		0x3107
#define SC031GS_ADDR_CHIPID_L		0x3108
#define SC031GS_DATA_CHIPID_H		0x00
#define SC031GS_DATA_CHIPID_L		0x31

#define SC031GS_STROBE              (0x3361)

int sc031gs_hw_reset(void);
int sc031gs_init(i2c_dev_t *i2c, uint32_t dev_addr);
int sc031gs_left_init(i2c_dev_t *i2c, uint32_t dev_addr);
int32_t sc031gs_mode_config(sensor_mode_t mode);
int sc031gs_left_mode_config(sensor_mode_t mode);
int32_t sc031gs_config(imageFormat_t cfg);
int sc031gs_debug_test(void);

#endif
