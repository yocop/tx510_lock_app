#ifndef _IRS2877C_CONFIG_DRV_H_
#define _IRS2877C_CONFIG_DRV_H_
#include <stdio.h>
#include "camera_config.h"

const regval_list sc031gs_640x480_raw8[] = {
	//SC031GS_MIPI_24M
	//640x480
	//1-Lane
	//8-bit
	//720Mbps
	//30fps

	{0x0103,0x01},
	{0x0100,0x00},
	{0x36e9,0x80},
	{0x36f9,0x80},
	{0x3001,0x00},
	{0x3000,0x00},
	{0x300f,0x0f},
	{0x3018,0x13},
	{0x3019,0xfe},
	{0x301c,0x78},
	{0x301f,0x07},
	{0x3031,0x08},
	{0x3037,0x00},
	{0x303f,0x01},
	{0x320c,0x03},
	{0x320d,0xe8},
	{0x320e,0x0b},
	{0x320f,0xb8},
	{0x3220,0x10},
	{0x3250,0xc0},
	{0x3251,0x02},
	{0x3252,0x0b},
	{0x3253,0xb3},
	{0x3254,0x02},
	{0x3255,0x07},
	{0x3304,0x48},
	{0x3306,0x38},
	{0x3309,0x68},
	{0x330b,0xe0},
	{0x330c,0x18},
	{0x330f,0x20},
	{0x3310,0x10},
	{0x3314,0x1e},
	{0x3315,0x38},
	{0x3316,0x40},
	{0x3317,0x10},
	{0x3329,0x34},
	{0x332d,0x34},
	{0x332f,0x38},
	{0x3335,0x3c},
	{0x3344,0x3c},
	{0x335b,0x80},
	{0x335f,0x80},
	{0x3366,0x06},
	{0x3385,0x31},
	{0x3387,0x51},
	{0x3389,0x01},
	{0x33b1,0x03},
	{0x33b2,0x06},
	{0x3621,0xa4},
	{0x3622,0x05},
	{0x3624,0x47},
	{0x3630,0x46},
	{0x3631,0x48},
	{0x3633,0x52},
	{0x3635,0x18},
	{0x3636,0x25},
	{0x3637,0x89},
	{0x3638,0x0f},
	{0x3639,0x08},
	{0x363a,0x00},
	{0x363b,0x48},
	{0x363c,0x06},
	{0x363d,0x00},
	{0x363e,0xf8},
	{0x3640,0x00},
	{0x3641,0x01},
	{0x36ea,0x3b},
	{0x36eb,0x0d},
	{0x36ec,0x0d},
	{0x36ed,0x33},
	{0x36fa,0x3b},
	{0x36fb,0x00},
	{0x36fc,0x03},
	{0x36fd,0x03},
	{0x3908,0x91},
	{0x3d08,0x01},
	{0x3e01,0x14},
	{0x3e02,0x80},
	{0x3e06,0x0c},
	{0x4500,0x59},
	{0x4501,0xc4},
	{0x4603,0x00},
	{0x4809,0x01},
	{0x4837,0x16},
	{0x5011,0x00},
	{0x36e9,0x00},
	{0x36f9,0x00},
	{0x0100,0x01},
	{0x4418,0x08},
	{0x4419,0x8e},

	//[gain<2]   
	{0x3314,0x1e},
	{0x3317,0x10},

	//[4>gain>=2]
	{0x3314,0x4f}, 
	{0x3317,0x0f}, 

	//[gain>=4]  
	{0x3314,0x4f}, 
	{0x3317,0x0f}, 
};

const regval_list sc031gs_640x480_raw10[] = {
	{0x0100,0x00},
	{0x3000,0x00},
	{0x3001,0x00},
	{0x300f,0x0f},
	{0x3018,0x13},
	{0x3019,0xfe},
	{0x301c,0x78},
	{0x3031,0x0a},
	{0x3037,0x20},
	{0x303f,0x01},
	{0x320c,0x03},
	{0x320d,0x6e},

	//120fps
	//{0x320e,0x02},
	//{0x320f,0xab},
	//{0x3252,0x02},
	//{0x3253,0xa6},

	//30fps
	{0x320e,0x0a},
	{0x320f,0xac},
	{0x3252,0x0a},
	{0x3253,0xa7},

	{0x3220,0x10},
	{0x3250,0xc0},
	{0x3251,0x02},
	{0x3254,0x02},
	{0x3255,0x07},
	{0x3304,0x48},
	{0x3306,0x38},
	{0x3309,0x68},
	{0x330b,0xe0},
	{0x330c,0x18},
	{0x330f,0x20},
	{0x3310,0x10},
	{0x3314,0x1e},
	{0x3315,0x38},
	{0x3316,0x40},
	{0x3317,0x10},
	{0x3329,0x34},
	{0x332d,0x34},
	{0x332f,0x38},
	{0x3335,0x3c},
	{0x3344,0x3c},
	{0x335b,0x80},
	{0x335f,0x80},
	{0x3366,0x06},
	{0x3385,0x31},
	{0x3387,0x51},
	{0x3389,0x01},
	{0x33b1,0x03},
	{0x33b2,0x06},
	{0x3621,0xa4},
	{0x3622,0x05},
	{0x3624,0x47},
	{0x3630,0x46},
	{0x3631,0x48},
	{0x3633,0x52},
	{0x3635,0x18},
	{0x3636,0x25},
	{0x3637,0x89},
	{0x3638,0x0f},
	{0x3639,0x08},
	{0x363a,0x00},
	{0x363b,0x48},
	{0x363c,0x06},
	{0x363d,0x00},
	{0x363e,0xf8},
	{0x3640,0x00},
	{0x3641,0x01},
	{0x36e9,0x00},
	{0x36ea,0x3b},
	{0x36eb,0x0e},
	{0x36ec,0x0e},
	{0x36ed,0x33},
	{0x36f9,0x00},
	{0x36fa,0x3a},
	{0x36fc,0x01},
	{0x3908,0x91},
	{0x3d08,0x01},

	//exp=0x148*12.19us
	{0x3e01,0x14},
	{0x3e02,0x80},
	{0x3e06,0x0c},
	{0x4500,0x59},
	{0x4501,0xc4},
	{0x4603,0x00},
	{0x4809,0x01},
	{0x4837,0x1b},
	{0x5011,0x00},
	{0x0100,0x01},

	//delay 10ms
	{0x4418,0x08},
	{0x4419,0x8e}, //20190222 cover avdd 2.6V-3.0V
};


const  regval_list sc031gs_sw_standby[] = {
	{0x0100 , 0x00},
};

const  regval_list sc031gs_streaming[] = {
	{0x0100 , 0x01},
};

const  regval_list sc031gs_debug_ae[] = {
	{0x3e01 , 0x14},
	{0x3e02 , 0x80},
	// {0x3e08 , 0x03},
	// {0x3e06 , 0x0c},
	{0x4419 , 0x80},
};

const  regval_list sc031gs_debug[] = {
	{0x3e01 , 0x01},
	{0x3e02 , 0x01},
	{0x3e06 , 0x01},
	{0x3e07 , 0x01},
	{0x3e08 , 0x01},
	{0x3e09 , 0x01},

};

#endif
