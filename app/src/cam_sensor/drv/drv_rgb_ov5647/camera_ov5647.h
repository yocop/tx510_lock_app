/*
 * Copyright (C) 2017-2019 Alibaba Group Holding Limited
 */

/******************************************************************************
 * @file     camera_ov5647.h
 * @brief    head file for camera_ov5647
 * @version  V2.0
 * @date     10. Nov 2019
 ******************************************************************************/

#ifndef _CAMERA_OV5647_H_
#define _CAMERA_OV5647_H_

#include <stdio.h>
#include <string.h>
// #include <csi_config.h>
#include <soc.h>
#include <pin.h>
#include "camera_ov_common.h"

#define OV5647_WINDOW_HEIGHT_MAX 1944
#define OV5647_WINDOW_HEIGHT_DEF 480
#define OV5647_WINDOW_WIDTH_MAX  2592
#define OV5647_WINDOW_WIDTH_DEF  640

#define OV5647_CHIPID_HIGH       0x56
#define OV5647_CHIPID_LOW        0x47
#define OV5647_IIC_SLAVE_ADDR    0x36

const struct regval_list ov5647_640x480[] = {
    {0x3034, 0x08},//SC_CMMN_PLL_CTRL0 d:0x1A
    {0x3035, 0x21},//SC_CMMN_PLL_CTRL0 d:0x11
    {0x3036, 0x46},//SC_CMMN_PLL_MULTIPLIER d:0x69
    {0x303c, 0x11},//SC_CMMN_PLLS_CTRL2 d:0x11
    {0x3106, 0xf5},//SRB CTRL d:0xf9 pll/4->pll/2
    {0x3821, 0x07},//TIMING_TC_REG21 d:0x00  open r_hbin,r_mirror_snr, r_mirror_isp
    {0x3820, 0x41},//TIMING_TC_REG20 d:0x40  open r_vbin
    {0x3827, 0xec},//no log in doc
    {0x370c, 0x0f},//no log in doc
    {0x3612, 0x59},//no log in doc
    {0x3618, 0x00},//no log in doc
    {0x5000, 0x06},//ISP CTRL00 d:0xff  disable lenc_en
    {0x5001, 0x01},//ISP CTRL01 d:0x01
    {0x5002, 0x41},//ISP CTRL02 d:0x41
    {0x5003, 0x08},//ISP CTRL03 d:0x0A  disable bin_auto_en
    {0x5a00, 0x08},//DIGC CTRL0 d:0x00  nothing happen bit[3] not used
    {0x3000, 0x00},//SC_CMMN_PAD_OEN0 d:0x00
    {0x3001, 0x00},//SC_CMMN_PAD_OEN1 d:0x00
    {0x3002, 0x00},//SC_CMMN_PAD_OEN2 d:0x00
    {0x3016, 0x08},//SC_CMMN_MIPI_PHY d:0x00 mipi_pad_enable
    {0x3017, 0xe0},//SC_CMMN_MIPI_PHY d:0x10
    {0x3018, 0x44},//SC_CMMN_MIPI_SC_CTRL d:0x58 set to TWO lane mode
    {0x301c, 0xf8},//no log in doc
    {0x301d, 0xf0},//no log in doc
    {0x3a18, 0x00},//AEC GAIN CEILING d:0x0
    {0x3a19, 0xf8},//ARC GAIN CEILING d:7c nothing happen
    {0x3c01, 0x80},//50/60 HZ DETECTION CTRL01 d:0x0 manual mode enable
    {0x3b07, 0x0c},//STROBE FREX_MODE_SEL d:0x08 open frex_inv
    {0x380c, 0x07},//TIMING_HTS  d:0xA Total horizontal size[12:8]
    {0x380d, 0x68},//TIMING_HTS  d:0x8C Total horizontal size[7:0] 1896
    {0x380e, 0x03},//TIMING_VTS  d:0x07 Total vertical size[9:8]
    {0x380f, 0xd8},//TIMING_VTS  d:0x98 Total vertical size[7:0] 984
    {0x3814, 0x31},//TIMING_X_INC  d:0x11 change Horizontal subsample odd increase number 1->3
    {0x3815, 0x31},//TIMING_Y_INC  d:0x11 change Vertical subsample odd increase number 1->3
    {0x3708, 0x64},//no log in doc
    {0x3709, 0x52},//no log in doc
};
const struct regval_list ov5647_640x480_2[] = {
    {0x3801, 0x00},//TIMING_X_ADDR_START d:0xC [7:0] -> x_start is 0
    {0x3802, 0x00},//TIMING_Y_ADDR_START d:0x0 [11:8]
    {0x3803, 0x00},//TIMING_Y_ADDR_START d:0x4 [7:0] -> y_start is 0
    {0x3804, 0x0a},//TIMING_X_ADDR_END d:0xA [11:8]
    {0x3805, 0x3f},//TIMING_X_ADDR_END d:0x33 [7:0] -> x_end is 2623
    {0x3806, 0x07},//TIMING_Y_ADDR_END d:0x7 [11:8]
    {0x3807, 0xa1},//TIMING_Y_ADDR_END d:0xA3 [7:0] -> y_end is 1953
    {0x3811, 0x08},//TIMING_ISP_X_WIN d:0x04 ISP horizontal offset[7:0]->8
    {0x3813, 0x02},//TIMING_ISP_Y_WIN d:0x02 ISP vertical offset[7:0]->2
    {0x3630, 0x2e},//no log in doc
    {0x3632, 0xe2},//no log in doc
    {0x3633, 0x23},//no log in doc
    {0x3634, 0x44},//no log in doc
    {0x3636, 0x06},//no log in doc
    {0x3620, 0x64},//no log in doc
    {0x3621, 0xe0},//no log in doc
    {0x3600, 0x37},//no log in doc
    {0x3704, 0xa0},//no log in doc
    {0x3703, 0x5a},//no log in doc
    {0x3715, 0x78},//no log in doc
    {0x3717, 0x01},//no log in doc
    {0x3731, 0x02},//no log in doc
    {0x370b, 0x60},//no log in doc
    {0x3705, 0x1a},//no log in doc
    {0x3f05, 0x02},//no log in doc
    {0x3f06, 0x10},//no log in doc
    {0x3f01, 0x0a},//no log in doc
    {0x3a08, 0x01},//B50 STEP d:0x01
    {0x3a09, 0x27},//B50 STEP d:0x27
    {0x3a0a, 0x00},//B60 STEP
    {0x3a0b, 0xf6},//B60 STEP
    {0x3a0d, 0x04},//B60 MAX d:0x8 -> 0x4
    {0x3a0e, 0x03},//B50 MAX d:0x6 -> 0x3
    {0x3a0f, 0x58},//WPT d:0x78 -> WPT stable range high limit(enter)
    {0x3a10, 0x50},//BPT d:0x68 -> BPT stable range low limit(enter)
    {0x3a1b, 0x58},//WPT2 d:78 -> WPT stable range high limit(go out)
    {0x3a1e, 0x50},//BPT d:0x68 -> BPT stable range low limit(go out)
    {0x3a11, 0x60},//HIGH VPT d:0xD0 -> vpt_high
    {0x3a1f, 0x28},//LOW VPT d:0x40 -> vpt_low Step manual mode, fast zone low limit
    {0x4001, 0x02},//BLC CTRL01 d:0x00 start_line is 2
    {0x4004, 0x02},//BLC CTRL04 d:0x08 blc_line_num is 2
    {0x4000, 0x09},//BLC CTRL00 d:0x89 Close blc_median_filter_enable
    {0x4837, 0x24},//PCLK_PERIOD d:0x15 Period of pclk2x, pclk_div=1,and 1-bit decimal
    {0x4050, 0x6e},//BLC MAX d:0xFF blc max black level
    {0x4051, 0x8f},//STABLE RANGE d:0x7F BLC stable range
    {0x0100, 0x01},//leave standby and sleep
};
#endif
