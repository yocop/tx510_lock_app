#include <stdio.h>
#include <ulog/ulog.h>
#include "iic_function.h"
#include "camera_config.h"
#include "drv_isp.h"
#include "soc.h"
#include "pin_name.h"
#include "sc132gs_drv.h"
#include "sc132gs_config.h"
#include "drv_gpio.h"
#include "cli/cli_api.h"
#include "if_v.h"

static i2c_dev_t i2c_sc132gs;
static i2c_dev_t i2c_sc132gs_right;

extern int g_scenario_type;
extern gpio_pin_handle_t gpio_pin_handle;

regval_list ir_expo_arg[] = {
	{0x3e01,0x00},  //0x03
	{0x3e02,0x00},  //0xb6
	{0x3e08,0x00},//; gain
	{0x3e09,0x00},//; gain
};


regval_list sc132_strobe_spk_time[] = {
	//@@Below need program every time during exposure update strobe 10ms
	//;Exp 3ms(0586), Gain 1x
	{0x3e01,0x12},  //0x07
	{0x3e02,0x3a},  //0xd0
	{0x3e08,0x23},//; gain
	{0x3e09,0x26},//; gain
};

regval_list sc132_strobe_spk_time_outside[] = {
	//@@Below need program every time during exposure update strobe 10ms
	//;Exp 1ms(01d6), Gain 1x
	{0x3e01,0x07},  //0x03
	{0x3e02,0xd0},  //0xb6
	{0x3e08,0x03},//; gain
	{0x3e09,0x20},//; gain
}; //spk_pic_addr

static int sc132gs_check_id(i2c_dev_t *i2c)
{
    int ret = 0;
    uint8_t data[2] = {0};

	//printf("check id , adr %08x \n", i2c->config.dev_addr);
	ret = iic_read16bit(i2c,  SC132GS_ADDR_CHIPID_H, data);
	printf("sc132gs: chipID H = 0x%02x \n", data[0]);
	if (ret < 0 || (data[0] != SC132GS_DATA_CHIPID_H))
		return -1;

	ret = iic_read16bit(i2c,  SC132GS_ADDR_CHIPID_L, data);
	printf("sc132gs: chipID L = 0x%02x \n", data[0]);
	
   	if (ret < 0 || (data[0] != SC132GS_DATA_CHIPID_L))
        return -1;

    return ret;
}


static void camera_io_out32(uint32_t addr, uint32_t value)			//write
{
	volatile uint32_t *ptr = (volatile uint32_t *)(addr);
	//printf("write: addr=0x%x, data=0x%x\n", addr, value);
	*ptr = value;
}

static uint32_t camera_io_in32(uint32_t addr)						//read
{
	volatile uint32_t *ptr = (volatile uint32_t *)(addr);
	//printf("read:  addr=0x%x, data=0x%x\n", ptr, *ptr);
	
	return *ptr;
}

static void camera_io_rstn_init(uint8_t index)
{
	uint32_t reg_val;

	reg_val = camera_io_in32(CSKY_AOGPIO_BASE + 0x04);
	reg_val |= (0x1 << index);
	
	camera_io_out32(CSKY_AOGPIO_BASE + 0x04, reg_val);
}

void camera_io_rstn_set(uint8_t index, bool level)
{
	uint32_t reg_val;

	reg_val = camera_io_in32(CSKY_AOGPIO_BASE + 0x00); 
	if (level)
		reg_val |= (0x1 << index);
	else
		reg_val &= ~(0x1 << index);

	camera_io_out32(CSKY_AOGPIO_BASE + 0x00, reg_val);
}

void sensorHwReset(int sensor_idx){
    camera_io_rstn_init(sensor_idx);
    camera_io_rstn_set(sensor_idx, true);    aos_msleep(1);
    camera_io_rstn_set(sensor_idx, false);   aos_msleep(1);
    camera_io_rstn_set(sensor_idx, true);    aos_msleep(1);
}


int sc132gs_hw_reset(void)
{
	volatile uint32_t *base;

    sensorHwReset(7);
#if 0
	//IR_PWRDN UART1_RTSN GPIOA[21]
    base = (uint32_t *)(CSKY_GPIO0_BASE + 0x04);  //GPIOA direction
    *base |= (0x1 << 21); 
    base = (uint32_t *)(CSKY_GPIO0_BASE + 0x00);  //out
    *base |= (0x1 << 21); //IR_PWRDN

	//IR_RESET AOGPIO7
	base = (uint32_t *)(CSKY_AOGPIO_BASE + 0x04); //gpio dicrection
	*base |= (0x1 << 7);
	base = (uint32_t *)(CSKY_AOGPIO_BASE + 0x00); //output
    *base &= ~(0x1 << 7);
    aos_msleep(1);
	*base |= (0x1 << 7);
#endif 
    return 0;
}

int sc132_write1(uint16_t reg, uint8_t value){

    int ret = 0;
    ir_expo_arg[0].addr = reg;
    ir_expo_arg[0].data = value;
//    ret = iic_write(&sc132_config_reg, (struct regval_list *)sc132_config_reg, ARRAY_SIZE(sc132_config_reg));
    ret = iic_write(&i2c_sc132gs_right, reg, value);

}

int sc132_read1(uint16_t reg){

    int ret = 0;
    uint8_t data[2] = {0};

    ret = iic_read(&i2c_sc132gs_right,  reg, &data);

    printf("read reg 0x%02x value 0x%x \n", reg,data[0]);
}

int sc132_set_exposure_para(void)
{
    int ret = 0;

    ret = iic_write_array(&i2c_sc132gs, (struct regval_list *)ir_expo_arg, ARRAY_SIZE(ir_expo_arg));
    if(ret != 0) {
        printf( " sc132 expo config error:%d \n", ret);
    }
	
    return ret; 
}

int sc132_get_exposure_para(void)
{
    int ret = 0;

    ret = iic_read_array(&i2c_sc132gs, (struct regval_list *)ir_expo_arg, ARRAY_SIZE(ir_expo_arg));
    if(ret != 0) {
        printf(" sc132 expo config error:%d \n", ret);
    }
	
    return ret; 
}

int sc132_config_spk_strobe_time(void)
{
	int ret = 0;

	if(SENSOR_EXPO_INDOOR == g_scenario_type)
	{
		ret = iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132_strobe_spk_time, ARRAY_SIZE(sc132_strobe_spk_time));
		printf("scence indoor!\n");
	}
	else if(SENSOR_EXPO_OUTDOOR == g_scenario_type)
	{
		ret = iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132_strobe_spk_time_outside, ARRAY_SIZE(sc132_strobe_spk_time_outside));
		printf("scence outdoor!\n");
	}
		
	if (ret < 0) {
		return -1;
	}

	return 0;
}
int sc132gs_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
	g_ifv_sensor_func.pGetExpoPara      = sc132_get_exposure_para; 
    g_ifv_sensor_func.pSetExpoPara      = sc132_set_exposure_para; 
    g_ifv_sensor_func.pSetSpkStrobePara = sc132_config_spk_strobe_time; 

   	memcpy(&i2c_sc132gs, i2c, sizeof(i2c_dev_t));
    i2c_sc132gs.config.dev_addr = dev_addr;
    //Check the id of camera
   	int ret = sc132gs_check_id(&i2c_sc132gs);
    if (ret < 0) {
        printf("sc132gs check id fail\n");
        return ret;
    }
	
    return ret;
}



int sc132gs_right_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
	memcpy(&i2c_sc132gs_right, i2c, sizeof(i2c_dev_t));
    i2c_sc132gs_right.config.dev_addr = dev_addr;
    //Check the id of camera
   	int ret = sc132gs_check_id(&i2c_sc132gs_right);
    if (ret < 0) {
        printf("sc132gs check id fail\n");
        return ret;
    }
	
    return ret;
}

int32_t sc132gs_mode_config(sensor_mode_t mode)
{
	int ret = 0;
	//printf("%s >>>>  %d, mode %d \n",__func__, __LINE__, mode);

	if (STANDBY == mode) {
		ret = iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132gs_sw_standby, ARRAY_SIZE(sc132gs_sw_standby));
		if (ret < 0) {
			return -1;
		}
	} 
	else if(STREAM == mode)
	{
		ret = iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132gs_streaming, ARRAY_SIZE(sc132gs_streaming));
		if (ret < 0) {
			return -1;
		}
	}
	else {}

	return 0;
}


int sc132gs_right_mode_config(sensor_mode_t mode)
{
	int ret = 0;
	//printf("%s >>>>  %d, mode %d \n",__func__, __LINE__, mode);

	if (STANDBY == mode) {
		ret = iic_write_array(&i2c_sc132gs_right, (struct regval_list *)sc132gs_sw_standby, ARRAY_SIZE(sc132gs_sw_standby));
		if (ret < 0) {
			return -1;
		}
	} 
	else if(STREAM == mode)
	{
		ret = iic_write_array(&i2c_sc132gs_right, (struct regval_list *)sc132gs_streaming, ARRAY_SIZE(sc132gs_streaming));
		if (ret < 0) {
			return -1;
		}
	}
	else {}

	return 0;
}

int32_t sc132gs_config(imageFormat_t cfg)
{
	int32_t ret = 0;

	//printf("sc132gs config. \n");

	if( R_1080x1280 == cfg.res && RAW10 == cfg.format)
	{
		ret = iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132gs_1080p_raw10, ARRAY_SIZE(sc132gs_1080p_raw10));
		if (ret < 0) {
			return -1;
		}

		// ret = iic_read_array(&i2c_sc132gs, (struct regval_list *)sc132gs_640x480_raw8, ARRAY_SIZE(sc132gs_640x480_raw8));
		// if (ret < 0) {
		// 	return -1;
		// }
		
		//sc132gs_mode_config(SC132GS_STANDBY);
	
		//printf("sc132gs reg init done. \n");
	}
	// else if( R_640x480 == cfg.res && RAW8 == cfg.format)
	// {
	// 	ret = iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132gs_640x480_raw8, ARRAY_SIZE(sc132gs_640x480_raw8));
	// 	if (ret < 0) {
	// 		return -1;
	// 	}

	// 	// ret = iic_read_array(&i2c_sc132gs, (struct regval_list *)sc132gs_640x480_raw8, ARRAY_SIZE(sc132gs_640x480_raw8));
	// 	// if (ret < 0) {
	// 	// 	return -1;
	// 	// }
		
	// 	//sc132gs_mode_config(SC132GS_STANDBY);
	
	// 	//printf("sc132gs reg init done. \n");
	// }

	return ret;
}

int32_t sc132gs_right_config(imageFormat_t cfg)
{
	int ret = 0;
	
	return ret;
}


int sc132gs_debug_test(void)
{
	// iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132gs_640x480_raw8, ARRAY_SIZE(sc132gs_640x480_raw8));
	// aos_msleep(100);
	// iic_read_array(&i2c_sc132gs, (struct regval_list *)sc132gs_640x480_raw8, ARRAY_SIZE(sc132gs_640x480_raw8));

	//iic_write_array(&i2c_sc132gs, (struct regval_list *)sc132gs_debug_ae, ARRAY_SIZE(sc132gs_debug_ae));
	aos_msleep(100);
	iic_read_array(&i2c_sc132gs, (struct regval_list *)sc132gs_debug, ARRAY_SIZE(sc132gs_debug));

	return 0;
}

int sc132_write(uint8_t isp_idx, uint16_t reg, uint8_t value){

    int ret = 0;
    ir_expo_arg[0].addr = reg;
    ir_expo_arg[0].data = value;
//    ret = iic_write(&sc132_config_reg, (struct regval_list *)sc132_config_reg, ARRAY_SIZE(sc132_config_reg));

    if(isp_idx == 0)
        ret = iic_write(&i2c_sc132gs_right, reg, value);
    if(isp_idx == 1)
        ret = iic_write(&i2c_sc132gs, reg, value);

}

int sc132_read(uint8_t isp_idx,uint16_t reg){

    int ret = 0;
    uint8_t data[2] = {0};

    if(isp_idx == 0)
        ret = iic_read(&i2c_sc132gs_right,  reg, &data);
    if(isp_idx == 1)
        ret = iic_read(&i2c_sc132gs,  reg, &data);

    printf("read reg 0x%02x value 0x%x \n", reg,data[0]);
}


void sensor_command(char *outbuf, int len, int argc, char **argv)
{
    uint16_t reg = 0;
    uint8_t value = 0;
    uint8_t isp_idx = 0;
    
    if (argc == 1) {
        printf("wrong argv numer.\n");
        return;
    }
    if (!strcmp(argv[1], "set")) {
        isp_idx = strtoul(argv[2], NULL, 16);
        reg = strtoul(argv[3], NULL, 16);
        value = strtoul(argv[4], NULL, 16);
        printf("set sensor%d reg 0x%x, value 0x%x ",isp_idx, reg, value);
        sc132_write(isp_idx, reg,value);
        
    } 
    else if (!strcmp(argv[1], "get")) {
        
            isp_idx = strtoul(argv[2], NULL, 16);
            reg = strtoul(argv[3], NULL, 16);
            printf("get sensor%d reg 0x%x \n",isp_idx, reg);
            sc132_read(isp_idx, reg);
        }
    else if (!strcmp(argv[1], "set1")) {
        reg = strtoul(argv[2], NULL, 16);
        value = strtoul(argv[3], NULL, 16);
        printf("set reg 0x%x, value 0x%x ", reg, value);
        sc132_write1(reg, value);
        
    } 
    else if (!strcmp(argv[1], "get1")) {
        
            reg = strtoul(argv[2], NULL, 16);
            
            printf("get reg 0x%x \n", reg);
            sc132_read1(reg);
        }
    else {
        cli_printf("param invalid!\n");
        return;
    }

    return;
}




static const struct cli_command_st img_cli_cmd[] = 
{
    [0] =
    {
        .name            = "sensor",
        .help            = "sensor set/get register",
        .function        = sensor_command
    },

};


int img_cli_register(void) 
{
    int ret = 0;

    ret = cli_register_commands(img_cli_cmd, sizeof(img_cli_cmd)/sizeof(struct cli_command_st));
    if (ret != 0) {
        return ret;
    }

    return 0;
}

void sc132gs_reg_test()
{
	iic_read_array(&i2c_sc132gs, (struct regval_list *)sc132gs_debug, ARRAY_SIZE(sc132gs_debug));
}

