#ifndef _SC132GS_DRV_H_
#define _SC132GS_DRV_H_
#include <stdio.h>
#include "pin_name.h"
#include "iic_function.h"
#include "sensor_common.h"


#define SC132GS_ADDR_CHIPID_H		0x3107
#define SC132GS_ADDR_CHIPID_L		0x3108
#define SC132GS_DATA_CHIPID_H		0x01
#define SC132GS_DATA_CHIPID_L		0x32

#define SC132GS_STROBE              (0x3361)

int sc132gs_hw_reset(void);
int sc132gs_init(i2c_dev_t *i2c, uint32_t dev_addr);
int sc132gs_left_init(i2c_dev_t *i2c, uint32_t dev_addr);
int32_t sc132gs_mode_config(sensor_mode_t mode);
int sc132gs_left_mode_config(sensor_mode_t mode);
int32_t sc132gs_config(imageFormat_t cfg);
int sc132gs_debug_test(void);
int sc132_set_exposure_para(void);
int sc132_get_exposure_para(void);
#endif
