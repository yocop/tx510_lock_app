#ifndef _GC2375_DRV_H_
#define _GC2375_DRV_H_
#include <stdio.h>
#include "aos/hal/i2c.h"
#include "drv_isp.h"
#include "aos/hal/gpio.h"
#include "pin_name.h"


#define GC2375_REG_CHIPID_H 0xF0
#define GC2375_REG_CHIPID_L 0xF1

int gc2375_init(i2c_dev_t *i2c, uint32_t dev_addr);
int32_t gc2375_config(imageFormat_t cfg);
void gc2375_write_reg(uint8_t reg, uint8_t val);



#endif
