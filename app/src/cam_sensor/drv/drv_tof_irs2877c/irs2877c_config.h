#ifndef _IRS2877C_CONFIG_DRV_H_
#define _IRS2877C_CONFIG_DRV_H_
#include <stdio.h>
#include "camera_config.h"

regval_list irs2877c_640x241x5_standby[] = {
	{0x9400, 0x0000}
};



regval_list irs2877c_640x241x5_stream[] = {
	{0x9400, 0x0001}
};


regval_list irs2877c_640x241x5_raw12[] = {
	{0x9000, 0x0A0A},	 // S00_EXPOTIME 0.128 ms @ 20.08 MHz (Grey-Scale)
	{0x9001, 0x0000},	 // S00_FRAMETIME
	{0x9002, 0x49CE},	 // S01_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9003, 0x0000},	 // S01_FRAMETIME
	{0x9004, 0x49CE},	 // S02_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9005, 0x0000},	 // S02_FRAMETIME
	{0x9006, 0x49CE},	 // S03_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9007, 0x0000},	 // S03_FRAMETIME
	{0x9008, 0x49CE},	 // S04_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9009, 0x0000},	 // S04_FRAMETIME
	{0x9080, 0x0A0A},	 // S00_EXPOTIMEMAX 0.128 ms @ 20.08 MHz (Grey-Scale)
	{0x9081, 0x0000},	 // S00_FRAMETIMEMIN
	{0x9082, 0x00A0},	 // S00_ILLUCFG Gray-Scale @ 20.08 MHz (LUT0)
	{0x9083, 0x00A0},	 // S00_SENSORCFG (Greyscale)
	{0x9084, 0x8000},	 // S00_ROCFG (Reconfig allowed)
	{0x9085, 0x49CE},	 // S01_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x9086, 0x0000},	 // S01_FRAMETIMEMIN
	{0x9087, 0x0000},	 // S01_ILLUCFG 0° @ 20.08 MHz (LUT0)
	{0x9088, 0x0000},	 // S01_SENSORCFG
	{0x9089, 0x0000},	 // S01_ROCFG (Reconfig forbidden)
	{0x908A, 0x49CE},	 // S02_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x908B, 0x0000},	 // S02_FRAMETIMEMIN
	{0x908C, 0x0030},	 // S02_ILLUCFG 90° @ 20.08 MHz (LUT0)
	{0x908D, 0x0000},	 // S02_SENSORCFG
	{0x908E, 0x0000},	 // S02_ROCFG (Reconfig forbidden)
	{0x908F, 0x49CE},	 // S03_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x9090, 0x0000},	 // S03_FRAMETIMEMIN
	{0x9091, 0x0060},	 // S03_ILLUCFG 180° @ 20.08 MHz (LUT0)
	{0x9092, 0x0000},	 // S03_SENSORCFG
	{0x9093, 0x0000},	 // S03_ROCFG (Reconfig forbidden)
	{0x9094, 0x49CE},	 // S04_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x9095, 0x0000},	 // S04_FRAMETIMEMIN
	{0x9096, 0x0010},	 // S04_ILLUCFG 270° @ 20.08 MHz (LUT0)
	{0x9097, 0x0000},	 // S04_SENSORCFG
	{0x9098, 0xC082},	 // S04_ROCFG (Reconfig & Usecase Switch allowed) 2 rotations of illu code
	{0x91C0, 0x0592},	 // CSICFG
	{0x91C1, 0x0000},	 // ROICOLMIN
	{0x91C2, 0x027F},	 // ROICOLMAX
	{0x91C3, 0xEF00},	 // ROIROW
	{0x91C4, 0x0240},	 // ROS
	{0x91C6, 0x0012},	 // PDMEAS0
	{0x91D1, 0x0008},	 // EXPCFG0
	{0x91D2, 0x0020},	 // EXPCFG1
	{0x91D3, 0x8008},	 // PSOUT (SE enabled)
	{0x91D4, 0x0135},	 // PADMODCFG
	{0x91DB, 0x0005},	 // MB_CFG0 (5 sequences to execute)
	{0x91DF, 0x061A},	 // MB0_FRAMETIME (30 fps)
	{0x91E7, 0x0008},	 // MBSEQ0_CFG0 (MB0 Repetitions = 1)
	{0x91F6, 0x16A1},	 // PLL_MODLUT0_CFG0 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91F7, 0x1EB8},	 // PLL_MODLUT0_CFG1 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91F8, 0x0005},	 // PLL_MODLUT0_CFG2 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91F9, 0x3F01},	 // PLL_MODLUT0_CFG3 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FA, 0x9990},	 // PLL_MODLUT0_CFG4 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FB, 0x04A1},	 // PLL_MODLUT0_CFG5 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FC, 0x000A},	 // PLL_MODLUT0_CFG6 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FD, 0x002B},	 // PLL_MODLUT0_CFG7 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x922C, 0x0005},	 // SENSOR_LENGHT_CODE0 (length = 6)
	{0x922D, 0x0038},	 // SENSOR_CODE0_0 (code = 111000)
	{0x9250, 0x0005},	 // ILLU_LENGHT_CODE0 (length = 6)
	{0x9251, 0x0030},	 // ILLU_CODE0_0 (code = 110000)
	{0x9274, 0x0002},	 // DIGCLKDIV_S0_PLL0 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x9284, 0x0002},	 // DLLREGDELAY_S0_PLL0 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x9294, 0x02B5},	 // Filter Stage initializaiton Value 611
	{0x9295, 0x02B5},	 // Filter Stage initializaiton Value 610
	{0x9296, 0x02BA},	 // Filter Stage initializaiton Value 69 
	{0x9297, 0x02B5},	 // Filter Stage initializaiton Value 68 
	{0x9298, 0x02B5},	 // Filter Stage initializaiton Value 67 
	{0x9299, 0x02BA},	 // Filter Stage initializaiton Value 66 
	{0x929A, 0x02B5},	 // Filter Stage initializaiton Value 65 
	{0x929B, 0x02B5},	 // Filter Stage initializaiton Value 64 
	{0x929C, 0x02BA},	 // Filter Stage initializaiton Value 63 
	{0x929D, 0x02B5},	 // Filter Stage initializaiton Value 62 
	{0x929E, 0x02B5},	 // Filter Stage initializaiton Value 61 
	{0x929F, 0x02BA},	 // Filter Stage initializaiton Value 60 
	{0x92A0, 0x015D},	 // Filter Stage initializaiton Value 57 
	{0x92A1, 0x015D},	 // Filter Stage initializaiton Value 56 
	{0x92A2, 0x015D},	 // Filter Stage initializaiton Value 55 
	{0x92A3, 0x015D},	 // Filter Stage initializaiton Value 54 
	{0x92A4, 0x015D},	 // Filter Stage initializaiton Value 53 
	{0x92A5, 0x015D},	 // Filter Stage initializaiton Value 52 
	{0x92A6, 0x015D},	 // Filter Stage initializaiton Value 51 
	{0x92A7, 0x015D},	 // Filter Stage initializaiton Value 50 
	{0x92A8, 0x0080},	 // Filter Stage initializaiton Value 47 
	{0x92A9, 0x00CA},	 // Filter Stage initializaiton Value 46 
	{0x92AA, 0x00B7},	 // Filter Stage initializaiton Value 45 
	{0x92AB, 0x0092},	 // Filter Stage initializaiton Value 44 
	{0x92AC, 0x00CA},	 // Filter Stage initializaiton Value 43 
	{0x92AD, 0x0080},	 // Filter Stage initializaiton Value 42 
	{0x92AE, 0x00CA},	 // Filter Stage initializaiton Value 41 
	{0x92AF, 0x0080},	 // Filter Stage initializaiton Value 40 
	{0x92B0, 0x0040},	 // Filter Stage initializaiton Value 37 
	{0x92B1, 0x0040},	 // Filter Stage initializaiton Value 36 
	{0x92B2, 0x0040},	 // Filter Stage initializaiton Value 35 
	{0x92B3, 0x0040},	 // Filter Stage initializaiton Value 34 
	{0x92B4, 0x0040},	 // Filter Stage initializaiton Value 33 
	{0x92B5, 0x0040},	 // Filter Stage initializaiton Value 32 
	{0x92B6, 0x0040},	 // Filter Stage initializaiton Value 31 
	{0x92B7, 0x0040},	 // Filter Stage initializaiton Value 30 
	{0x92B8, 0x0040},	 // Filter Stage initializaiton Value 27 
	{0x92B9, 0x0040},	 // Filter Stage initializaiton Value 26 
	{0x92BA, 0x0040},	 // Filter Stage initializaiton Value 25 
	{0x92BB, 0x0040},	 // Filter Stage initializaiton Value 24 
	{0x92BC, 0x0040},	 // Filter Stage initializaiton Value 23 
	{0x92BD, 0x0040},	 // Filter Stage initializaiton Value 22 
	{0x92BE, 0x0040},	 // Filter Stage initializaiton Value 21 
	{0x92BF, 0x0040},	 // Filter Stage initializaiton Value 20 
	{0x92C0, 0x0040},	 // Filter Stage initializaiton Value 17 
	{0x92C1, 0x0040},	 // Filter Stage initializaiton Value 16 
	{0x92C2, 0x0040},	 // Filter Stage initializaiton Value 15 
	{0x92C3, 0x0040},	 // Filter Stage initializaiton Value 14 
	{0x92C4, 0x0040},	 // Filter Stage initializaiton Value 13 
	{0x92C5, 0x0040},	 // Filter Stage initializaiton Value 12 
	{0x92C6, 0x0040},	 // Filter Stage initializaiton Value 11 
	{0x92C7, 0x0040},	 // Filter Stage initializaiton Value 10 
	{0x92C8, 0x0040},	 // Filter Stage initializaiton Value 07 
	{0x92C9, 0x0040},	 // Filter Stage initializaiton Value 06 
	{0x92CA, 0x0040},	 // Filter Stage initializaiton Value 05 
	{0x92CB, 0x0040},	 // Filter Stage initializaiton Value 04 
	{0x92CC, 0x0040},	 // Filter Stage initializaiton Value 03 
	{0x92CD, 0x0040},	 // Filter Stage initializaiton Value 02 
	{0x92CE, 0x0040},	 // Filter Stage initializaiton Value 01 
	{0x92CF, 0x0040},	 // Filter Stage initializaiton Value 00 
	{0x92D0, 0x9E60},	 // TH0
	{0x92D1, 0x9E9E},	 // TH1
	{0x92D2, 0x9E9E},	 // TH2
	{0x92D3, 0x9E9E},	 // TH3
	{0x92D4, 0x8461},	 // TH4
	{0x92D5, 0x9D9D},	 // TH5
	{0x92D6, 0x9D9D},	 // TH6
	{0x92D7, 0x009D},	 // TH7
	{0x92D8, 0x503A},	 // TH8
	{0x92D9, 0x7663},	 // TH9
	{0x92DA, 0x9887},	 // TH10
	{0x92DB, 0x00A8},	 // TH11
	{0x92DC, 0x1F1E},	 // TH12
	{0x92DD, 0x2120},	 // TH13
	{0x92DE, 0x2322},	 // TH14
	{0x92DF, 0x0024},	 // TH15
	{0x92E0, 0x473F},	 // TH16
	{0x92E1, 0x7760},	 // TH17
	{0x92E2, 0x967F},	 // TH18
	{0x92E3, 0x009E},	 // TH19
	{0x92E4, 0x1911},	 // TH20
	{0x92E5, 0x2720},	 // TH21
	{0x92E6, 0x352E},	 // TH22
	{0x92E7, 0x003C},	 // TH23
	{0x92E8, 0x160E},	 // TH24
	{0x92E9, 0x251D},	 // TH25
	{0x92EA, 0x342C},	 // TH26
	{0x92EB, 0x433B},	 // TH27
	{0x92EC, 0x524A},	 // TH28
	{0x92ED, 0x0059},	 // TH29
	{0x92EE, 0x0100},	 // CMPMUX
	{0x92EF, 0x018E},	 // HH
	{0x92F0, 0x00CF},	 // HL
	{0x92F1, 0x0049},	 // LH
	{0x92F2, 0x0036},	 // LL
	{0x92F3, 0x0001},	 // PlausiCheck ; Idle Mode enable 
	{0x92F4, 0x0068},	 // Idle Counter
	{0x92F5, 0x0300},	 // Module ID 0
	{0x92F6, 0x0000},	 // Module ID 1
	{0x92F7, 0x0000},	 // Module ID 2
	{0x92F8, 0x0000},	 // Module ID 3
	{0x92F9, 0x0000},	 // Module ID 4
	{0x92FA, 0x0000},	 // Module ID 5
	{0x92FB, 0x0000},	 // Module ID 6
	{0x92FC, 0x005A},	 // Module ID 7
	{0x92FD, 0x42F8},	 // CRC
	{0x9401, 0x0002},	 // Mode
	{0x980A, 0xFEFF},	 // EXPPAT5
	{0x980C, 0x3F00},	 // EXPPAT7
	{0x980D, 0x3F00},	 // EXPPAT8
	{0xA001, 0x0007},	 // DMUX1
	{0xA008, 0x1513},	 // PADGPIOCFG1
	{0xA039, 0x16A1},	 // PLL_SYSLUT_CFG0 (for fref = 24.00 MHz)
	{0xA03A, 0x5555},	 // PLL_SYSLUT_CFG1 (for fref = 24.00 MHz)
	{0xA03B, 0x0005},	 // PLL_SYSLUT_CFG2 (for fref = 24.00 MHz)
	{0xA03C, 0x0000},	 // PLL_SYSLUT_CFG3 (for fref = 24.00 MHz)
	{0xA03D, 0x04D0},	 // PLL_SYSLUT_CFG4 (for fref = 24.00 MHz)
	{0xA03E, 0x0000},	 // PLL_SYSLUT_CFG5 (for fref = 24.00 MHz)
	{0xA03F, 0x000F},	 // PLL_SYSLUT_CFG6 (for fref = 24.00 MHz)
	{0xA058, 0x0A08},	 // PIXELREFTRIM1
	{0xA05B, 0x7422},	 // SENSREFI
	
	//{0x9400, 0x0001},

};

regval_list irs2877c_640x240x5_raw12[] = {
	{0x9000, 0x0A0A},	 // S00_EXPOTIME 0.128 ms @ 20.08 MHz (Grey-Scale)
	{0x9001, 0x0000},	 // S00_FRAMETIME
	{0x9002, 0x49CE},	 // S01_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9003, 0x0000},	 // S01_FRAMETIME
	{0x9004, 0x49CE},	 // S02_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9005, 0x0000},	 // S02_FRAMETIME
	{0x9006, 0x49CE},	 // S03_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9007, 0x0000},	 // S03_FRAMETIME
	{0x9008, 0x49CE},	 // S04_EXPOTIME 1.000 ms @ 20.08 MHz 
	{0x9009, 0x0000},	 // S04_FRAMETIME
	{0x9080, 0x0A0A},	 // S00_EXPOTIMEMAX 0.128 ms @ 20.08 MHz (Grey-Scale)
	{0x9081, 0x0000},	 // S00_FRAMETIMEMIN
	{0x9082, 0x00A0},	 // S00_ILLUCFG Gray-Scale @ 20.08 MHz (LUT0)
	{0x9083, 0x00A0},	 // S00_SENSORCFG (Greyscale)
	{0x9084, 0x8000},	 // S00_ROCFG (Reconfig allowed)
	{0x9085, 0x49CE},	 // S01_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x9086, 0x0000},	 // S01_FRAMETIMEMIN
	{0x9087, 0x0000},	 // S01_ILLUCFG 0° @ 20.08 MHz (LUT0)
	{0x9088, 0x0000},	 // S01_SENSORCFG
	{0x9089, 0x0000},	 // S01_ROCFG (Reconfig forbidden)
	{0x908A, 0x49CE},	 // S02_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x908B, 0x0000},	 // S02_FRAMETIMEMIN
	{0x908C, 0x0030},	 // S02_ILLUCFG 90° @ 20.08 MHz (LUT0)
	{0x908D, 0x0000},	 // S02_SENSORCFG
	{0x908E, 0x0000},	 // S02_ROCFG (Reconfig forbidden)
	{0x908F, 0x49CE},	 // S03_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x9090, 0x0000},	 // S03_FRAMETIMEMIN
	{0x9091, 0x0060},	 // S03_ILLUCFG 180° @ 20.08 MHz (LUT0)
	{0x9092, 0x0000},	 // S03_SENSORCFG
	{0x9093, 0x0000},	 // S03_ROCFG (Reconfig forbidden)
	{0x9094, 0x49CE},	 // S04_EXPOTIMEMAX 1.000 ms @ 20.08 MHz 
	{0x9095, 0x0000},	 // S04_FRAMETIMEMIN
	{0x9096, 0x0010},	 // S04_ILLUCFG 270° @ 20.08 MHz (LUT0)
	{0x9097, 0x0000},	 // S04_SENSORCFG
	{0x9098, 0xC082},	 // S04_ROCFG (Reconfig & Usecase Switch allowed) 2 rotations of illu code
	{0x91C0, 0x0592},	 // CSICFG
	{0x91C1, 0x0000},	 // ROICOLMIN
	{0x91C2, 0x027F},	 // ROICOLMAX
	{0x91C3, 0xEE00},	 // ROIROW
	{0x91C4, 0x0240},	 // ROS
	{0x91C6, 0x0012},	 // PDMEAS0
	{0x91D1, 0x0008},	 // EXPCFG0
	{0x91D2, 0x0020},	 // EXPCFG1
	{0x91D3, 0x8008},	 // PSOUT (SE enabled)
	{0x91D4, 0x0135},	 // PADMODCFG
	{0x91DB, 0x0005},	 // MB_CFG0 (5 sequences to execute)
	{0x91DF, 0x061A},	 // MB0_FRAMETIME (30 fps)
	{0x91E7, 0x0008},	 // MBSEQ0_CFG0 (MB0 Repetitions = 1)
	{0x91F6, 0x16A1},	 // PLL_MODLUT0_CFG0 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91F7, 0x1EB8},	 // PLL_MODLUT0_CFG1 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91F8, 0x0005},	 // PLL_MODLUT0_CFG2 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91F9, 0x3F01},	 // PLL_MODLUT0_CFG3 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FA, 0x9990},	 // PLL_MODLUT0_CFG4 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FB, 0x04A1},	 // PLL_MODLUT0_CFG5 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FC, 0x000A},	 // PLL_MODLUT0_CFG6 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x91FD, 0x002B},	 // PLL_MODLUT0_CFG7 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x922C, 0x0005},	 // SENSOR_LENGHT_CODE0 (length = 6)
	{0x922D, 0x0038},	 // SENSOR_CODE0_0 (code = 111000)
	{0x9250, 0x0005},	 // ILLU_LENGHT_CODE0 (length = 6)
	{0x9251, 0x0030},	 // ILLU_CODE0_0 (code = 110000)
	{0x9274, 0x0002},	 // DIGCLKDIV_S0_PLL0 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x9284, 0x0002},	 // DLLREGDELAY_S0_PLL0 (for f_illu = 20.08 MHz [f_mod = 120.48 MHz])
	{0x9294, 0x02B5},	 // Filter Stage initializaiton Value 611
	{0x9295, 0x02B5},	 // Filter Stage initializaiton Value 610
	{0x9296, 0x02BA},	 // Filter Stage initializaiton Value 69 
	{0x9297, 0x02B5},	 // Filter Stage initializaiton Value 68 
	{0x9298, 0x02B5},	 // Filter Stage initializaiton Value 67 
	{0x9299, 0x02BA},	 // Filter Stage initializaiton Value 66 
	{0x929A, 0x02B5},	 // Filter Stage initializaiton Value 65 
	{0x929B, 0x02B5},	 // Filter Stage initializaiton Value 64 
	{0x929C, 0x02BA},	 // Filter Stage initializaiton Value 63 
	{0x929D, 0x02B5},	 // Filter Stage initializaiton Value 62 
	{0x929E, 0x02B5},	 // Filter Stage initializaiton Value 61 
	{0x929F, 0x02BA},	 // Filter Stage initializaiton Value 60 
	{0x92A0, 0x015D},	 // Filter Stage initializaiton Value 57 
	{0x92A1, 0x015D},	 // Filter Stage initializaiton Value 56 
	{0x92A2, 0x015D},	 // Filter Stage initializaiton Value 55 
	{0x92A3, 0x015D},	 // Filter Stage initializaiton Value 54 
	{0x92A4, 0x015D},	 // Filter Stage initializaiton Value 53 
	{0x92A5, 0x015D},	 // Filter Stage initializaiton Value 52 
	{0x92A6, 0x015D},	 // Filter Stage initializaiton Value 51 
	{0x92A7, 0x015D},	 // Filter Stage initializaiton Value 50 
	{0x92A8, 0x0080},	 // Filter Stage initializaiton Value 47 
	{0x92A9, 0x00CA},	 // Filter Stage initializaiton Value 46 
	{0x92AA, 0x00B7},	 // Filter Stage initializaiton Value 45 
	{0x92AB, 0x0092},	 // Filter Stage initializaiton Value 44 
	{0x92AC, 0x00CA},	 // Filter Stage initializaiton Value 43 
	{0x92AD, 0x0080},	 // Filter Stage initializaiton Value 42 
	{0x92AE, 0x00CA},	 // Filter Stage initializaiton Value 41 
	{0x92AF, 0x0080},	 // Filter Stage initializaiton Value 40 
	{0x92B0, 0x0040},	 // Filter Stage initializaiton Value 37 
	{0x92B1, 0x0040},	 // Filter Stage initializaiton Value 36 
	{0x92B2, 0x0040},	 // Filter Stage initializaiton Value 35 
	{0x92B3, 0x0040},	 // Filter Stage initializaiton Value 34 
	{0x92B4, 0x0040},	 // Filter Stage initializaiton Value 33 
	{0x92B5, 0x0040},	 // Filter Stage initializaiton Value 32 
	{0x92B6, 0x0040},	 // Filter Stage initializaiton Value 31 
	{0x92B7, 0x0040},	 // Filter Stage initializaiton Value 30 
	{0x92B8, 0x0040},	 // Filter Stage initializaiton Value 27 
	{0x92B9, 0x0040},	 // Filter Stage initializaiton Value 26 
	{0x92BA, 0x0040},	 // Filter Stage initializaiton Value 25 
	{0x92BB, 0x0040},	 // Filter Stage initializaiton Value 24 
	{0x92BC, 0x0040},	 // Filter Stage initializaiton Value 23 
	{0x92BD, 0x0040},	 // Filter Stage initializaiton Value 22 
	{0x92BE, 0x0040},	 // Filter Stage initializaiton Value 21 
	{0x92BF, 0x0040},	 // Filter Stage initializaiton Value 20 
	{0x92C0, 0x0040},	 // Filter Stage initializaiton Value 17 
	{0x92C1, 0x0040},	 // Filter Stage initializaiton Value 16 
	{0x92C2, 0x0040},	 // Filter Stage initializaiton Value 15 
	{0x92C3, 0x0040},	 // Filter Stage initializaiton Value 14 
	{0x92C4, 0x0040},	 // Filter Stage initializaiton Value 13 
	{0x92C5, 0x0040},	 // Filter Stage initializaiton Value 12 
	{0x92C6, 0x0040},	 // Filter Stage initializaiton Value 11 
	{0x92C7, 0x0040},	 // Filter Stage initializaiton Value 10 
	{0x92C8, 0x0040},	 // Filter Stage initializaiton Value 07 
	{0x92C9, 0x0040},	 // Filter Stage initializaiton Value 06 
	{0x92CA, 0x0040},	 // Filter Stage initializaiton Value 05 
	{0x92CB, 0x0040},	 // Filter Stage initializaiton Value 04 
	{0x92CC, 0x0040},	 // Filter Stage initializaiton Value 03 
	{0x92CD, 0x0040},	 // Filter Stage initializaiton Value 02 
	{0x92CE, 0x0040},	 // Filter Stage initializaiton Value 01 
	{0x92CF, 0x0040},	 // Filter Stage initializaiton Value 00 
	{0x92D0, 0x9E60},	 // TH0
	{0x92D1, 0x9E9E},	 // TH1
	{0x92D2, 0x9E9E},	 // TH2
	{0x92D3, 0x9E9E},	 // TH3
	{0x92D4, 0x8461},	 // TH4
	{0x92D5, 0x9D9D},	 // TH5
	{0x92D6, 0x9D9D},	 // TH6
	{0x92D7, 0x009D},	 // TH7
	{0x92D8, 0x503A},	 // TH8
	{0x92D9, 0x7663},	 // TH9
	{0x92DA, 0x9887},	 // TH10
	{0x92DB, 0x00A8},	 // TH11
	{0x92DC, 0x1F1E},	 // TH12
	{0x92DD, 0x2120},	 // TH13
	{0x92DE, 0x2322},	 // TH14
	{0x92DF, 0x0024},	 // TH15
	{0x92E0, 0x473F},	 // TH16
	{0x92E1, 0x7760},	 // TH17
	{0x92E2, 0x967F},	 // TH18
	{0x92E3, 0x009E},	 // TH19
	{0x92E4, 0x1911},	 // TH20
	{0x92E5, 0x2720},	 // TH21
	{0x92E6, 0x352E},	 // TH22
	{0x92E7, 0x003C},	 // TH23
	{0x92E8, 0x160E},	 // TH24
	{0x92E9, 0x251D},	 // TH25
	{0x92EA, 0x342C},	 // TH26
	{0x92EB, 0x433B},	 // TH27
	{0x92EC, 0x524A},	 // TH28
	{0x92ED, 0x0059},	 // TH29
	{0x92EE, 0x0100},	 // CMPMUX
	{0x92EF, 0x018E},	 // HH
	{0x92F0, 0x00CF},	 // HL
	{0x92F1, 0x0049},	 // LH
	{0x92F2, 0x0036},	 // LL
	{0x92F3, 0x0001},	 // PlausiCheck ; Idle Mode enable 
	{0x92F4, 0x0068},	 // Idle Counter
	{0x92F5, 0x0300},	 // Module ID 0
	{0x92F6, 0x0000},	 // Module ID 1
	{0x92F7, 0x0000},	 // Module ID 2
	{0x92F8, 0x0000},	 // Module ID 3
	{0x92F9, 0x0000},	 // Module ID 4
	{0x92FA, 0x0000},	 // Module ID 5
	{0x92FB, 0x0000},	 // Module ID 6
	{0x92FC, 0x005A},	 // Module ID 7
	{0x92FD, 0x42F8},	 // CRC
	{0x9401, 0x0002},	 // Mode
	{0x980A, 0xFEFF},	 // EXPPAT5
	{0x980C, 0x3F00},	 // EXPPAT7
	{0x980D, 0x3F00},	 // EXPPAT8
	{0xA001, 0x0007},	 // DMUX1
	{0xA008, 0x1513},	 // PADGPIOCFG1
	{0xA039, 0x16A1},	 // PLL_SYSLUT_CFG0 (for fref = 24.00 MHz)
	{0xA03A, 0x5555},	 // PLL_SYSLUT_CFG1 (for fref = 24.00 MHz)
	{0xA03B, 0x0005},	 // PLL_SYSLUT_CFG2 (for fref = 24.00 MHz)
	{0xA03C, 0x0000},	 // PLL_SYSLUT_CFG3 (for fref = 24.00 MHz)
	{0xA03D, 0x04D0},	 // PLL_SYSLUT_CFG4 (for fref = 24.00 MHz)
	{0xA03E, 0x0000},	 // PLL_SYSLUT_CFG5 (for fref = 24.00 MHz)
	{0xA03F, 0x000F},	 // PLL_SYSLUT_CFG6 (for fref = 24.00 MHz)
	{0xA058, 0x0A08},	 // PIXELREFTRIM1
	{0xA05B, 0x7422},	 // SENSREFI
	
	//{0x9400, 0x0001},

};

#endif
