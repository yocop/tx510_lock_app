#ifndef _IRS2877C_DRV_H_
#define _IRS2877C_DRV_H_
#include <stdio.h>
#include "pin_name.h"
#include "iic_function.h"


#define IRS2877C_ADDR_CHIPID		0xA098
#define IRS2877C_DATA_CHIPID		0x96FD

typedef enum
{
    IRS2877C_STANDBY = 0,
    IRS2877C_STREAM  = 1,
} irs2877c_mode_e;



int irs2877c_hw_reset(int bitn);
int irs2877c_init(i2c_dev_t *i2c, uint32_t dev_addr);
int32_t irs2877c_mode_config(irs2877c_mode_e mode);
int32_t irs2877c_config(imageFormat_t cfg);



#endif
