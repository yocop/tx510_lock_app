#ifndef _BF2253_DRV_H_
#define _BF2253_DRV_H_
#include <stdio.h>
#include "iic_function.h"
#include "drv_isp.h"

#define BF2253_REG_CHIPID_H 0xfc
#define BF2253_REG_CHIPID_L 0xfd
#define BF2253_REG_STANDBY 	0xe0

#define BF2253_G_1600_1200	1

// typedef enum TSM_SENSOR_EXPO
// {
// 	SENSOR_EXPO_INDOOR,
// 	SENSOR_EXPO_OUTDOOR,
//     SENSOR_EXPO_BUTT,
// } TSM_SENSOR_EXPO_E;

typedef enum
{
    BF2253_NO_NEED_CONFIG = 0,
    BF2253_NEED_CONFIG = 1,
} bf2253_config_t;

typedef enum
{
    BF2253_STANDBY = 0,
    BF2253_STREAM  = 1,
} bf2253_mode_t;


extern bf2253_config_t g_bf2253_config;

int bf2253_init(i2c_dev_t *i2c, uint32_t dev_addr);

int32_t bf2253_config(imageFormat_t cfg);
int bf2253_mode_config(bf2253_mode_t mode, uint8_t index);

#endif
