#ifndef _SC2355_DRV_H_
#define _SC2355_DRV_H_
#include <stdio.h>
#include "iic_function.h"
#include "drv_isp.h"
//#include "test_hist.h"

#define SC2355_SLAVE_ADDR_SID1	(0x30)
#define SC2355_RGB_MCLK_IDX     0
#define SC2355_IR_MCLK_IDX      1

#define SC2355_REG_CHIPID_H 0x3107
#define SC2355_REG_CHIPID_L 0x3108
#define SC2355_REG_STANDBY 	0x0100

#define SC2355_G_1600_1200	1

typedef enum
{
    SC2355_NO_NEED_CONFIG = 0,
    SC2355_NEED_CONFIG = 1,
} sc2355_config_t;

// typedef enum
// {
//     STANDBY = 0,
//     STREAM  = 1,
// } sc2355_mode_t;

#define SC2355_RGB_RSTN_INDEX	(0)
#define SC2355_IR_RSTN_INDEX	(11)

extern sc2355_config_t g_sc2355_config;

int sc2355_init(i2c_dev_t *i2c, uint32_t dev_addr);
int sc2355_hw_reset(void);
int32_t sc2355_config(imageFormat_t cfg);
int switch_exposure_para(exposure_scene_t scene);
int set_spk_exposure_para(exposure_scene_t scene);
int32_t sc2355_config_reglist(uint32_t ir_mode, uint32_t addr, uint32_t data);
uint32_t sc2355_lookup_reg(uint32_t ir_mode, uint32_t addr);
int set_sc2355_reglist(regval_list reglist[], uint32_t reglist_len);
int sc2355_mode_config(sensor_mode_t mode);

#endif
