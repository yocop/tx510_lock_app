#ifndef _GC1054_DRV_H_
#define _GC1054_DRV_H_
#include <stdio.h>
//#include "iic_function.h"
#include "aos/hal/i2c.h"
#include "drv_isp.h"
#include "aos/hal/gpio.h"
#include "pin_name.h"


#define GC1054_REG_CHIPID_H 0xF0
#define GC1054_REG_CHIPID_L 0xF1

int gc1054_init(i2c_dev_t *i2c, uint32_t dev_addr);
int32_t gc1054_config(imageFormat_t cfg);
int gc1054_mode_config(i2c_dev_t *i2c, uint32_t dev_addr, sensor_mode_t mode);


#endif
