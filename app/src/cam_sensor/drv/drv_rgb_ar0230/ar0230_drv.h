#ifndef _AR0230_DRV_H_
#define _AR0230_DRV_H_
#include <stdio.h>
//#include "iic_function.h"



int ar0230_init(i2c_dev_t *i2c, uint32_t dev_addr);

int ar0230_finalize();

int ar0230_cfg_format(imageFormat_t cfg);

#endif
