#ifndef _OVXC7022_DRV_H_
#define _OVXC7022_DRV_H_
#include <stdio.h>
//#include "iic_function.h"
#include "drv_isp.h"
#include "aos/hal/gpio.h"
#include "pin_name.h"



/**
  xc7022 need io for reset,
  User should config acture io at first.
*/
typedef enum 
{
	UN_USED_IO_7022 = 0,
	GPIO_7022 = 1,
	AOGPIO_7022 = 2,
}em_io_7022_type;

typedef struct
{
	em_io_7022_type io_type;
	gpio_dev_t gpio;		// config this field if io_type is gpio
	uint8_t ao_gpio_id;		// config this field if io_type is ao_gpio
}st_7022_reset_t;


typedef struct xc7022_cfg
{
	imageFormat_t cfg;
	uint8_t		  color_bar;	// 0xaa is valid value
}xc7022_cfg_t;


int xc7022_init(i2c_dev_t *i2c, uint32_t dev_addr, st_7022_reset_t *reset_io);

int xc7022_finalize();

int xc7022_cfg_bypass(bool setl);

int xc7022_cfg_format(xc7022_cfg_t *xc7022_cfg);

int xc7022_debug_register(void);

int xc7022_hw_reset(st_7022_reset_t *reset_io);



#endif
