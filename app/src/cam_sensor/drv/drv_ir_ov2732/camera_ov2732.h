/*
 * Copyright (C) 2017-2019 Alibaba Group Holding Limited
 */

/******************************************************************************
 * @file     camera_ov2732.h
 * @brief    head file for camera_ov2732
 * @version  V1
 * @date     9. Nov 2019
 ******************************************************************************/

#ifndef _CAMERA_OV2732_H_
#define _CAMERA_OV2732_H_

#include <stdio.h>
#include <string.h>
// #include <csi_config.h>
#include <soc.h>
#include <pin.h>
#include "camera_ov_common.h"

#define OV2732_WINDOW_HEIGHT_MAX 800
#define OV2732_WINDOW_HEIGHT_DEF 720
#define OV2732_WINDOW_WIDTH_MAX  1280
#define OV2732_WINDOW_WIDTH_DEF  1280

#define OV2732_CHIPID_HIGH       0x27
#define OV2732_CHIPID_LOW        0x32
#define OV2732_IIC_SLAVE_ADDR    0x10

const struct regval_list ov2732_1920x1080[] = {
	{0x0103,0x01},		/* soft reset */
	{0x0305,0x3c},
	{0x0307,0x00},
	{0x0308,0x03},
	{0x0309,0x03},
	{0x0327,0x07},
	{0x3016,0x32},
	{0x3000,0x00},
	{0x3001,0x00},
	{0x3002,0x00},
	{0x3013,0x00},
	{0x301f,0xf0},
	{0x3023,0xf0},
	{0x3020,0x9b},
	{0x3022,0x51},
	{0x3106,0x11},
	{0x3107,0x01},
	{0x3500,0x00},
	{0x3501,0x40},
	{0x3502,0x00},
	{0x3503,0x88},
	{0x3505,0x83},
	{0x3508,0x01},
	{0x3509,0x80},
	{0x350a,0x04},
	{0x350b,0x00},
	{0x350c,0x00},
	{0x350d,0x80},
	{0x350e,0x04},
	{0x350f,0x00},
	{0x3510,0x00},
	{0x3511,0x00},
	{0x3512,0x20},
	{0x3600,0x55},
	{0x3601,0x52},
	{0x3612,0xb5},
	{0x3613,0xb3},
	{0x3616,0x83},
	{0x3621,0x00},
	{0x3624,0x06},
	{0x3642,0x88},
	{0x3660,0x00},
	{0x3661,0x00},
	{0x366a,0x64},
	{0x366c,0x00},
	{0x366e,0xff},
	{0x366f,0xff},
	{0x3677,0x11},
	{0x3678,0x11},
	{0x3679,0x0c},
	{0x3680,0xff},
	{0x3681,0x16},
	{0x3682,0x16},
	{0x3683,0x90},
	{0x3684,0x90},
	{0x3768,0x04},
	{0x3769,0x20},
	{0x376a,0x04},
	{0x376b,0x20},
	{0x3620,0x80},
	{0x3662,0x10},
	{0x3663,0x24},
	{0x3665,0xa0},
	{0x3667,0xa6},
	{0x3674,0x01},
	{0x373d,0x24},
	{0x3741,0x28},
	{0x3743,0x28},
	{0x3745,0x28},
	{0x3747,0x28},
	{0x3748,0x00},
	{0x3749,0x78},
	{0x374a,0x00},
	{0x374b,0x78},
	{0x374c,0x00},
	{0x374d,0x78},
	{0x374e,0x00},
	{0x374f,0x78},
	{0x3766,0x12},
	{0x37e0,0x00},
	{0x37e6,0x04},
	{0x37e5,0x04},
	{0x37e1,0x04},
	{0x3737,0x04},
	{0x37d0,0x0a},
	{0x37d8,0x04},
	{0x37e2,0x08},
	{0x3739,0x10},
	{0x37e4,0x18},
	{0x37e3,0x04},
	{0x37d9,0x10},
	{0x4040,0x04},
	{0x4041,0x0f},
	{0x4008,0x00},
	{0x4009,0x0d},
	{0x37a1,0x14},
	{0x37a8,0x16},
	{0x37ab,0x10},
	{0x37c2,0x04},
	{0x3705,0x00},
	{0x3706,0x28},
	{0x370a,0x00},
	{0x370b,0x78},
	{0x3714,0x24},
	{0x371a,0x1e},
	{0x372a,0x03},
	{0x3756,0x00},
	{0x3757,0x0e},
	{0x377b,0x00},
	{0x377c,0x0c},
	{0x377d,0x20},
	{0x3790,0x28},
	{0x3791,0x78},
	{0x3800,0x00},
	{0x3801,0x00},
	{0x3802,0x00},
	{0x3803,0x04},
	{0x3804,0x07},
	{0x3805,0x8f},
	{0x3806,0x04},
	{0x3807,0x43},
	{0x3808,0x07},
	{0x3809,0x80},
	{0x380a,0x04},
	{0x380b,0x38},
	{0x380c,0x02},	//0x0278=632	0x0790=1936
	{0x380d,0x78},
	{0x380e,0x04},	//0x04a0=1184	0x0478=1144
	{0x380f,0xa0},
	{0x3811,0x08},
	{0x3813,0x04},
	{0x3814,0x01},
	{0x3815,0x01},
	{0x3816,0x01},
	{0x3817,0x01},
	{0x381d,0x40},
	{0x381e,0x02},
	{0x3820,0x88},
	{0x3821,0x00},
	{0x3822,0x04},
	{0x3835,0x00},
	{0x4303,0x19},
	{0x4304,0x19},
	{0x4305,0x03},
	{0x4306,0x81},
	{0x4503,0x00},
	{0x4508,0x14},
	{0x450a,0x00},
	{0x450b,0x40},
	{0x4833,0x08},
	{0x5000,0xa9},
	{0x5001,0x09},
	{0x3b00,0x00},
	{0x3b02,0x00},
	{0x3b03,0x00},
	{0x3c80,0x08},
	{0x3c82,0x00},
	{0x3c83,0xb1},
	{0x3c87,0x08},
	{0x3c8c,0x10},
	{0x3c8d,0x00},
	{0x3c90,0x00},
	{0x3c91,0x00},
	{0x3c92,0x00},
	{0x3c93,0x00},
	{0x3c94,0x00},
	{0x3c95,0x00},
	{0x3c96,0x00},
	{0x3c97,0x00},
	{0x3c98,0x00},
	{0x4000,0xf3},
	{0x4001,0x60},
	{0x4002,0x00},
	{0x4003,0x40},
	{0x4090,0x14},
	{0x4601,0x10},
	{0x4701,0x00},
	{0x4708,0x09},
	{0x470a,0x00},
	{0x470b,0x40},
	{0x470c,0x81},
	{0x480c,0x12},
	{0x4710,0x06},
	{0x4711,0x00},
	{0x4837,0x12},
	{0x4800,0x00},
	{0x4c01,0x00},
	{0x5036,0x00},
	{0x5037,0x00},
	{0x580b,0x0f},
	{0x4903,0x80},
	{0x484b,0x05},
	{0x400a,0x00},
	{0x400b,0x90},
	{0x4003,0x40},
	{0x5000,0xf9},
	{0x5200,0x1b},
	{0x4837,0x16},
	{0x380e,0x04},	//04a0
	{0x380f,0x78},
	{0x3500,0x00},
	{0x3501,0x49},
	{0x3502,0x80},
	{0x3508,0x02},
	{0x3509,0x80},
	{0x3d8c,0x11},
	{0x3d8d,0xf0},
	{0x5180,0x00},
	{0x5181,0x10},
	{0x36a0,0x16},
	{0x36a1,0x50},
	{0x36a2,0x60},
	{0x36a3,0x80},
	{0x36a4,0x00},
	{0x36a5,0xa0},
	{0x36a6,0x00},
	{0x36a7,0x50},
	{0x36a8,0x00},
	{0x36a9,0x50},
	{0x36aa,0x00},
	{0x36ab,0x50},
	{0x36ac,0x00},
	{0x36ad,0x50},
	{0x36ae,0x00},
	{0x36af,0x50},
	{0x36b0,0x00},
	{0x36b1,0x50},
	{0x36b2,0x00},
	{0x36b3,0x50},
	{0x36b4,0x00},
	{0x36b5,0x50},
	{0x36b9,0xee},
	{0x36ba,0xee},
	{0x36bb,0xee},
	{0x36bc,0xee},
	{0x36bd,0x0e},
	{0x36b6,0x08},
	{0x36b7,0x08},
	{0x36b8,0x10},

};

#endif // _CAMERA_OV2732_H_
