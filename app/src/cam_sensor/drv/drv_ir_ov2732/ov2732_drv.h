#ifndef _OV2732_DRV_H_
#define _OV2732_DRV_H_
#include <stdio.h>
#include "iic_function.h"
#include "drv_isp.h"
//#include "test_hist.h"
#include "sensor_common.h"


#define OV2732_REG_CHIPID_H 0x300B
#define OV2732_REG_CHIPID_L 0x300C
#define OV2732_REG_STANDBY 	0x0100

typedef enum
{
    OV2732_NO_NEED_CONFIG = 0,
    OV2732_NEED_CONFIG = 1,
} ov2732_config_t;

#define OV2732_RGB_RSTN_INDEX	(0)
#define OV2732_IR_RSTN_INDEX	(11)

extern ov2732_config_t g_ov2732_config;

int ov2732_init(i2c_dev_t *i2c, uint32_t dev_addr);

int32_t ov2732_config(imageFormat_t cfg);
int switch_exposure_para(exposure_scene_t scene);
int set_spk_exposure_para(exposure_scene_t scene);
int32_t ov2732_config_reglist(uint32_t ir_mode, uint32_t addr, uint32_t data);
uint32_t ov2732_lookup_reg(uint32_t ir_mode, uint32_t addr);
int set_ov2732_reglist(regval_list reglist[], uint32_t reglist_len);
int ov2732_mode_config(sensor_mode_t mode);

#endif
