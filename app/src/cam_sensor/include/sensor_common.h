/*
 * Copyright (C) 2015-2017 Alibaba Group Holding Limited
 */

#ifndef SENSOR_COMMON_H
#define SENSOR_COMMON_H

typedef struct _regval_list {
    uint32_t addr;
    uint32_t data;
}regval_list;

typedef enum
{
    STANDBY = 0,
    STREAM  = 1,
}sensor_mode_t;

typedef enum{
    IIC_BUS_NUMBER_0,
    IIC_BUS_NUMBER_1,
    IIC_BUS_NUMBER_2,
    IIC_BUS_NUMBER_3,
}iic_bus_number_e;

typedef enum
{
    EXPOSURE_PARA_INDOOR = 0,
    EXPOSURE_PARA_OUTDOOR = 1,
} exposure_scene_t;

#endif /* SENSOR_COMMON_H */
