#ifndef _EEPROM_READ_DATA_H
#define _EEPROM_READ_DATA_H

#include <stdio.h>
#include "iic_function.h"

typedef struct {
	uint8_t name_type;
	uint8_t part_number;
	uint8_t year_number;
	uint8_t week_number;
	uint16_t product_ID;
} SN_inf_t;



uint32_t HexToNum(uint8_t *data,int size);
int eeprom_init(i2c_dev_t *i2c, uint32_t dev_addr);
int eeprom_read_data(i2c_dev_t *i2c,uint16_t begain_addr,uint16_t end_addr,uint8_t *data_image);
int eeprom_read_intri_param(double *param_data);
int eeprom_read_image_data(uint8_t  *image_data);

uint32_t eeprom_read_SN_data(void);


#endif