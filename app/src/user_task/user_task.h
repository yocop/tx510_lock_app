#ifndef __USER_TASK_H
#define __USER_TASK_H
#include <aos/kernel.h>
// #include <ipcm.h>

#include <mm_config.h>

//IPC


#define  QSPI_AIEINIT_DATA_BASEADDR    (0xc0000000 + 0x1000000 - 1024*1024)
// #define  ONE_FACE_FEATURE_SIZE         512//384
#define LIG_INT_NUM_TEST  150000
#define SRC_WIDTH  640
#define SRC_HEIGHT 360
#define DST_WIDTH  240
#define DST_HEIGHT 320
enum SHOW_MODE{
	ABGR =1,
	IR
};

enum DRAW_RECT_COLOUR{
	RED   =0x00ff0000,
	GREEN =0x0000ff00,
	BLUE  =0x000000ff,
	BLACK =0X00000000,
	WHITE =0x00ffffff
};

#define VERSION_CUSTOM  "VERSION: 20200717_1-T\r\n"

#define  FLASH_FIRMWARE    0  // download the firmware to flash 
#define  APP_USE_SCREEN    0 

#if FLASH_FIRMWARE
#define SENSER_POWER_GPIO         0
#define TFT_LCD_2P8               0
#else
#define SENSER_POWER_GPIO         1
#define TFT_LCD_2P8               1
#endif 
#define DPU_LCD_2P8               1

#define MONOCULAR_LOCK_INNO 	  1

 
struct AIE_FACE_MATCH_PARAM
{
	int pool_face_num;
	int face_feature_addr;
}__attribute__ ((packed));



void timer_gui_lable_reset(void);
int QSPI_flash_face_features_save(void);
void QSPI_flash_face_features_read(void);
void flash_data_move_task( void *arg);
int check_flash_data_move_done();
void check_memory_data_consistency();

int set_flash_data_move_done_flag(void);
void gui_lvgl_task(void *arg);
void user_task(void *arg);
void picture_conversion_task(void *arg);
void set_match_pool_num(int x);
int  get_match_pool_num(void);

extern int face_feature_load();
extern int face_feature_add(void *feature);
extern int face_feature_get(uint32_t i, void *feature);
extern int face_feature_delete(uint32_t i);
extern int face_feature_clear();
extern int face_feature_dump_id();

int readBasePic(void);
int writeFlashRegisterType(uint8_t * register_type);
int readFlashRegisterType(uint8_t * type_value);
#endif

