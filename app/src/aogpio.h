
#ifndef _AOGPIO_H_
#define  _AOGPIO_H_

#include "stdint.h"

#define AOGPIO_BASE            0xFFF02000
#define AO_GPIO_DR 0x0
#define AO_GPIO_DDR 0x4
#define AOGPIO_EXT_PORTA 0x50

typedef enum
{
    AO_GPIO_INPUT,
    AO_GPIO_OUTPUT,
} aogpio_direction_t;

typedef enum
{
    AO_GPIO_OUTPUT_LOW,
    AO_GPIO_OUTPUT_HIGH,
} aogpio_output_lvl_t;

typedef enum
{
    AO_GPIO_INPUT_LOW,
    AO_GPIO_INPUT_HIGH,
} aogpio_input_lvl_t;

extern void aogpio_set_direction(uint32_t pin_num, aogpio_direction_t dr);
extern void aogpio_write(uint32_t pin_num, aogpio_output_lvl_t pin_data);
extern uint32_t aogpio_read(uint32_t pin_num);

#endif