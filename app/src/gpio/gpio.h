#ifndef __GPIO_H
#define __GPIO_H

#include "drv_gpio.h"
#include "pin_name.h"

extern gpio_pin_handle_t pin_touch;
extern gpio_pin_handle_t pin_lock;

extern gpio_pin_handle_t pin_pir;
extern gpio_pin_handle_t pin_key;
extern gpio_pin_handle_t pin_power_down;
extern gpio_pin_handle_t pin_mipi_pwr;

extern gpio_pin_handle_t pin_speeker ;
extern gpio_pin_handle_t pin_lcd_cs ;
extern gpio_pin_handle_t pin_lcd_reset ;
extern gpio_pin_handle_t pin_lcd_scl;
extern gpio_pin_handle_t pin_lcd_sdi;

void gpio_set(void);

#endif
















