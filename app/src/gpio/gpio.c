#include "drv_gpio.h"  
#include "pin_name.h"
#include "gpio.h"
#include "user_task.h"

extern int ipc_aie_reg_send_req(int32_t image_ready);

gpio_pin_handle_t pin_touch = 0;
gpio_pin_handle_t pin_lock = 0;
gpio_pin_handle_t pin_pir = 0;
gpio_pin_handle_t pin_key = 0;
gpio_pin_handle_t pin_power_down = 0;
//gpio_pin_handle_t pin_mipi_pwr = 0;

gpio_pin_handle_t pin_speeker = 0;

//gpio_pin_handle_t pin_lcd_cs = 0;
//gpio_pin_handle_t pin_lcd_reset = 0;
//gpio_pin_handle_t pin_lcd_scl = 0;
//gpio_pin_handle_t pin_lcd_sdi = 0;
//gpio_pin_handle_t pin_lcd_pwr = 0;

gpio_pin_handle_t gpio_pin_handle;


static void reg_gpio_interrupt_handler(int idx)
{
   //printf("gpio_interrupt_handler\r\n");
   //ipc_aie_reg_send_req(0);
}

void gpio_set()
{
	gpio_pin_handle_t pin_mipi_pwr = 0;

#if 0
pin_speeker = csi_gpio_pin_initialize(PA1,0);	   //
		   csi_gpio_pin_config_mode(pin_speeker,GPIO_MODE_PULLDOWN);	//pulldown	open door
		   csi_gpio_pin_config_direction(pin_speeker, GPIO_DIRECTION_OUTPUT);
		   csi_gpio_pin_write(pin_speeker,0);	 //low level
#endif

		pin_lock  = csi_gpio_pin_initialize(PA3,0);
		csi_gpio_pin_config_mode(pin_lock, GPIO_MODE_PULLUP);
		csi_gpio_pin_config_direction(pin_lock, GPIO_DIRECTION_OUTPUT);
		csi_gpio_pin_write(pin_lock, 1);


		pin_key  = csi_gpio_pin_initialize(PA7,reg_gpio_interrupt_handler);
		csi_gpio_pin_config_mode(pin_key, GPIO_MODE_PULLDOWN);
		csi_gpio_pin_config_direction(pin_key, GPIO_DIRECTION_INPUT);
		csi_gpio_pin_set_irq(pin_key, GPIO_IRQ_MODE_RISING_EDGE, 1);	  //


		pin_power_down =    csi_gpio_pin_initialize(PA2, 0);
		csi_gpio_pin_config_mode(pin_power_down, GPIO_MODE_PULLDOWN);
		csi_gpio_pin_config_direction(pin_power_down, GPIO_DIRECTION_OUTPUT);
		csi_gpio_pin_write(pin_power_down, 1);
					
#if SENSER_POWER_GPIO
//liwei
		//Config GPIO0 <=> GPIOA0
		pin_mipi_pwr =  csi_gpio_pin_initialize(PA0, 0);
		csi_gpio_pin_config_mode(pin_mipi_pwr, GPIO_MODE_PULLUP);
		csi_gpio_pin_config_direction(pin_mipi_pwr, GPIO_DIRECTION_OUTPUT);
		csi_gpio_pin_write(pin_mipi_pwr, 1);
				
		//Config GPIO1 <=> GPIOA1  
		gpio_pin_handle = csi_gpio_pin_initialize(PA1, 0);
		csi_gpio_pin_config_mode(gpio_pin_handle, GPIO_MODE_PULLDOWN);
		csi_gpio_pin_config_direction(gpio_pin_handle, GPIO_DIRECTION_OUTPUT);
		csi_gpio_pin_write(gpio_pin_handle, true);

		//Config USI3_SCLK <=> GPIOB12  
		gpio_pin_handle = csi_gpio_pin_initialize(PB12, 0);
		csi_gpio_pin_config_mode(gpio_pin_handle, GPIO_MODE_PULLDOWN);
		csi_gpio_pin_config_direction(gpio_pin_handle, GPIO_DIRECTION_OUTPUT);
		csi_gpio_pin_write(gpio_pin_handle, true);
		//Config USI3_SD0 <=> GPIOB14  
		gpio_pin_handle = csi_gpio_pin_initialize(PB14, 0);
		csi_gpio_pin_config_mode(gpio_pin_handle, GPIO_MODE_PULLDOWN);
		csi_gpio_pin_config_direction(gpio_pin_handle, GPIO_DIRECTION_OUTPUT);
		csi_gpio_pin_write(gpio_pin_handle, true);
		//Config USI3_SD1 <=> GPIOB15  
		gpio_pin_handle = csi_gpio_pin_initialize(PB15, 0);
		csi_gpio_pin_config_mode(gpio_pin_handle, GPIO_MODE_PULLDOWN);
		csi_gpio_pin_config_direction(gpio_pin_handle, GPIO_DIRECTION_OUTPUT);
		csi_gpio_pin_write(gpio_pin_handle, true);
#endif
}




