
/**=========================================**
				Includes
**==========================================**/

#include "lv_user_api.h"
#include <aos/aos.h>


int lv_gui_start_flag = 0;
aos_mutex_t g_lvgl_lock;


/**==================================================================================================**
												Vars
**===================================================================================================**/


/**--------------lv_line线条---------------------*/
lv_obj_t * line1;

//坐标点集合
lv_point_t line_points[] = { {0, 0}, {0,0},{0,0},{0,0},{0, 0} }; //{ {10, 10}, {60,10},{60,60},{10,60},{10, 10} };//顺时针画框
lv_point_t line1_points[] = { {0, 0}, {0,0},{0,0},{0,0},{0, 0} }; //{ {10, 10}, {60,10},{60,60},{10,60},{10, 10} };//顺时针画框

//坐标点的个数
#define LINE_POINTS_NUM (sizeof(line_points)/sizeof(line_points[0]))


/**------------- lv_lablel标签---------------------*/
lv_obj_t* label1;
lv_obj_t* label2;

/**===================================================================================================**
										Funs dec
**====================================================================================================**/

static void task_gui_handler(void *arg);


/**===================================================================================================**
				Funs def
**====================================================================================================**/

/**
 * task_gui_hander任务
 * @param arg:TBD
 * @return void
 */


/**
 * lvgl初始化
 * @param:  void
 * @return: void
 */
void lv_gui_init(void)
{	
	// static lv_style_t text_style;

	/*lvgl 系统初始化 & lvgl显示接口初始化*/
	lv_port_disp_init();

	static lv_style_t h_style;
	static lv_style_t welcome_style;
	lv_style_copy(&h_style, &lv_style_transp);
	lv_style_copy(&welcome_style, &lv_style_transp);

	lv_obj_set_style(lv_layer_top(), &h_style);
	lv_obj_refresh_style(lv_layer_top());
	lv_obj_set_style(lv_layer_sys(), &h_style);
	lv_obj_refresh_style(lv_layer_sys());

	/*创建task_ui_hander任务*/
	aos_mutex_new(&g_lvgl_lock);
	aos_task_new("ui_handle", task_gui_handler, NULL, 10*1024);


	//1.创建自定义样式
	// static lv_style_t line_style;
	// lv_style_copy(&line_style, &lv_style_plain);
	// line_style.line.color = LV_COLOR_BLUE;//线条的颜色
	// line_style.line.width = 2;  //线条的厚度
	// line_style.line.rounded = 0;//线条的末端是否为圆角
	// //2.创建线条对象
	// line1 = lv_line_create(lv_layer_top(), NULL);//创建线条对象
	// lv_line_set_style(line1, LV_LINE_STYLE_MAIN, &line_style);//设置样式

	
	// lv_style_copy(&text_style, &lv_style_transp);
	
	// //创建label1标签,用来显示label2标签的长文本模式
	// label1 = lv_label_create(lv_layer_top(),NULL);//创建标签
	// lv_label_set_long_mode(label1,LV_LABEL_LONG_BREAK);//设置长文本模式
	// lv_obj_set_width(label1,360);//设置宽度,一定得放在lv_label_set_long_mode的后面,否则不起作用的
	// //lv_label_set_recolor(label1,true);//使能文本重绘色功能
	// //lv_label_set_text(label1,"#ff0000 Title:#mode");//设置文本,带有颜色重绘
	// text_style.text.font = &lv_font_roboto_28;
	// text_style.text.color = LV_COLOR_RED;
	// lv_label_set_style(label1, LV_LABEL_STYLE_MAIN, &text_style);
	// lv_clear_text();
	// aos_msleep(3);
	lv_gui_start_flag = 1;
}

/**
 * task_gui_hander任务
 * @param arg:TBD
 * @return :void
 */
static void task_gui_handler(void *arg) 
{
    while (1) {
        /* Periodically call the lv_task handler.
        * It could be done in a timer interrupt or an OS task too.*/
        //printf("lv_task_handler=========start\n");
		aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	    lv_task_handler();
		aos_mutex_unlock(&g_lvgl_lock);

		//printf("lv_task_handler=========end\n");
        aos_msleep(50);
    }
}





/**
 * 使用lv_line线条控件画框
 * @param:  方框的；两组顶点坐标
 * @return: void
 */
void lv_draw_frame_by_line(    lv_coord_t xmin, lv_coord_t xmax, lv_coord_t ymin,  lv_coord_t ymax)
{
	if(!lv_gui_start_flag)
		return ;
#if 0
	lv_points[0].x = xmin;
	lv_points[0].y = ymin;	

	lv_points[1].x = xmax;
	lv_points[1].y = ymin;	

	lv_points[2].x = xmax;
	lv_points[2].y = ymax;	

	lv_points[3].x = xmin;
	lv_points[3].y = ymax;	

	lv_points[4].x = xmin;
	lv_points[4].y = ymin;	
#else
	line_points[0].x = 0;
	line_points[0].y = 0;	

	line_points[1].x = xmax - xmin;
	line_points[1].y = 0;	

	line_points[2].x = xmax - xmin;
	line_points[2].y = ymax - ymin;	

	line_points[3].x = 0;
	line_points[3].y = ymax - ymin;	

	line_points[4].x = 0;
	line_points[4].y = 0;	

#endif
	//printf("%s face : xmin :%d, ymin :%d, xmax :%d, ymax :%d \n",__func__, xmin, ymin,xmax,ymax);	

	//lv_obj_set_pos(line1,60,140);//设置坐标	
	aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	lv_obj_align(line1, NULL, LV_ALIGN_IN_TOP_LEFT,0,0);
	lv_obj_set_pos(line1,xmin,ymin);//设置坐标	
	
	//使能大小自适应,当然了,你也可以不调用,因为默认就是被使能了的
	lv_line_set_auto_size(line1,true);
	//设置坐标点集合,同时也会在此内部计算出线条对象的大小
	lv_line_set_points(line1, line_points, LINE_POINTS_NUM);
	aos_mutex_unlock(&g_lvgl_lock);
}

/**
 * 删除lv_line线条控件画的框
 * @param:  void
 * @return: void
 */
void lv_clear_frame(void)
{
	if(!lv_gui_start_flag)
		return ;
	 //lv_obj_del(line1);//立即删除线条对象

	aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	lv_line_set_points(line1, line1_points, LINE_POINTS_NUM);
	aos_mutex_unlock(&g_lvgl_lock);
}




/**
 * 使用lv_lable标签控件写字符串
 * @param:  text文本
 * @return: void
 */
void lv_write_text_by_lable(lv_coord_t x, lv_coord_t y,const char * text)
{
	char buf[50];
	//static uint16_t id = 0;
	
	sprintf(buf,"%s",text);
	//sprintf(buf,"%s%d",text, id);

	aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	//lv_label_set_text(label1,text);//设置文本
	lv_label_set_text(label1,buf);//设置文本
	//lv_label_set_align(label1,LV_LABEL_ALIGN_CENTER);//文本居中对齐
	//lv_label_set_style(label1,LV_LABEL_STYLE_MAIN,&lv_style_plain_color);//设置主背景样式
	//lv_label_set_body_draw(label1,true);//使能背景绘制
	//注意:设置与屏幕的对齐方式,这个接口也是跟调用位置有关系的,最好放到后面调用,因为放的太前调用的话,
	//在那时标签对象的大小可能还是未知的,此时lv_obj_align接口就没办法算出对齐之后的坐标,从而也就
	//达不到我们所要的对齐效果
	//lv_obj_align(label1,NULL,LV_ALIGN_IN_BOTTOM_LEFT,0,0);
	lv_obj_set_pos(label1,x,y);
	aos_mutex_unlock(&g_lvgl_lock);

	printf("==%s=\n", __func__);

}

/**
 * 使用lv_lable标签控件写字符串&id
 * @param:  text文本, id 序号
 * @return: void
 */
void lv_write_text_id_by_lable(lv_coord_t x, lv_coord_t y,const char * text, uint32_t id)
{
	char buf[50];
	
	//sprintf(buf,"%s",text);
	sprintf(buf,"%s%u",text, id);
	

	aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	//lv_label_set_text(label1,text);//设置文本
	lv_label_set_text(label1,buf);//设置文本
	//lv_label_set_align(label1,LV_LABEL_ALIGN_CENTER);//文本居中对齐
	//lv_label_set_style(label1,LV_LABEL_STYLE_MAIN,&lv_style_plain_color);//设置主背景样式
	//lv_label_set_body_draw(label1,true);//使能背景绘制
	//注意:设置与屏幕的对齐方式,这个接口也是跟调用位置有关系的,最好放到后面调用,因为放的太前调用的话,
	//在那时标签对象的大小可能还是未知的,此时lv_obj_align接口就没办法算出对齐之后的坐标,从而也就
	//达不到我们所要的对齐效果
	//lv_obj_align(label1,NULL,LV_ALIGN_IN_BOTTOM_MID,0,0);
	lv_obj_set_pos(label1,x,y);
	aos_mutex_unlock(&g_lvgl_lock);

}

/**
 * 使用lv_lable标签控件写字符串&id
 * @param:  text文本, id 序号,time
 * @return: void
 */
void lv_write_text_id_time_by_lable(lv_coord_t x, lv_coord_t y,const char * text, uint32_t id,uint32_t time)
{
	char buf[50];
	
	//sprintf(buf,"%s",text);
	sprintf(buf,"%s %u %ums",text, id, time);
	
	aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	//lv_label_set_text(label1,text);//设置文本
	lv_label_set_text(label1,buf);//设置文本
	//lv_label_set_align(label1,LV_LABEL_ALIGN_CENTER);//文本居中对齐
	//lv_label_set_style(label1,LV_LABEL_STYLE_MAIN,&lv_style_plain_color);//设置主背景样式
	//lv_label_set_body_draw(label1,true);//使能背景绘制
	//注意:设置与屏幕的对齐方式,这个接口也是跟调用位置有关系的,最好放到后面调用,因为放的太前调用的话,
	//在那时标签对象的大小可能还是未知的,此时lv_obj_align接口就没办法算出对齐之后的坐标,从而也就
	//达不到我们所要的对齐效果
	//lv_obj_align(label1,NULL,LV_ALIGN_CENTER,0,0);
	lv_obj_set_pos(label1,x,y);
	aos_mutex_unlock(&g_lvgl_lock);
}


/**清除字幕
 * @param:  void
 * @return: void
 */
void lv_clear_text(void)
{
	aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	lv_label_set_text(label1,"");//设置文本
	aos_mutex_unlock(&g_lvgl_lock);
}

extern lv_font_t myfont_puhui6;
lv_obj_t * lv_app_notimce_add(int x, int y,const char * format,...)
{
	char txte[50];

	va_list va;
	va_start(va, format);
	vsprintf(txte,format, va);
	va_end(va);

	static lv_style_t  mbox_style;
	lv_obj_t * mbox;

	aos_mutex_lock(&g_lvgl_lock, AOS_WAIT_FOREVER);
	mbox = lv_mbox_create(lv_scr_act(), NULL);
	lv_style_copy(&mbox_style,lv_mbox_get_style(mbox, LV_MBOX_STYLE_BG));

	mbox_style.text.font = &myfont_puhui6;
	// mbox_style.text.font = &lv_font_roboto_28;
	mbox_style.text.color = LV_COLOR_RED;

	mbox_style.body.opa = 255;
	mbox_style.body.grad_color=LV_COLOR_YELLOW;

	lv_obj_set_style(mbox, &mbox_style);
	lv_mbox_set_recolor(mbox,true);

	lv_mbox_set_text(mbox,txte);
	// void lv_mbox_set_text(lv_obj_t * mbox, const char * txt)
	lv_obj_align(mbox,NULL,LV_ALIGN_IN_TOP_LEFT,x,y);
	//lv_obj_set_pos(mbox,x,y);
	lv_mbox_set_anim_time(mbox,0);
	lv_mbox_start_auto_close(mbox,2000);
	aos_mutex_unlock(&g_lvgl_lock);

    return mbox;
}


// void lv_app_notimce_clear(void)
// {
// 	lv_label_set_text(mbox,"");//设置文本
// }
