
#ifndef LV_USER_API_H
#define LV_USER_API_H


#include <stdio.h>
#include "aos/kernel.h"
#include "lv_port_disp.h"

#include "../lv_core/lv_obj.h"
#include "../lv_core/lv_style.h"
#include "../lv_misc/lv_area.h"
#include "../lv_misc/lv_color.h"



/**
 * lvgl初始化
 * @param:  void
 * @return: void
 */
void lv_gui_init(void);

/**
 * 使用lv_line线条控件画框
 * @param:  方框的；两组顶点坐标
 * @return: void
 */
void lv_draw_frame_by_line(lv_coord_t xmin, lv_coord_t xmax, lv_coord_t ymin,  lv_coord_t ymax);

/**
 * 删除lv_line线条控件画的框
 * @param:  void
 * @return: void
 */
void lv_clear_frame(void);

/**
 * 使用lv_lable标签控件写字符串
 * @param:  text文本
 * @return: void
 */
void lv_write_text_by_lable(lv_coord_t x, lv_coord_t y,const char * text);

/**
 * 使用lv_lable标签控件写字符串&id
 * @param:  text文本, id 序号
 * @return: void
 */
void lv_write_text_id_by_lable(lv_coord_t x, lv_coord_t y,const char * text, uint32_t id);


/**
 * 使用lv_lable标签控件写字符串&id
 * @param:  text文本, id 序号,time
 * @return: void
 */
void lv_write_text_id_time_by_lable(lv_coord_t x, lv_coord_t y,const char * text, uint32_t id,uint32_t time);

/**清除字幕
 * @param:  void
 * @return: void
 */
void lv_clear_text(void);

lv_obj_t * lv_app_notimce_add(int x, int y,const char * format,...);


#endif /*LV_USER_API_H*/

