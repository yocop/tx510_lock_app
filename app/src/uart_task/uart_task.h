#ifndef _UART_TASK_H_
#define _UART_TASK_H_

#include "ipcm.h"

void uart_task(void *paras);
extern uint8_t msg_check_sum(uint8_t *inbuf,uint8_t size);
extern aos_sem_t 	cpu805_ipc_cmd_recv;


#endif