
#ifndef _USBD_TSM_H_
#define _USBD_TSM_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct product_info{
        unsigned char sn[14];
        unsigned char hwid[3];
        unsigned char switchV;
        unsigned char hwboardid;
        unsigned int deviceType;
        unsigned char wifi_mac[6];
        unsigned char bt_mac[6];
        unsigned char eth_mac[6];

}product_info_s;

void ftempl_func(int port, int notification);

void ftempl_usb_task(void *paras);

#ifdef __cplusplus
}
#endif

#endif
