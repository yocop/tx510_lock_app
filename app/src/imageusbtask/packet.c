#include <string.h>
#include <aos/kernel.h>
#include "packet.h"
#include <stdint.h>
#include "uart_packet.h"

#if 0
void store_le_uint32(uint8_t *addr, uint32_t value) /* big endian  */
{
    addr[0] = value >> 24;
    addr[1] = (value >> 16) & 0xff;
    addr[2] = (value >> 8) & 0xff;
    addr[3] = value & 0xff;
}

void store_le_uint16(uint8_t *addr, uint16_t value) /* big endian */
{
    addr[0] = value >> 8; 
    addr[1] = value & 0xff;
}

void dump_le_uint32(uint8_t *addr, uint32_t *value)   /* big endian to little endian*/
{
	value = 0;
    *value = addr[0] << 24;
	*value |= addr[1] << 16;
	*value |= addr[2] << 24;
	*value |= addr[3];
}

void dump_le_uint16(uint8_t *addr, uint16_t *value) /* big endian to little endian*/
{
	*value = 0;
	*value = addr[0]<<8;
	*value |= addr[1];
}


uint32_t dump_uint32(uint8_t *addr)   /*little endian to little endian*/
{
	uint32_t value = 0;
    value = addr[3] << 24;
	value |= addr[2] << 16;
	value |= addr[1] << 24;
	value |= addr[0];
	return value;
}
#endif 

void pack_head(uint8_t cmd, uint32_t id, uint32_t payload_len, uint8_t *packet)
{
    req_pack_head_t *req = (req_pack_head_t *)packet;
    req->head[0] = 'T';
    req->head[1] = 'S';
    req->head[2] = 'M';
    req->cmd = cmd;
    store_le_uint32(req->id, id);
    store_le_uint32(req->len, payload_len);
    
    return ;
}

void pack_resp_head(uint8_t cmd, uint8_t result, uint32_t id, uint32_t payload_len, uint8_t *packet)
{
    resp_pack_head_t *resq = (resp_pack_head_t *)packet;
    resq->head[0] = 'T';
    resq->head[1] = 'S';
    resq->head[2] = 'M';
    resq->cmd = cmd;
	resq->result = result;
    store_le_uint32(resq->id, id);
    store_le_uint32(resq->len, payload_len);
    
    return ;
}



int judge_pack_cmd(uint8_t *recv)
{
	int ret = -1;
	
	//uint8_t *packet;
    resp_pack_head_t* resp = (resp_pack_head_t *)recv;
	if (resp->head[0] == 'T' && resp->head[1] == 'S' && resp->head[2] == 'M'
        && GET_RESP_RESULT(resp) == RESULT_SUCCESS)
    return  0;
	
	return ret;
}


int send_packet(unsigned char *packet, uint32_t len)
{
//    int ret;   
//    int i = 0; 
//
//    uint32_t send_times = ROUND_DOWN_TIMES(len, FRAME_LEN);
//    for(i = 0; i < send_times; i++) {
//        ret = usb_write((char*)packet + i * FRAME_LEN, FRAME_LEN, TIMEOUT);
//        if (ret < 0 || ret != FRAME_LEN)
//            return ESENDPACK;
//    }
//    uint32_t left_len = len - i * FRAME_LEN;
//    if (left_len) {
//        ret = usb_write((char*)packet + i * FRAME_LEN, left_len, TIMEOUT);
//        if (ret < 0 || ret != left_len)
//            return ESENDPACK;
//    }
//    
    //return SUCCESS;
    return 0;
}

int get_result(uint8_t *send, uint8_t *recv)
{   
//    int ret;
//    uint8_t *packet;
//	resp_pack_head_t* resp = (resp_pack_head_t *)recv;
//
//    pack_head(CMD_GET_RESULT, 0, 0, send);
//    while(1) {
//		send_packet(send, GET_REQ_LEN(send));
//        ret = usb_read((char*)resp, FRAME_LEN, TIMEOUT);
//        if (ret < 0)
//            return EGETPACK;
//        
//		if (resp->head[0] == 'T' && resp->head[1] == 'S' && resp->head[2] == 'M'
//            && GET_RESP_RESULT(resp) == RESULT_SUCCESS)
//            break;    
//    }
//
    //return SUCCESS;
    return 0;
}
