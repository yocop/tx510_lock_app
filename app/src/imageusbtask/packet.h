#ifndef PACKET_H_
#define PACKET_H_

#include <stdint.h>
#define TIMEOUT     1000
#define ESENDPACK   -21
#define EGETPACK    -22


//#define TX510_EG_360_FACTORY_MODE 

#define RESP_IMAGE_HEAD_SIZE  (13+6)

typedef struct {
    uint8_t head[3];
    uint8_t cmd;
    uint8_t id[4];
    uint8_t len[4];
    uint8_t dat[0];   
} req_pack_head_t;


typedef struct {
    uint8_t ch[2];
    uint8_t height[2];
    uint8_t width[2];
    uint8_t dat[0];
} pic_payload_t;

typedef struct {
    uint8_t head[3];
    uint8_t cmd;
    uint8_t result;
    uint8_t id[4];
    uint8_t len[4];
    uint8_t dat[0];   
} resp_pack_head_t;

typedef struct{
    uint8_t faceid[4];
    uint8_t score[2];
    uint8_t qvalue[2];
}pic_score_t;


typedef struct{
	uint8_t type;
	uint8_t pixs;
	uint8_t ch[2];
	uint8_t height[2];
	uint8_t width[2];
}img_resolution_t;

typedef struct{
	img_resolution_t ir;
	img_resolution_t rgb;
	img_resolution_t depth;
}img_param_payload_t;

#if defined(TX510_EG_360_FACTORY_MODE)

#define CMD_TEST_ELECTRIC_OFF  0x1
#define CMD_TEST_ELECTRIC_ON   0x2
#define CMD_TEST_SN            0x3
#define CMD_TEST_LCD           0x4
#define CMD_TEST_CAMERA        0x5
#define CMD_TEST_LED           0x6
#define CMD_TEST_BT_WIFI       0x7
#define CMD_TEST_NETWORK       0x8
#define CMD_TEST_DATA          0x9
#define CMD_TEST_SENSOR        0xA
#define CMD_TEST_MIC_SPEAKER   0xB
#define CMD_TEST_PREVENT       0xC
#define CMD_TEST_USB_OTG       0xD
#define CMD_TEST_WG            0xE
#define CMD_TEST_WARNING       0xF
#define CMD_TEST_CONNECT       0x10
#define CMD_TEST_UART          0x11
#define CMD_TEST_GPIO          0x12
#define CMD_GET_RESULT         0xC3

#define CMD_REGISTER       0x13
#define CMD_RECOGNISE      0x14
#define CMD_GET_RESULT_1     0x15
#define CMD_GET_DEPTH_IMG  0x16
#define CMD_GET_ALL_IMG    0x17
#define CMD_GET_IR_IMG     0x18
#define CMD_GET_RGB_IMG    0x19
#define CMD_SET_IMG_PARAM  0x1a
#define CMD_GET_ALL_IMG_SPK 0x1b
#define CMD_SET_SN          0x1c
#define CMD_GET_DEPTH_PIC   0x20
#define CMD_SET_IMA_SOURCE  0x21

#else 

//#define CMD_REGISTER       0x1
//#define CMD_RECOGNISE      0x2
//#define CMD_GET_RESULT     0x3
//#define CMD_GET_DEPTH_IMG  0x4
//#define CMD_GET_ALL_IMG    0x5
//#define CMD_GET_IR_IMG     0x6
//#define CMD_GET_RGB_IMG    0x7
//#define CMD_SET_IMG_PARAM  0x8
//#define CMD_GET_ALL_IMG_SPK 0x9
//#define CMD_SET_SN          0xb
//#define CMD_GET_DEPTH_PIC   0x20
//#define CMD_SET_IMA_SOURCE  0x21
//#define CMD_SET_IMA_CAL     0xF5
//#define CMD_SET_IMA_DEFAULT 0xF6

#define CMD_REGISTER       0x1
#define CMD_RECOGNISE      0x2
#define CMD_GET_RESULT     0x3
#define CMD_GET_DEPTH_IMG  0x4
#define CMD_GET_ALL_IMG    0x5
#define CMD_GET_IR_IMG     0x6
#define CMD_GET_RGB_IMG    0x7
#define CMD_SET_IMG_PARAM  0x8
#define CMD_GET_ALL_IMG_SPK 0x9
#define CMD_HANDSHAKE_REQ    0x10
#define CMD_HANDSHAKE_RES    0x30
#define CMD_BIN_INFO_REQ     0x11
#define CMD_BIN_INFO_RES     0x31
#define CMD_TRANSPORT_REQ    0x12
#define CMD_TRANSPORT_RES    0x32
#define CMD_FLASH_REQ        0x13
#define CMD_FLASH_RES        0x33
#define CMD_RESET_REQ        0x14
#define CMD_RESET_RES        0x34
#define CMD_SET_SN          0xb
#define CMD_GET_DEPTH_PIC   0x20
#define CMD_SET_IMA_SOURCE  0x21
#define CMD_SET_IMA_CAL     0xF5
#define CMD_SET_IMA_DEFAULT 0xF6



#endif 

#define RESULT_NOT_READY   0xF0
#define RESULT_SUCCESS     0xF1
#define RESULT_FAILED      0xF2


#define GET_REQ_CMD(req)    ((((req_pack_head_t*)req)->cmd))
#define GET_REQ_LEN(req)    (((((req_pack_head_t*)req)->len[0]) << 24) \
                            + ((((req_pack_head_t*)req)->len[1]) << 16) \
                            + ((((req_pack_head_t*)req)->len[2]) << 8) \
                            + (((req_pack_head_t*)req)->len[3]))
                            
#define GET_REQ_ID(req)     (((((req_pack_head_t*)req)->id[0])<<24)+((((req_pack_head_t*)req)->id[1])<<16) \
                            + ((((req_pack_head_t*)req)->id[2]) << 8) + (((req_pack_head_t*)req)->id[3]))
                            
#define GET_REQ_CH(req)    (((((req_pack_head_t*)req)->dat[0]) << 8) + (((req_pack_head_t*)req)->dat[1]))

#define GET_REQ_H(req)     (((((req_pack_head_t*)req)->dat[2])<<8) + (((req_pack_head_t*)req)->dat[3]))

#define GET_REQ_W(req)     (((((req_pack_head_t*)req)->dat[4])<<8)+(((req_pack_head_t*)req)->dat[5]))

                                


#define GET_RESP_CMD(resp)  	((((resp_pack_head_t*)resp)->cmd))
#define GET_RESP_RESULT(resp) 	((((resp_pack_head_t*)resp)->result))
#define GET_RESP_LEN(resp)  	(sizeof(resp_pack_head_t) + ((((resp_pack_head_t*)resp)->len[0]) << 24) \
                                + ((((resp_pack_head_t*)resp)->len[1]) << 16) + ((((resp_pack_head_t*)resp)->len[2]) << 8) \
                                + (((resp_pack_head_t*)resp)->len[3]))




//void store_le_uint32(uint8_t *addr, uint32_t value);
//void store_le_uint16(uint8_t *addr, uint16_t value);
//void dump_le_uint32(uint8_t *addr, uint32_t *value);
//void dump_le_uint16(uint8_t *addr, uint16_t *value);

void pack_head(uint8_t cmd, uint32_t id, uint32_t payload_len, uint8_t *packet);
//int send_packet(uint8_t *packet, uint32_t len);
//int get_result(uint8_t *send, uint8_t *recv);
void pack_resp_head(uint8_t cmd, uint8_t result, uint32_t id, uint32_t payload_len, uint8_t *packet);



#endif
