#include "stdio.h"
#include "aogpio.h"
#include <aos/kv.h>
#include "if_v.h"
#include "lv_port_disp.h"
#include "drv_delay.h"
#include "cx_cloud.h"
#include "cx_record.h"
#include "cx_facerecog.h"

typedef unsigned int u32;
typedef unsigned int u16;
extern void wr(u32 addr, u32 data);
extern u32 rd(u32 addr);
extern uint32_t g_ulIfvDebugEnable;

void aogpio_set_direction(uint32_t pin_num, aogpio_direction_t dr)
{
    uint32_t data;
    data = rd(AOGPIO_BASE + AO_GPIO_DDR);

    if (AO_GPIO_OUTPUT == dr)
    {
        wr(AOGPIO_BASE + AO_GPIO_DDR, data | (0x01 << pin_num));
    }
    else //in put
    {
        wr(AOGPIO_BASE + AO_GPIO_DDR, data & (~(0x01 << pin_num)));
    }
}

void aogpio_write(uint32_t pin_num, aogpio_output_lvl_t pin_data)
{
    volatile uint32_t data;
    data = rd(AOGPIO_BASE + AO_GPIO_DR);
    
    if (AO_GPIO_OUTPUT_LOW == pin_data)
    {
        wr(AOGPIO_BASE + AO_GPIO_DR, data & (~(0x01 << pin_num)));
    }
    else
    {
        wr(AOGPIO_BASE + AO_GPIO_DR, data | (0x01 << pin_num));
    }
}

uint32_t aogpio_read(uint32_t pin_num)
{
    uint32_t data;
    data = rd(AOGPIO_BASE + AOGPIO_EXT_PORTA);

    return (data >> pin_num) & 0x01;
}

#define REGISTER_KEY_DETECT_PIN 3
#define REGISTER_KEY_DETECT_PIN_HIGH  1
#define REGISTER_KEY_DETECT_PIN_LOW  0
extern uint8_t register_by_ir;
extern uint32_t register_ir_flag;

void button_check_entry(void *paras)
{
    static int time_count;
    static int button_flag = 0;
    static int button_flag_last = 0;
    static int intercom_state = CX_CLOUD_DIR_FROM_CLOUD; 
    aogpio_set_direction(REGISTER_KEY_DETECT_PIN, AO_GPIO_INPUT);
    while(1) {
        button_flag = aogpio_read(REGISTER_KEY_DETECT_PIN);
        if((button_flag == 1) && (intercom_state == CX_CLOUD_DIR_FROM_CLOUD)) {
            cx_cloud_switch_intercom(cx_record_get_handle(), CX_CLOUD_DIR_TO_CLOUD);
            intercom_state = CX_CLOUD_DIR_TO_CLOUD;
        } else if((button_flag == 0) && (intercom_state == CX_CLOUD_DIR_TO_CLOUD)) {
            cx_cloud_switch_intercom(cx_record_get_handle(), CX_CLOUD_DIR_FROM_CLOUD);
            intercom_state = CX_CLOUD_DIR_FROM_CLOUD;
        }
        
        if(button_flag == 1) {
            time_count += 100;
        } else {
            if((time_count > 0) && (time_count < 1000)) {
               cx_facerecog_user_verify(0);
            } else if((time_count >= 1000) && (time_count < 3000)) {
                cx_facerecog_user_enroll(0, 0);
            }
            time_count = 0;
        }
        button_flag_last = button_flag;
        aos_msleep(100);
    }
}

void button_face_feature(void *paras)
{
	int count =0;	
    int32_t ret = 0;
    aogpio_set_direction(REGISTER_KEY_DETECT_PIN, AO_GPIO_INPUT);

	//read key detect
	while(1)
    {
        while( 1 ){
            #ifdef AOS_BOARD_TX510_LOCK_NC
             if( aogpio_read(REGISTER_KEY_DETECT_PIN) == REGISTER_KEY_DETECT_PIN_LOW)
            #else
             if( aogpio_read(REGISTER_KEY_DETECT_PIN) == REGISTER_KEY_DETECT_PIN_HIGH )
            #endif
            {
                count++;
                if(count >= 100)
                    break;
            }
            else
            {
                break;
            }
            
            aos_msleep(100);
        }

        /*if (count >= 100)
        {
            if (!g_ulIfvDebugEnable)
            {
                g_ulIfvDebugEnable = true;
                ret = aos_kv_set(IFV_CLI_DEBUG, &g_ulIfvDebugEnable, sizeof(g_ulIfvDebugEnable), 1);
                if (ret != 0) 
                {
                    printf("Failed to enable ifv debug\r\n");
                }
                //lv_write_myFontsimsun28_text("Debug Mode");
            }
            else
            {
                g_ulIfvDebugEnable = false;
                ret = aos_kv_set(IFV_CLI_DEBUG, &g_ulIfvDebugEnable, sizeof(g_ulIfvDebugEnable), 1);
                if (ret != 0) 
                {
                    printf("Failed to disable ifv debug\r\n");
                }
                //lv_write_myFontsimsun28_text("Demo Mode");
            }
        }*/
        if (count >= 50)
        {
            printf("clear all users!\n");
            ret = tsm_ff_clear();
            printf("clear all finished!\n");
            if (ret < 0)
            {
                printf("Failed to clear face feature %d\r\n", ret);
            }
            else
            {
                //lv_write_text_by_lable("All Cleared");
                //lv_write_myFontsimsun28_messageid(FACERECOG_MSG_CLEAR_ALL_FACE, 0);
                lv_clear_text();
                lv_write_text_by_lable(0, 0, "clear all users!");
                printf("Success to clear face feature %d\r\n", ret);
                //codec_send(clear_done, sizeof(clear_done)/sizeof(char));
                //mdelay(1000);
                aos_msleep(2000);
                lv_clear_text();
            }
        }
        else if (count >= 10)
        {
            //if_vSaveIrToFile();
            mdelay(2000);
        }
        else if (count >= 1)
        {
            //aie_vis_detect_enable = 1;
            printf("button register!\n");
            if(register_by_ir==0){
                ret = if_vRegister(NULL);
                if (ret < 0)
                {
                    printf("Failed to register face feature %d\r\n", ret);
                }
                else
                {
                    printf("Success to register face feature %d\r\n", ret);
                }
            }
            else{
                printf("register by ir!\n");
				ifv_printf(IFV_MSG_DEBUG,"===== ir register=====\n");
				register_ir_flag = 1;
                extern void if_vRecognition_process(unsigned char * card_number, app2if_v_cb *info,  int timeout);
				if_vRecognition_process(NULL, NULL, 5);
            }
        }
        
        count = 0;
        aos_msleep(100);
    }   
}