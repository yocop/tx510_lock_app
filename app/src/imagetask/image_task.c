/*
 * Copyright (C) 2015-2017 Alibaba Group Holding Limited
 */
#include <stdio.h>
#include <aos/kernel.h>
#include <csky_common.h>
#include "drv_isp.h"
#include <pin.h>
#include "drv_gpio.h"   
#include "pin_name.h" 
//#include <assert.h>
#include "cam_engine_api.h"
#include "cam_engine_aaa_api.h"
#include "hal/hal_api.h" 
#include "ulog/ulog.h"
#include "aos/hal/i2c.h"
#include "aos/hal/mipi_csi.h"
#include "camera_config.h"
#include "mm_config.h"
#include "drv_isp.h"
#include "cam_engine_ctrl.h"
#include "image_proc.h"
#include "iic_function.h"

#include "isp_function.h"
#include "isp_irq_cb.h"
#include "mipi_function.h"
#include "image_task.h"

#include "wrapper.h"
#include "image_proc.h"
#include "mm_config.h"
#include "if_v.h"
// #include "test_hist.h" 
// #include "ov9282_drv.h"
// #include "ov9732_vis_drv.h"
#include "irp_jp.h"
#include "iic_function.h"
#include "lcd.h"
#include "image_proc.h"

#define  CK_IIC_SLAVE_ADDR_9282      0x60
#define  CK_IIC_SLAVE_ADDR_9732      0x10
#define  CK_IIC_SLAVE_ADDR_IRLED     0x63
#define  CK_IIC_SLAVE_ADDR_XC7022    0x1B
#define  CK_IIC_SLAVE_ADDR_AR0230    0x10
// static i2c_dev_t i2c_usi_10;
// static i2c_dev_t i2c_usi_11;

extern void mdelay(uint32_t ms);
extern void udelay(uint32_t us);

extern exposure_scene_t g_cur_para;

extern  volatile uint32_t ispProcFlag[ISP_NUM];
extern  volatile uint32_t curVal[ISP_NUM];

extern volatile int isp_mi_event[ISP_NUM];

#define IMG_DEPTH_SIZE   (640 * 360 *2)
#define IMG_INPUT_SIZE   (640 * 360)
//****************************************
#define IMG_FACE_HOR_SIZE  640
#define IMG_FACE_VER_SIZE  360

// #define FACE_FEATURE_SIZE  384
#define FACE_FEATURE_NUM   5
#define FACE_MATCH_THRES   0.71f

/* 3D calibrated parameters  */
volatile uint32_t ai_start_flag = 0;
volatile static uint32_t ai_start_register = 0;

CamEngineHandle_t hCamEngine0, hCamEngine1;

//photo rgb and ir
uint32_t phase1_allow_recv = 1;
uint32_t phase2_allow_recv = 0;

uint32_t *g_fb_rgb_cur  = NULL;
uint32_t *g_fb_ir_cur = NULL;
uint32_t *g_fb_spk_right_cur = NULL;
uint32_t *g_fb_spk_left_cur  = NULL;

//volatile uint32_t g_usb_image_type = USB_IMAGE_TYPE_NONE;
volatile uint32_t g_snapshot_mode = SNAPSHOT_MODE_NONE;
volatile uint32_t g_snapshot_done = 0;
volatile uint32_t g_snapshot_3in1_mode = SNAPSHOT_MODE_3IN1_NONE;

//volatile uint32_t g_rgb_mode = CFG_MODE_RGB_1080P_YUV; 
volatile uint32_t g_rgb_mode = CFG_MODE_RGB_640X360_RGB; 
volatile uint32_t g_ir_mode = CFG_MODE_IR_640x360_RAW;

volatile int g_rgb_img_ready = 0;
volatile int g_ir_img_ready = 0;
volatile int g_spk1_img_ready = 0;
volatile int g_spk2_img_ready = 0;
volatile int g_face_found = 0;
volatile int g_noface_count = 0;

uint32_t g_fb_buffer_group_id  = ISP1_BUFFER_GROUP_0;

uint32_t *g_fb_rgb[ISP1_BUFFER_GROUP_NUM]           = {0};
uint32_t *g_fb_ir[ISP1_BUFFER_GROUP_NUM]            = {0};
uint32_t *g_fb_spk_right[ISP1_BUFFER_GROUP_NUM]     = {0};
uint32_t *g_fb_spk_left[ISP1_BUFFER_GROUP_NUM]      = {0};

static gpio_pin_handle_t pin_mipi_pwr = 0;
gpio_pin_handle_t gpio_pin_handle;
gpio_pin_handle_t gpio_pin_usi3_sd1_handle;

// static uint32_t mode_change_en = false;
// static uint32_t mode_set = SNAPSHOT_MODE_NONE;

extern void rgb_720P_yuv_2_640x360_rgb(void);
extern void rgb_640x360_rgb_2_720P_yuv(void);
extern int32_t led_check_enable(void);

// static const CamEnginePathConfig_t defaultMpConfig = { 1280U, 720U,
//             CAMERIC_MI_DATAMODE_DISABLED, CAMERIC_MI_DATASTORAGE_SEMIPLANAR };

// static const CamEnginePathConfig_t defaultSpConfig = { 640U, 360U,	// resize to 640x360
//             CAMERIC_MI_DATAMODE_RGB888, CAMERIC_MI_DATASTORAGE_INTERLEAVED };

void example_pin_camera_iic0_init(void)
{
	//drv_pinmux_config(EXAMPLE_PIN_CAMERA_IIC_SDA, EXAMPLE_PIN_CAMERA_IIC_SDA_FUNC);
	//drv_pinmux_config(EXAMPLE_PIN_CAMERA_IIC_SCL, EXAMPLE_PIN_CAMERA_IIC_SCL_FUNC);
}

void example_pin_camera_iic1_init(void)
{
//	drv_pinmux_config(EXAMPLE_PIN_IIC_SLAVE_SDA, EXAMPLE_PIN_IIC_SLAVE_SDA_FUNC);
//	drv_pinmux_config(EXAMPLE_PIN_IIC_SLAVE_SCL, EXAMPLE_PIN_IIC_SLAVE_SCL_FUNC);
}


/* process the key for register */
static void reg_gpio_interrupt_handler(int idx)
{
   printf("gpio_interrupt_handler\r\n");
   
}

void gpio_init(void)
{
    gpio_pin_handle_t pin_key = 0;
    pin_key  = csi_gpio_pin_initialize(PA7,reg_gpio_interrupt_handler);
	csi_gpio_pin_config_mode(pin_key, GPIO_MODE_PULLDOWN);
	csi_gpio_pin_config_direction(pin_key, GPIO_DIRECTION_INPUT);
	csi_gpio_pin_set_irq(pin_key, GPIO_IRQ_MODE_RISING_EDGE, 1);	  //
    /* sensor power */
    //Config GPIO0 <=> GPIOA0
    pin_mipi_pwr =  csi_gpio_pin_initialize(PA0, 0);
    csi_gpio_pin_config_mode(pin_mipi_pwr, GPIO_MODE_PULLUP);
    csi_gpio_pin_config_direction(pin_mipi_pwr, GPIO_DIRECTION_OUTPUT);
    csi_gpio_pin_write(pin_mipi_pwr, 1);
	/* sensor power */		
    //Config GPIO1 <=> GPIOA1  
    gpio_pin_handle = csi_gpio_pin_initialize(PA1, 0);
    csi_gpio_pin_config_mode(gpio_pin_handle, GPIO_MODE_PULLDOWN);
    csi_gpio_pin_config_direction(gpio_pin_handle, GPIO_DIRECTION_OUTPUT);
    csi_gpio_pin_write(gpio_pin_handle, true);

    //Config USI3_SCLK <=> GPIOB12  
    gpio_pin_handle = csi_gpio_pin_initialize(PB12, 0);
    csi_gpio_pin_config_mode(gpio_pin_handle, GPIO_MODE_PULLDOWN);
    csi_gpio_pin_config_direction(gpio_pin_handle, GPIO_DIRECTION_OUTPUT);
    csi_gpio_pin_write(gpio_pin_handle, true);
#if 1	
    //Config USI3_SD0 <=> GPIOB14  
    gpio_pin_handle = csi_gpio_pin_initialize(PB14, 0);
    csi_gpio_pin_config_mode(gpio_pin_handle, GPIO_MODE_PULLDOWN);
    csi_gpio_pin_config_direction(gpio_pin_handle, GPIO_DIRECTION_OUTPUT);
    csi_gpio_pin_write(gpio_pin_handle, true);

    //Config USI3_SD1 <=> GPIOB15  
    gpio_pin_usi3_sd1_handle = csi_gpio_pin_initialize(PB15, 0);
    csi_gpio_pin_config_mode(gpio_pin_usi3_sd1_handle, GPIO_MODE_PULLDOWN);
    csi_gpio_pin_config_direction(gpio_pin_usi3_sd1_handle, GPIO_DIRECTION_OUTPUT);
    csi_gpio_pin_write(gpio_pin_usi3_sd1_handle, false);  //低

    aos_msleep(5);
    csi_gpio_pin_write(gpio_pin_usi3_sd1_handle, true);  //高
#endif
}

//lfcui 
// #define  MM_IMG_RGB_BUF0   MM_IMG_ISP0_BUF1   /** 640 * 360 * 4 = 900K */ 
// #define  MM_IMG_IR_BUF0    MM_IMG_ISP1_BUF0   /** 640 * 360 = 225K */ 
// #define  MM_IMG_SPKR_BUF0  0x06380000         /** 640 * 360 = 225K */ 
// #define  MM_IMG_SPKL_BUF0  0x063c0000         /** 640 * 360 = 225K */ 
// #define  MM_IMG_RGB_BUF1   0x06400000         /** 640 * 360 * 4 = 900K */ 
// #define  MM_IMG_IR_BUF1    0x06500000         /** 640 * 360 = 225K */ 
// #define  MM_IMG_SPKR_BUF1  0x06540000         /** 640 * 360 = 225K */ 
// #define  MM_IMG_SPKL_BUF1  0x06580000         /** 640 * 360 = 225K */ 
#define  MM_DEPTH_BUF      0x07C00000  
#define  MM_IR_ISP_BUF0    0x07800000 
// #define  MM_MIPI2DMA_BUF1  0x06600000         /** 1280 * 720 * 2 = 1.8M (0x1c2000)*/ 
#define  MM_MIPI2DMA_BUF2  0x06800000         /** 1280 * 720 * 2 = 1.8M (0x1c2000)*/ 

#define  MM_IMG_AR0230_BUF0   0x07000000
#define  MM_IMG_AR0230_BUF1   0x07400000

void vi_camera_buf_init(void)
{
    // g_fb_rgb[ISP1_BUFFER_GROUP_0]       = (uint32_t *)MM_IMG_RGB_BUF0;
    // g_fb_ir[ISP1_BUFFER_GROUP_0]        = (uint32_t *)MM_IR_ISP_BUF0;       //MIPI2 IR
    // g_fb_spk_right[ISP1_BUFFER_GROUP_0] = (uint32_t *)MM_IMG_SPKR_BUF0;     //MIPI1 SPECKLE
    // g_fb_spk_left[ISP1_BUFFER_GROUP_0]  = (uint32_t *)MM_IMG_SPKL_BUF0;     //MIPI2 SPECKLE
    
    // g_fb_rgb[ISP1_BUFFER_GROUP_1]       = (uint32_t *)MM_IMG_RGB_BUF1;
    // g_fb_ir[ISP1_BUFFER_GROUP_1]        = (uint32_t *)MM_IR_ISP_BUF0;       //MIPI2 IR
    // g_fb_spk_right[ISP1_BUFFER_GROUP_1] = (uint32_t *)MM_IMG_SPKR_BUF1;     //MIPI1 SPECKLE
    // g_fb_spk_left[ISP1_BUFFER_GROUP_1]  = (uint32_t *)MM_IMG_SPKL_BUF1;     //MIPI2 SPECKLE
}


uint32_t get_ir_buf_addr(void)
{
    return (uint32_t)g_fb_ir[g_fb_buffer_group_id];
}

uint32_t vi_get_snap_3in1_rgb_adr(void)
{
    return MM_IMG_RGB_BUF0;
}

uint32_t vi_get_snap_3in1_ir_adr(void)
{
	return MM_USB_BUF_IR;
}

uint32_t vi_get_snap_3in1_spk_adr(void)
{	
	return MM_USB_BUF_SPK;
}

uint32_t vi_get_snap_3in1_depth_adr(void)
{
    return MM_DEPTH_BUF;
}

uint32_t vi_get_snap_ir_720P_raw10_adr(void)
{
	return (uint32_t)MM_MIPI2DMA_BUF1;
}

uint32_t vi_get_uvc_ir_720P_yuv_adr(void)
{
	return (uint32_t)MM_MIPI2DMA_BUF1;
}

uint32_t vi_get_uvc_rgb_720P_yuv_adr(void)
{
	return MM_IMG_AR0230_BUF0;
}

uint32_t vi_get_uvc_depth_640x360_raw16_adr(void)
{
	return MM_DEPTH_BUF;
}

void vi_isp_buffer_set(uint32_t idx, uint32_t reg, uint32_t address)
{
    volatile uint32_t *ptr;
    
    isp_reg_read(idx, MARVIN_CTRL_ID);
    isp_reg_read(idx, MI_INIT);
    isp_reg_read(idx, reg);
    isp_reg_read(idx, reg);

    // To make sure buffer MI_SP_Y_BASE_AD is not being written.
    isp_reg_write(idx, reg,  address); // mipi2 IR
    ptr = (volatile uint32_t *)(isp_reg_base(idx) + reg);
    if (*ptr != address)
    {
        printf("Failed to set MI_SP_Y_BASE_AD for 0x%x\r\n", address);
    }
}

// int image_type_is_change(USB_IMAGE_TYPE type)
// {
//     static uint32_t pre_type = USB_IMAGE_TYPE_IR;
//     int ret = 0;
//     if (pre_type != type) {
//         ret = 1;
//     }
//     pre_type = type;
//     return ret;
// }

void vi_isp1_IR_mode(void)
{
	ir_led_power_off();
    //start ir led   
    ir_led_power_on(); 
    aos_msleep(1);
}


// void vi_isp1_switch2ir_mode(void)
// {
// 	if(g_ir_mode == CFG_MODE_IR_640x360_RAW)
// 	{
// 		vi_isp_buffer_set(ISP_1, MI_SP_Y_BASE_AD, (uint32_t)MM_IMG_ISP1_BUF0);
// 	}
//     switch_exposure_para(g_cur_para);

//     //PRINTF_LOG("[ir] led power off\n");
//     ir_led_power_off();
//     //start ir led   
//     ir_led_power_on(); 
//     aos_msleep(5);
	
//     //mipi2isp_channel_stop(2,1);
// 	mipi2isp_channel_start(2,1);

// }

void vi_isp1_switch2spk2_mode(void)
{
    
    vi_isp_buffer_set(ISP_1, MI_SP_Y_BASE_AD, (uint32_t)g_fb_spk_left[g_fb_buffer_group_id]);
    //vi_isp_buffer_set(ISP_1, MI_SP_Y_BASE_AD, (uint32_t)g_fb_spk_left[g_fb_buffer_group_id]);
	//vi_isp_buffer_set(ISP_1, MI_SP_Y_BASE_AD, (uint32_t)g_fb_spk_left[g_fb_buffer_group_id]);
    // stop ir led
	
	//set_spk_exposure_para(EXPOSURE_PARA_INDOOR);
    ir_led_power_off();
    // start speckle
    speckle_power_on();
    
	mipi2isp_channel_start(2,1);
}

// void vi_phase1_status_reset(void)
// {
//     g_face_found = 0;
//     g_isp_mi1_event = 0;
//     g_ir_img_ready = 0;
//     g_spk1_img_ready = 0;
//     g_spk2_img_ready = 0;
//     g_rgb_img_ready = 0;
//     vi_isp1_switch2ir_mode();
//     phase1_allow_recv = 1;
// }

// void vi_phase2_status_reset(void)
// {
//     /* Set RGB image parameters */
//     g_isp_mi0_event = 0;
//     g_rgb_img_ready = 0;
    
//     /* Set SPARKLE images parameters */
//     g_isp_mi1_event = 0;
//     g_spk1_img_ready = 0;
//     g_spk2_img_ready = 0;
//     vi_isp1_switch2spk2_mode();
//     //mipi1_mipi2dma_start();
//     phase2_allow_recv = 1;
// }

void vi_set_snap_mode(uint32_t mode)
{
#if 0
	mode_change_en = true;
	mode_set = mode;
	printf("set_snap_mode=%d\n", mode);
#else 
	ifv_msg_pkg_t msg;
	if(SNAPSHOT_MODE_3IN1 == mode)
		msg.cmd = APP2IFV_SNAPSHOT_MODE_3IN1;
	else if(SNAPSHOT_MODE_DONE == mode)
		msg.cmd = APP2IFV_SNAPSHOT_MODE_DONE;
		
	ifv_post_msg(msg);

#endif 	
}

uint32_t vi_get_snap_status(void)
{
    return g_snapshot_done;
}


// int vi_change_snap_mode(uint32_t mode)
// {
// 	imageChannel_t vis, ir;
//     g_snapshot_mode = mode;
//     g_snapshot_done = 0;
//  	if(SNAPSHOT_MODE_3IN1 == mode)
//  	{
//  		//isp_set_mode(ISP_0,SNAP_MODE);
//  		isp_set_mode(ISP_1, SNAP_MODE);
//  		if(g_rgb_mode == CFG_MODE_RGB_720P_YUV)
//  		{
//  			printf("VIS_MODE_RGB_720P_YUV \n");
//  			mipi2isp_channel_stop(MIPI_0, ISP_0);
//  			rgb_720P_yuv_2_640x360_rgb(); //set isp resize true
//  			mipi2isp_channel_start(MIPI_0, ISP_0);
//  			g_rgb_mode = CFG_MODE_RGB_640X360_RGB;
//  		}
//  		else if(g_rgb_mode != CFG_MODE_RGB_640X360_RGB)
//  		{
//  			printf("VIS_SET_MODE_RGB_640X360 \n");
// 			vis.in.format = YUV422;
// 			vis.in.res = R_720P;
// 			vis.out.format = ARGB;
// 			vis.out.res = R_640x360;
// 			vis.mipi_ch = MIPI_0;
// 			vis.isp_ch = ISP_0;
// 			vis_image_channel_set(&vis, true,0,0);	
//  			g_rgb_mode = CFG_MODE_RGB_640X360_RGB;
//  		}
//  		else if(g_rgb_mode == CFG_MODE_RGB_640X360_RGB)
//  		{
//  			printf("VIS_MODE_RGB_640X360 \n");
// 			mipi2isp_channel_start(MIPI_0, ISP_0);
//  		}

// //		if(g_ir_mode == CFG_MODE_IR_720P_YUV)
// //		{
// //			mipi2isp_channel_stop(MIPI_2, ISP_1);
// // 			rgb_720P_yuv_2_640x360_rgb(); //set isp resize
// // 			mipi2isp_channel_start(MIPI_2, ISP_1);
// //		}
// // 		else 
// 		if(g_ir_mode != CFG_MODE_IR_640x360_RAW)
//  		{
//  			printf("IR_SET_MODE_RAW8_640X360 \n");
// 			ir.in.format = RAW8;
// 			ir.in.res = R_720P;
// 			ir.out.format = RAW8;
// 			ir.out.res = R_640x360;
// 			ir.mipi_ch = MIPI_2;
// 			ir.isp_ch = ISP_1;
// 			vi_isp1_IR_mode();
// 			ir_image_channel_set(&ir, true, 0, 0);
//  			g_ir_mode = CFG_MODE_IR_640x360_RAW;
//  		}	
// 		else
// 		{
// 			printf("IR_MODE_RAW8_640X360 \n");
// 			vi_isp1_switch2ir_mode();
// 		}
// 		g_snapshot_3in1_mode = SNAPSHOT_MODE_3IN1_VIS_IR;	
//  	}
//  	else if(SNAPSHOT_MODE_RGB_1080P_YUV == mode)
//  	{
//  		//isp_set_mode(ISP_0, SNAP_MODE);
//  		if(g_rgb_mode == CFG_MODE_RGB_1080P_YUV)
//  		{
//  			printf("VIS_MODE_1080P_YUV\n");
//  			mipi2isp_channel_start(0,0);
//  		}
//  		else
//  		{		
//  			printf("VIS_SET_MODE_1080P_YUV\n");
//  			vis.in.format = YUV422;
// 			vis.in.res = R_1080P;
// 			vis.out.format = YUV422;
// 			vis.out.res = R_1080P;
// 			vis.mipi_ch = MIPI_0;
// 			vis.isp_ch = ISP_0;
// 			vis_image_channel_set(&vis, false,0,0);
//  			g_rgb_mode = CFG_MODE_RGB_1080P_YUV;
//  		}
//  	}
//  	else if(SNAPSHOT_MODE_IR_720P_RAW10 == mode)
//  	{
//  		isp_set_mode(ISP_1, SNAP_MODE);
//  #if 0
//  		if(g_ir_mode == CFG_MODE_IR_720P_YUV)
//  		{
//  			printf("switch 720P yuv 2 720p raw10 \n");
//  		}
//  		else if(g_ir_mode != CFG_MODE_IR_720P_RAW10)
//  		{
//  			printf("config IR 720p raw10 \n");
//  			camera_config_9282(CONFIG_TYPE_9282_720P_RAW10);
//  		}
//  #endif
//  		if(g_ir_mode == CFG_MODE_IR_720P_RAW10)
//  		{
// 			printf("IR_MODE_720P_RAW10 \n");
// 			vi_isp1_switch2ir_mode();
//  		}
// 		else
// 		{
// 			printf("IR_SET_MODE_720P_RAW10 \n");
//  			ir.in.format = RAW10;
// 			ir.in.res = R_720P;
// 			ir.out.format = RAW10;
// 			ir.out.res = R_720P;
// 			ir.mipi_ch = MIPI_2;
// 			ir.isp_ch = ISP_1;
// 			vi_isp1_IR_mode();
// 			ir_image_channel_set(&ir, true, 0, 0);
//  			g_ir_mode = CFG_MODE_IR_720P_RAW10;
// 		}
//  	}
//  	else if(SNAPSHOT_MODE_DEPTH_640x360 == mode)
//  	{
//  		//g_usb_image_type = USB_IMAGE_TYPE_DEPTH;
//  		//vi_isp1_switch2spk2_mode();
//  	}
// 	else if(SNAPSHOT_MODE_UVC_RGB_720P_YUV == mode)
// 	{
// 		if(g_rgb_mode == CFG_MODE_RGB_720P_YUV)
// 		{
// 			printf("RGB_MODE_720P_YUV \n");
// 		}
// 		else if(g_rgb_mode == CFG_MODE_RGB_1080P_YUV)
// 		{
// 			printf("RGB_SET_MODE_720P_YUV \n");

// 			vis.in.format = YUV422;
// 			vis.in.res = R_720P;
// 			vis.out.format = YUV422;
// 			vis.out.res = R_720P;
// 			vis.mipi_ch = MIPI_0;
// 			vis.isp_ch = ISP_0;
// 			vis_image_channel_set(&vis, false,0,0);

// 			g_rgb_mode = CFG_MODE_RGB_720P_YUV;
// 		}
// 		else if(g_rgb_mode == CFG_MODE_RGB_640X360_RGB)
// 		{
// 			printf("RGB_SWITCH_MODE_720P_YUV \n");
// 			rgb_640x360_rgb_2_720P_yuv();
// 			g_rgb_mode = CFG_MODE_RGB_720P_YUV;
// 		}
// 	}
// 	else if(SNAPSHOT_MODE_UVC_IR_720P_YUV == mode)
// 	{
// 		//isp_set_mode(ISP_1, STREAM_MODE);
// 		isp_set_mode(ISP_1, SNAP_MODE);
// 		if(g_ir_mode != CFG_MODE_IR_720P_YUV)
// 		{
// 			printf("IR_SET_MODE_720P_YUV \n");

// 			ir.in.format = RAW10;
// 			ir.in.res = R_720P;
// 			ir.out.format = RAW10;
// 			ir.out.res = R_720P;
// 			ir.mipi_ch = MIPI_2;
// 			ir.isp_ch = ISP_1;
// 			vi_isp1_IR_mode();
// 			ir_image_channel_set(&ir, false, 0, 0);
// 			g_ir_mode = CFG_MODE_IR_720P_YUV; 
// 		}
// 		else
// 		{
// 			printf("IR_MODE_720P_YUV \n");
// 			g_ir_mode = CFG_MODE_IR_720P_YUV;
// 		}
		
// 	}
// 	else if(SNAPSHOT_MODE_UVC_DEPTH_640X360_RAW16 == mode)
// 	{
// 		isp_set_mode(ISP_1, SNAP_MODE);
// 		if(g_ir_mode != CFG_MODE_IR_640x360_RAW)
// 		{
// 			printf("IR_SET_MODE_640x360_RAW \n");
// 			ir.in.format = RAW8;
// 			ir.in.res = R_720P;
// 			ir.out.format = RAW8;
// 			ir.out.res = R_640x360;
// 			ir.mipi_ch = MIPI_2;
// 			ir.isp_ch = ISP_1;
// 			ir_image_channel_set(&ir, true, 0, 0);
// 			vi_isp1_switch2spk2_mode();
// 			g_ir_mode = CFG_MODE_IR_640x360_RAW;
// 		}
// 		else
// 		{
// 			printf("IR_MODE_640x360_RAW \n");
// 		}
// 	}	

// 	return 0;
// }

/* isp process */
void algo_isp_handle(void)
{
    static int saveVal[ISP_NUM];
    if(ispProcFlag[ISP_1] == 1){   
        //printf("[isp] ir hanlde  is runing\n");
        ispProcFlag[ISP_1] = 0;
        saveVal[ISP_1] = curVal[ISP_1];
        curVal[ISP_1] = 0;
        HalIsrHandlerSrc(1, saveVal[ISP_1]);         
    }  
	else if(ispProcFlag[ISP_0] == 1){   
        //printf("[isp] rgb hanlde  is runing\n");
        ispProcFlag[ISP_0] = 0;
        saveVal[ISP_0] = curVal[ISP_0];
        curVal[ISP_0] = 0;
        HalIsrHandlerSrc(0, saveVal[ISP_0]);         
    }
}

// static int image_task_init(void)
// {
// 	gpio_init();
// 	vi_camera_buf_init();
	
// 	example_pin_camera_iic0_init();
// 	example_pin_camera_iic1_init();
// 	i2c_usi_10.port = IIC_PORT_USI_10;
// 	i2c_usi_10.config.dev_addr = CK_IIC_SLAVE_ADDR_9732;
// 	int ret = hal_i2c_init(&i2c_usi_10);
// 	if (ret < 0) {
// 		printf("i2c_usi_10 init failed \n");
// 		return ret;
// 	}
// 	i2c_usi_11.port = IIC_PORT_USI_11;
// 	i2c_usi_11.config.dev_addr = CK_IIC_SLAVE_ADDR_9282;
// 	ret = hal_i2c_init(&i2c_usi_11);
// 	if (ret < 0) {
// 		printf("i2c_usi_10 init failed \n");
// 		return ret;
// 	}

// 	isp_i2c_init(0, &i2c_usi_10,CK_IIC_SLAVE_ADDR_9732);

// 	ov9732_init(&i2c_usi_10, CK_IIC_SLAVE_ADDR_9732);
// 	ov9282_init(&i2c_usi_11, CK_IIC_SLAVE_ADDR_9282);
// 	ir_led_init(&i2c_usi_11, CK_IIC_SLAVE_ADDR_IRLED);

// 	mipi_init(MIPI_0);
// 	mipi_init(MIPI_2);
// 	isp_init(ISP_0,1);
// 	isp_init(ISP_1,2);

// 	imageChannel_t  vis;
// 	vis.in.format = RAW10;
// 	vis.in.res = R_720P;
// 	vis.out.format = ARGB;
// 	vis.out.res = R_640x360;
// 	vis.mipi_ch = MIPI_0;
// 	vis.isp_ch = ISP_0;
// 	vis_image_channel_set(&vis, false, 0, 0);
	
// 	imageChannel_t  ir;
// 	ir.in.format = RAW8;
// 	ir.in.res = R_720P;
// 	ir.out.format = RAW8;
// 	ir.out.res = R_640x360;
// 	ir.mipi_ch = MIPI_2;
// 	ir.isp_ch = ISP_1;
// 	//ir_image_channel_set(&ir, true);

// 	printf("ov9282 init done \n");
// 	aos_msleep(3000);

// //	//RGB 720P YUV 
// //	vis.in.format = YUV422;
// //	vis.in.res = R_720P;
// //	vis.out.format = YUV422;
// //	vis.out.res = R_720P;
// //	vis.mipi_ch = MIPI_0;
// //	vis.isp_ch = ISP_0;
// //	vis_image_channel_set(&vis, false);
// //
// //	//IR 720P RAW10
// //	ir.in.format = RAW10;
// //	ir.in.res = R_720P;
// //	ir.out.format = RAW10;
// //	ir.out.res = R_720P;
// //	ir.mipi_ch = MIPI_2;
// //	ir.isp_ch = ISP_1;
// //	printf("config 9282 \n");
// //	ir_image_channel_set(&ir, true);
// //
// //	aos_msleep(10000);
// //
// //	//RGB 1080P YUV 
// //	vis.in.format = YUV422;
// //	vis.in.res = R_1080P;
// //	vis.out.format = YUV422;
// //	vis.out.res = R_1080P;
// //	vis.mipi_ch = MIPI_0;
// //	vis.isp_ch = ISP_0;
// //	vis_image_channel_set(&vis, false);
// //
// //	//IR 720P ISP
// //	ir.in.format = RAW10;
// //	ir.in.res = R_720P;
// //	ir.out.format = RAW10;
// //	ir.out.res = R_720P;
// //	ir.mipi_ch = MIPI_2;
// //	ir.isp_ch = ISP_1;
// //	ir_image_channel_set(&ir, false);

// 	return 0;
// }
uint32_t g_isp_buffer_num = 3;
// void image_test_task(void *arg)
// {
// 	int ret = 0;
//     TSM_VI_INFO_S stViInfo = {0};
//     static uint32_t g_snapshot_3in1_m = SNAPSHOT_MODE_3IN1_VIS_IR;
//     // uint32_t buffer_list[3] = {MM_IMG_ISP1_BUF0, MM_IMG_ISP1_BUF1, MM_IMG_IR_BUF1};
//     char *dpu_disp_addr = NULL;

// 	gpio_init();
// 	vi_camera_buf_init();
	
// 	example_pin_camera_iic0_init();
// 	example_pin_camera_iic1_init();
    
//     camera9732_init();
//     camera9282_init();
//     lcd_init();

//     dpu_disp_addr = (char *)tsm_vo_get_disp_memory();
    
//     aos_msleep(5);
    
// 	if(ret != 0)
// 	{
// 		printf("image task init failed\n");
// 		return;
// 	}
	
// 	while(1)
// 	{
// 		algo_isp_handle();
// 		aos_msleep(10);
        
// 		if (!isp_mi_event[ISP_0] || !isp_mi_event[ISP_1]) 
// 		{
// 			continue;
// 		}	

// 		isp_mi_event[ISP_0]  = 0;
// 		isp_mi_event[ISP_1]  = 0;
        
//         stViInfo.stSnsInfo.u32MipiId    = MIPI_1;
//         stViInfo.stPipeInfo.u32PipeId   = ISP_1;
//         stViInfo.stLedInfo.enLightType  = LED_LIGHT_TYPE_IR;
// 		tsm_vi_pause(&stViInfo);

// #if 1
//         if (dpu_disp_addr)
//         {
//             image_convert_size((uint32_t*)SRC_ADDR,
//                         SRC_WIDTH,SRC_HEIGHT,
//                         (char*)dpu_disp_addr,
//                         DST_WIDTH,DST_HEIGHT);
//         }
// #else
//         cfg_isp_base_buffer(stViInfo.stPipeInfo.u32PipeId, 
//                             buffer_list, 
//                             g_isp_buffer_num, 
//                             0x40000);

//         aos_msleep(1000);
// #endif

//         stViInfo.stSnsInfo.u32MipiId    = MIPI_1;
//         stViInfo.stPipeInfo.u32PipeId   = ISP_1;
//         if (SNAPSHOT_MODE_3IN1_VIS_IR == g_snapshot_3in1_m)
//         {
//             stViInfo.stLedInfo.enLightType  = LED_LIGHT_TYPE_SPK;
//             g_snapshot_3in1_m = SNAPSHOT_MODE_3IN1_SPK;
//         }
//         else if (SNAPSHOT_MODE_3IN1_SPK == g_snapshot_3in1_m)
//         {
//             stViInfo.stLedInfo.enLightType  = LED_LIGHT_TYPE_IR;
//             g_snapshot_3in1_m = SNAPSHOT_MODE_3IN1_VIS_IR;
//         }
//         stViInfo.stSnsInfo.u32Expo      = SENSOR_EXPO_INDOOR;
// 		tsm_vi_resume(&stViInfo);
// 		printf("ir & rgb ready\n");
//     }
// }


// void image_task(void *arg)
// {
// 	int ret = 0;

// 	ret = image_task_init();
// 	if(ret != 0)
// 	{
// 		printf("image task init failed\n");
// 		return;
// 	}
	
// 	while(1)
// 	{
// 		algo_isp_handle();
// 		aos_msleep(10);

// 		if(SNAPSHOT_MODE_3IN1 == g_snapshot_mode)
// 		{
// 			if (SNAPSHOT_MODE_3IN1_VIS_IR == g_snapshot_3in1_mode)
// 			{
// 				if (!isp_mi_event[ISP_0] || !isp_mi_event[ISP_1]) 
// 				{
// 					continue;
// 				}	
// 				mipi2isp_channel_stop(MIPI_0, ISP_0);
// 				isp_mi_event[ISP_0]  = 0;
// 				isp_mi_event[ISP_1]  = 0;

// 				//time[0] =  aos_now_ms();
// 				if (!led_check_enable())
// 				{
// 					//PRINTF_LOG("isp_run ir: led: %02x \r\n", led_read_flag());
// 					printf("isp_run ir: led\r\n");
// 					vi_isp1_switch2ir_mode();
// 					continue;
// 				}
// 				vi_isp1_switch2spk2_mode();
// 				g_snapshot_3in1_mode = SNAPSHOT_MODE_3IN1_SPK;
// 				printf("ir & rgb ready\n");
// 			}
// 			else if (SNAPSHOT_MODE_3IN1_SPK == g_snapshot_3in1_mode)
// 			{
// 				if (!isp_mi_event[ISP_1]) {
// 					continue;
// 				}	
// 				isp_mi_event[ISP_1] = 0;   
// 				if (!led_check_enable()){
// 					//PRINTF_LOG("isp_run spk: led: %02x \r\n",led_read_flag());
// 					printf("isp_run spk: led \r\n");
// 					vi_isp1_switch2spk2_mode();
// 					continue;
// 				}
// 				//time[1] =  aos_now_ms();
// 				//printf("time[0], time[1]= (%d, %d)\n", time[0], time[1]);
// 				ir_led_power_off();
// 				printf("speckle ready\n");
// 				g_spk1_img_ready = 1;
// 				g_snapshot_3in1_mode = SNAPSHOT_MODE_3IN1_NONE;  
// 				g_snapshot_mode = SNAPSHOT_MODE_NONE;
// 			}
// 		}
// 		else if(SNAPSHOT_MODE_IR_720P_RAW10 == g_snapshot_mode)
// 		{
// 			if (!isp_mi_event[ISP_1]) {
// 				continue;
// 			}
// 			//time[0] =  aos_now_ms();
// 			isp_mi_event[ISP_1] = 0;
// 			if (!led_check_enable())
// 			{
// 				//PRINTF_LOG("isp_run ir: led: %02x \r\n",led_read_flag());
// 				printf("isp_run ir: led \r\n");
// 				vi_isp1_switch2ir_mode();
// 				continue;
// 			}
// 			printf("SNAPSHOT_MODE_IR_720P_RAW10 done \n");
// 			//aos_msleep(30);
// 			g_snapshot_done = 1;
// 			g_snapshot_mode = SNAPSHOT_MODE_NONE;
// 		}
// 	 	else if(SNAPSHOT_MODE_RGB_1080P_YUV == g_snapshot_mode)
// 	 	{
// 	 		if (!isp_mi_event[ISP_0]) 
// 			{
// 				continue;
// 			}
// 	 		isp_mi_event[ISP_0] = 0;
// 	 		mipi2isp_channel_stop(MIPI_0,ISP_0);
			
// 	 		printf("SNAPSHOT_MODE_RGB_1080P_YUV done \n");
// 	 		g_snapshot_mode = SNAPSHOT_MODE_NONE;
// 	 		g_snapshot_done = 1;
// 	 	}
// 		else if(SNAPSHOT_MODE_DONE == g_snapshot_mode)
// 		{
// 			//mipi2isp_channel_start(0,0);
// 			ir_led_power_off();
// 			printf("snap clear status \n");
// 			g_snapshot_done = 0; 
// 			g_snapshot_mode = SNAPSHOT_MODE_NONE;
// 		}
// 		else if(SNAPSHOT_MODE_NONE == g_snapshot_mode || g_snapshot_3in1_mode == SNAPSHOT_MODE_3IN1_NONE)
// 		{
// 			if(mode_change_en)
// 			{
// 				printf("snap change mode \n");
// 				vi_change_snap_mode(mode_set);
// 				mode_change_en = false;
// 			}
// 		}

// 		if(SNAPSHOT_MODE_UVC_IR_720P_YUV == g_snapshot_mode)
// 		{
// 			if (!isp_mi_event[ISP_1])
// 			{
// 				continue;
// 			}
// 			isp_mi_event[ISP_1] = 0;
// 			test_hist_get_img();
// 			ir_led_power_off();
// 			vi_isp1_switch2ir_mode();
// 		}
// 		else if(SNAPSHOT_MODE_UVC_DEPTH_640X360_RAW16 == g_snapshot_mode)
// 		{
// 			if (!isp_mi_event[ISP_1])
// 			{
// 				continue;
// 			}
// 			isp_mi_event[ISP_1] = 0;		

// 			//g_depth_image_status = 0;

// 			if (led_check_enable())
// 			{
// 				g_spk1_img_ready = 1;
// 				ir_led_power_off();
// 				printf("uvc depth : speckle ready \n");
// 			}
// 			else
// 			{
// 				printf("uvc depth : speckle	again ! \n");
// 				vi_isp1_switch2spk2_mode();
// 			}			
// 		}
//      }
// }

