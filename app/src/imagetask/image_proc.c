#include <string.h>
#include <stdio.h>
#include "image_proc.h"

#define COLOR_TO_MTK_COLOR_SIMUL(color) ((((color) >> 19) & 0x1f) << 11) |((((color) >> 10) & 0x3f) << 5) |(((color) >> 3) & 0x1f)

void image_convert_size(uint32_t *p_src, uint32_t src_width, uint32_t src_height,
                        char *p_dst, uint32_t dst_width, uint32_t dst_height)
{
	if((NULL == p_src) || (NULL == p_dst))
	{
		return;
	}
	uint32_t i = 0, j = 0;
	uint32_t width_offset, height_offset = 0;
	uint32_t *p_start_adr = NULL;

	int station = 0;
	int picture_data;
	unsigned char input[4];
	int rgb565 = 0;
	int isrc_width = 0;

	width_offset = (src_width - dst_width) / 2 ;
	height_offset = (src_height - dst_height) / 2 ;

	p_start_adr = p_src + src_width * height_offset + width_offset;
	for (i = 0; i < dst_height; i++) //320 row
	{
	    station = 480 * (i + 1);
		isrc_width = i * src_width;
		for(j = 0; j < dst_width; j++) //240 column
		{
			picture_data = *(p_start_adr + isrc_width + j);
            input[0] = (unsigned char)(picture_data >> 16);
			input[1] = (unsigned char)(picture_data >> 8);
			input[2] = (unsigned char)picture_data; 
			rgb565 = COLOR_TO_MTK_COLOR_SIMUL((input[0] << 16) | (input[1] << 8) | input[2]);
				
			*(short int*)(p_dst + station -2 * (j + 1)) = (unsigned char)(rgb565) | ((unsigned char)(rgb565 >> 8) << 8);
	        //*(short int*)(p_dst + station -2*(j+1)) =0xff;
		}
	}
}

void image_shot_run(void *arg)
{
	printf("image convert size \n");
	while (1) 
	{
		image_convert_size((uint32_t*)SRC_ADDR,SRC_WIDTH,SRC_HEIGHT,(char*)DST_ADDR,DST_WIDTH,DST_HEIGHT);
	}
}
