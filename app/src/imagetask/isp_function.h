#ifndef _ISP_FUNCTION_H_
#define _ISP_FUNCTION_H_

#include "drv_isp.h"

int isp_sw_reset(uint8_t isp_id);

int isp_init(int isp_id, isp_work_mode_e mode);
int isp_config(int isp_id, imageFormat_t *in, imageFormat_t *out);
void isp_set_mode(int isp_id, int mode);
void isp_res_crop_map(image_res_e in_res, image_res_e out_res, uint32_t *width, uint32_t *height);


#endif

