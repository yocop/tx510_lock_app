#ifndef IMAGE_PROC_H_
#define IMAGE_PROC_H_
#include <stdint.h>
#include "mm_config.h"

#define SRC_ADDR   MM_IMG_RGB_BUF0 
#define DST_ADDR   MM_IMG_FOR_DPU
#define SRC_WIDTH  640
#define SRC_HEIGHT 360
#define DST_WIDTH  240
#define DST_HEIGHT 320

typedef enum 
{
	SNAPSHOT_MODE_3IN1_NONE = 0,
    SNAPSHOT_MODE_3IN1_VIS_IR,
    SNAPSHOT_MODE_3IN1_SPK,
}SNAPSHOT_MODE_3IN1_E;

typedef enum 
{
    SNAPSHOT_MODE_NONE = 0,
    SNAPSHOT_MODE_3IN1,			
    SNAPSHOT_MODE_RGB_1080P_YUV,
    SNAPSHOT_MODE_RGB_720P_YUV,
    SNAPSHOT_MODE_RGB_640X360_RGB,
    SNAPSHOT_MODE_IR_720P_RAW10,
    SNAPSHOT_MODE_IR_640x360_RAW,
    SNAPSHOT_MODE_DEPTH_640x360,
    SNAPSHOT_MODE_UVC_RGB_720P_YUV,
    SNAPSHOT_MODE_UVC_IR_720P_YUV,
    SNAPSHOT_MODE_UVC_DEPTH_640X360_RAW16,
    SNAPSHOT_MODE_DONE,
    SNAPSHOT_MODE_SET,
}SNAPSHOT_MODE_E;

typedef enum 
{
    CFG_MODE_NONE = 0,		
    CFG_MODE_RGB_1080P_YUV,
    CFG_MODE_RGB_720P_YUV,
    CFG_MODE_RGB_640X360_RGB,
    CFG_MODE_IR_720P_YUV,
    CFG_MODE_IR_720P_RAW10,
    CFG_MODE_IR_640x360_RAW,
}SENSOR_CFG_MODE_E;

/** 
 * @brief  convert image size
 * @param p_src      : Source address 
 * @param src_width  : The width of the source image
 * @param src_height : The heigth of the source image
 * @param p_dst      : Destination address
 * @param dst_width  : The width of the destination image
 * @param dst_height : The heigth of the destination image
 */
//void image_convert_size(uint32_t *p_src, uint32_t src_width, uint32_t src_height,
//                        char *p_dst, uint32_t dst_width, uint32_t dst_height);

void image_shot_run(void *arg);                        
  
#endif