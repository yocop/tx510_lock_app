#ifndef __IMAGE_TASK_H_
#define __IMAGE_TASK_H_

#define  CARMERA_DEBUG  1
#if  CARMERA_DEBUG
#define PRINTF_LOG(format,...) 	printf(format "\n", ##__VA_ARGS__)
#else
#define PRINTF_LOG(format,...)
#endif

#define CONFIG_TYPE_9282_720P_ISP       0	//RAW10
#define CONFIG_TYPE_9282_720P_NO_ISP  	1	//RAW8
#define CONFIG_TYPE_9282_SPK360P   		2	//RAW8
#define CONFIG_TYPE_9282_720P_RAW10   	3   //

void example_pin_camera_iic0_init(void);
void example_pin_camera_iic1_init(void);

void vi_isp1_switch2spk2_mode(void);
void vi_phase1_status_reset(void);
void vi_phase2_status_reset(void);
int vi_camera_monocular_init(void);
void vi_set_snap_mode(uint32_t mode);
uint32_t vi_get_snap_status(void);
uint32_t vi_get_snap_3in1_rgb_adr(void);
uint32_t vi_get_snap_3in1_ir_adr(void);
uint32_t vi_get_snap_3in1_spk_adr(void);
uint32_t vi_get_snap_3in1_depth_adr(void);
uint32_t vi_get_snap_ir_720P_raw10_adr(void);

void image_task(void *arg);

#endif


