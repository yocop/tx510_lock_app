#include <stdint.h>
#include "pic_crop.h"

#define ADAPT_PIC_2_640x360_TEST_ENABLE 0

/*
return : 1, zoom in or out by width *w     
         2, zoom in or out by width *h   
*/

int checkPicSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight, float *w, float *h)
{
    int ret = 0;

    float kw = (float)srcWidth/dstWidth;
    float kh = (float)srcHeight/dstHeight;

    if(kw >= kh){       
        *w = 1 / kw;
        ret = 1;
    }
    else{
        *h = 1 / kh;
        ret = 2;
    }
    
    return ret;
}


int fillPicEdge(uint32_t *src, uint32_t *dst, int srcWdith, int srcHeight, int dstWidth, int dstHeight, int flag)
{
    int i,j = 0;
    if((NULL == src) || (NULL == dst) || (srcWdith > dstWidth) || (srcHeight > dstHeight))
    {
        return -1;
    }

    for(i = 0; i < dstHeight; i++)
    {
        for(j = 0; j < dstWidth * 4; j+=4)
        {
            if((i < srcHeight) && (j < srcWdith * 4))
            {
                dst[i*dstWidth+j] = src[i*srcWdith+j];
                dst[i*dstWidth+j + 1] = src[i*srcWdith+j + 1];
                dst[i*dstWidth+j + 2] = src[i*srcWdith+j + 2];
                dst[i*dstWidth+j + 3] = src[i*srcWdith+j + 3];
            }
            else //fill pic
            {
                dst[i*dstWidth+j] = 0;
                dst[i*dstWidth+j + 1] = 0;
                dst[i*dstWidth+j + 2] = 0;
                dst[i*dstWidth+j + 3] = 0;
            }
        }
    }

    return 0;
}

void image_8bits_rotate_right_90(char *src_image, char *dst_image, uint32_t width, uint32_t hight)
{
    for (int j = 0; j < width; j++)
    {
        for (int i = 0; i < hight; i++)
        {
            *(dst_image + j * hight + i) = *(src_image + (hight - i) * width + j);
        }
    }
}

void image_8bits_rotate_left_90(char *src_image, char *dst_image, uint32_t width, uint32_t hight)
{
    for (int i = 0; i < hight; i++)
    {
        for (int j = 0; j < width; j++)
        {
            *(dst_image + (width - j) * hight + i) = *(src_image + i * width + j);
        }
    }
}



#if ADAPT_PIC_2_640x360_TEST_ENABLE
#include "jpegConvert.h"
#include "net_param.h"

#define SRC_PIC_SIZE 99866
#define SRC_PIC_ADR 0x6b00000  //jpg
#define DST_PIC_ADR 0x6d00000  //raw(jpg 2 raw)
#define RESIZE_PIC_ADR 0x6F00000  //resize raw(zoom in or out)
#define DST_PIC_360x640_ADR 0x7100000 //raw (fill in to 640x360) 

void test_pic_crop(void)
{
    int result = 0;
	int srcW = 439;
	int srcH = 721;
	int dstW = 360;
	int dstH = 640;
	float w,h = 0;

	tsmImg_t srcImg;
	tsmImg_t dstImg;

#if 1
    //jpg 2 raw rgb
    jpeg_to_raw(SRC_PIC_ADR, SRC_PIC_SIZE, DST_PIC_ADR, &srcW, &srcH, EXT_RGBA);
	printf("convert done,width %d ,height %d \n", srcW, srcH);
	
    //check pic size
	result = checkPicSize(srcW, srcH, dstW, dstH, &w, &h);
	printf("ret %d, w %f, h %f \n", result, w, h);
#endif

#if 1
	srcImg.w = srcW;
	srcImg.h = srcH;
	srcImg.bits = 32;
	srcImg.data = DST_PIC_ADR;

	dstImg.w = dstW;
	dstImg.h = dstH;
	dstImg.bits = 32;
	dstImg.data = RESIZE_PIC_ADR;

	printf("resize begin:result %d \n", result);
	if(1 == result)
	{
		dstImg.h = srcH * w;
		printf("resizeDstH %d \n", srcH * w);
		TSM_ALGO_RunAmrResize(&srcImg, &dstImg);

		srcW = 360;
		srcH = dstImg.h;
	}
	else if(2 == result)
	{
		dstImg.w = srcW * h;
		printf("resizeDstW %d \n", srcW * h);
		TSM_ALGO_RunAmrResize(&srcImg, &dstImg);

		srcW = dstImg.w;
		srcH = 640;
	}
	printf("resize done \n");
#endif

#if 1
	printf("fillPicEdge begin \n");
	fillPicEdge(RESIZE_PIC_ADR, DST_PIC_360x640_ADR, srcW, srcH, dstW, dstH, 0);
	printf("fillPicEdge done \n");
#endif

}

#endif