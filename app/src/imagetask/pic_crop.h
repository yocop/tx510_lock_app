#ifndef _PIC_CROP_H_
#define _PIC_CROP_H_

#include "stddef.h"

int checkPicSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight, float *w, float *h);
int fillPicEdge(uint32_t *src, uint32_t *dst, int srcWdith, int srcHeight, int dstWidth, int dstHeight, int flag);
void image_8bits_rotate_right_90(char *src_image, char *dst_image, uint32_t width, uint32_t hight);
void image_8bits_rotate_left_90(char *src_image, char *dst_image, uint32_t width, uint32_t hight);

void test_pic_crop(void);
#endif

