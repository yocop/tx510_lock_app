/*
 * Copyright (C) 2019-2020 Alibaba Group Holding Limited
 */

#include <aos/aos.h>
#include <aos/kv.h>

#include <yoc/fota.h>
#include <uservice/eventid.h>
#include <uservice/uservice.h>

#define KV_FOTA_CYCLE_MS        "fota_cycle"
#define KV_FOTA_ENABLED         "fota_en"

#define TAG "app_fota"

static fota_t *g_fota_handle = NULL;


/**
 *  return 0: still do the default handle
 *  not zero: only do the user handle
 */
int fota_reboot_flag=1;
int fota_finish_flag=0;
static int fota_event_cb(void *arg, fota_event_e event)
{
    fota_t *fota = (fota_t *)arg;
    switch (event) {
        case FOTA_EVENT_VERSION:
            fota_finish_flag=1;
            /* get new version, return -1 if we do not want upgrade */
            LOGD(TAG, "FOTA VERSION :%x", fota->status);
            break;

        case FOTA_EVENT_FAIL:
            /* fota download or flash error
             * it will try next version check
             */
            LOGD(TAG, "FOTA FAIL :%x", fota->status);
            break;

        case FOTA_EVENT_FINISH:
            LOGD(TAG, "FOTA FINISH :%x", fota->status);
            fota_finish_flag=0;
            /*
             *如果重启，不进行升级，调用fota_set_not_ota_flag()函数
             *普通升级调用 fota_clear_not_ota_flag()重启生效
             *靜默升级调用 fota_set_silent_ota_flag()重启生效
            */
            //fota_set_not_ota_flag();

            /* 立即重启升级 */
            // app_speaker_mute(1);
            if(fota_reboot_flag){  
                // app_sys_reboot(BOOT_REASON_SOFT_RESET);
                aos_reboot();
            }
            break;

        default:
            break;
    }
    return 0;
}

// occ
static char ca_crt_rsa[] = {
"-----BEGIN CERTIFICATE-----\r\n"
"MIIEaTCCA1GgAwIBAgILBAAAAAABRE7wQkcwDQYJKoZIhvcNAQELBQAwVzELMAkG\r\n"
"A1UEBhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExEDAOBgNVBAsTB1Jv\r\n"
"b3QgQ0ExGzAZBgNVBAMTEkdsb2JhbFNpZ24gUm9vdCBDQTAeFw0xNDAyMjAxMDAw\r\n"
"MDBaFw0yNDAyMjAxMDAwMDBaMGYxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9i\r\n"
"YWxTaWduIG52LXNhMTwwOgYDVQQDEzNHbG9iYWxTaWduIE9yZ2FuaXphdGlvbiBW\r\n"
"YWxpZGF0aW9uIENBIC0gU0hBMjU2IC0gRzIwggEiMA0GCSqGSIb3DQEBAQUAA4IB\r\n"
"DwAwggEKAoIBAQDHDmw/I5N/zHClnSDDDlM/fsBOwphJykfVI+8DNIV0yKMCLkZc\r\n"
"C33JiJ1Pi/D4nGyMVTXbv/Kz6vvjVudKRtkTIso21ZvBqOOWQ5PyDLzm+ebomchj\r\n"
"SHh/VzZpGhkdWtHUfcKc1H/hgBKueuqI6lfYygoKOhJJomIZeg0k9zfrtHOSewUj\r\n"
"mxK1zusp36QUArkBpdSmnENkiN74fv7j9R7l/tyjqORmMdlMJekYuYlZCa7pnRxt\r\n"
"Nw9KHjUgKOKv1CGLAcRFrW4rY6uSa2EKTSDtc7p8zv4WtdufgPDWi2zZCHlKT3hl\r\n"
"2pK8vjX5s8T5J4BO/5ZS5gIg4Qdz6V0rvbLxAgMBAAGjggElMIIBITAOBgNVHQ8B\r\n"
"Af8EBAMCAQYwEgYDVR0TAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQUlt5h8b0cFilT\r\n"
"HMDMfTuDAEDmGnwwRwYDVR0gBEAwPjA8BgRVHSAAMDQwMgYIKwYBBQUHAgEWJmh0\r\n"
"dHBzOi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMDMGA1UdHwQsMCow\r\n"
"KKAmoCSGImh0dHA6Ly9jcmwuZ2xvYmFsc2lnbi5uZXQvcm9vdC5jcmwwPQYIKwYB\r\n"
"BQUHAQEEMTAvMC0GCCsGAQUFBzABhiFodHRwOi8vb2NzcC5nbG9iYWxzaWduLmNv\r\n"
"bS9yb290cjEwHwYDVR0jBBgwFoAUYHtmGkUNl8qJUC99BM00qP/8/UswDQYJKoZI\r\n"
"hvcNAQELBQADggEBAEYq7l69rgFgNzERhnF0tkZJyBAW/i9iIxerH4f4gu3K3w4s\r\n"
"32R1juUYcqeMOovJrKV3UPfvnqTgoI8UV6MqX+x+bRDmuo2wCId2Dkyy2VG7EQLy\r\n"
"XN0cvfNVlg/UBsD84iOKJHDTu/B5GqdhcIOKrwbFINihY9Bsrk8y1658GEV1BSl3\r\n"
"30JAZGSGvip2CTFvHST0mdCF/vIhCPnG9vHQWe3WVjwIKANnuvD58ZAWR65n5ryA\r\n"
"SOlCdjSXVWkkDoPWoC209fN5ikkodBpBocLTJIg1MGCUF7ThBCIxPTsvFwayuJ2G\r\n"
"K1pp74P1S8SqtCr4fKGxhZSM9AyHDPSsQPhZSZg=\r\n"
"-----END CERTIFICATE-----\r\n"
" "
};

void app_fota_init(void)
{
    int ret;
    LOGD(TAG, "%s", aos_get_app_version());

    int fota_en = 1;
    ret = aos_kv_getint(KV_FOTA_ENABLED, &fota_en);

    if (ret == 0 && fota_en == 0) {
        LOGI(TAG, "fota disabled, use \"kv setint fota_en 1\" to enable");
        return;
    }

    fota_register_copc();
    netio_register_httpc(ca_crt_rsa);
    netio_register_flash();
    g_fota_handle = fota_open("copc", "flash://misc", fota_event_cb);

    fota_config_t config;

    config.sleep_time = 60000;
    config.timeoutms = 10000;
    config.retry_count = 0;

    config.auto_check_en = 1;

    aos_kv_getint(KV_FOTA_CYCLE_MS, &(config.sleep_time));

    fota_config(g_fota_handle, &config);
}

void app_fota_start(void)
{
    if (g_fota_handle != NULL) {
        fota_start(g_fota_handle);
    }
}

void app_fota_stop(void)
{
    if (g_fota_handle != NULL) {
        fota_stop(g_fota_handle);
    }
}

int app_fota_is_downloading(void)
{
    if (g_fota_handle == NULL) {
        return 0;
    }

    if (fota_get_status(g_fota_handle) == FOTA_DOWNLOAD) {
        return 1;
    }
    return 0;
}

void app_fota_do_check(void)
{
    if (g_fota_handle == NULL) {
        return;
    }

    LOGD(TAG, "app_fota_do_check");

    /* do sleep here, for avoid lpm conflict */
    aos_msleep(200);
    fota_do_check(g_fota_handle);
}

void app_fota_set_auto_check(int enable)
{
    if (g_fota_handle == NULL) {
        return;
    }

    fota_set_auto_check(g_fota_handle, enable);
}
