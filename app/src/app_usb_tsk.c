	
#include <stdio.h>
#include <string.h>
#include <pin.h>
#include <aos/kernel.h>
#include <aos/debug.h>
#include <aos/kv.h>
#include <ulog/ulog.h>
#include <smxfs.h>
#include <if_v.h>
#include "uinit.h"
#include "lv_user_api.h"

#define TAG "usbtsk"

extern uint32_t g_uvc_preview_mode_enable;
extern uint32_t g_usbPicSaveEnable;
extern uint32_t g_ulIfvusbupdate;

extern void ftempl_usb_task(void *paras);
extern void usb_uvc_init(void);

void usb_upgrade_task(void *arg)
{
    while(1) {
        //printf("hello world! count %d \r\n", count++);
        aos_msleep(2000);
		if(g_uvc_preview_mode_enable == false){
			if(g_ulIfvusbupdate==true){	
				printf("su_CheckRoothubStatus \r\n");
				su_CheckRoothubStatus();
				if(SFS_DEVICE_MOUNTED == sfs_devstatus(0))
				{
					printf("got usb devive success!! \r\n");
					if(g_app_flow_cfg.display_enable)
						lv_app_notimce_add(0,60,"Update Firmware");
					aos_msleep(1000);
					extern void fw_update(void);
					fw_update();
				}
			}	
		}
    }
}
extern void setUsbClkEnble();
extern void setUsbClkDisable();
extern int USBDDemoStart(void);
extern int USBHDemoStart(void);
int init_usb_tsk(void)
{
	if(g_uvc_preview_mode_enable == false){
		setUsbClkDisable();
	}
	
	if(g_uvc_preview_mode_enable == true){
		setUsbClkEnble();
    	USBDDemoStart();
    	aos_task_new("ftempl_usb_task", ftempl_usb_task, NULL, 30*1024);
		usb_uvc_init();
	}

	uint32_t g_ulIfvusbupdate = false;
	int usb_up_len = sizeof(g_ulIfvusbupdate);
	int usb_pic_save_len = sizeof(g_usbPicSaveEnable);
	aos_kv_get(IFV_USB_FIRMWARE_ENABLE, &g_ulIfvusbupdate, &usb_up_len);
	aos_kv_get(IFV_USB_PIC_SAVE_ENABLE, &g_usbPicSaveEnable, &usb_pic_save_len);
	if(g_uvc_preview_mode_enable == false){
		if((true == g_ulIfvusbupdate) || (true == g_usbPicSaveEnable)){
			setUsbClkEnble();
	    	USBHDemoStart();
		}
		else{
			printf("usb host mode disable \r\n");
		}
	}
	return 0;
}
