#ifndef _AOLISTHEAD_H
#define _AOLISTHEAD_H


// #define __inline__ 


typedef struct _AoListHead {
	struct _AoListHead *next, *prev;
}AoListHead;

//#define AO_LIST_HEAD_INIT(name) { &(name), &(name) }

#define AO_LIST_HEAD(name) \
	struct AoListHead name = AO_LIST_HEAD_INIT(name)

#define AO_LIST_HEAD_INIT(ptr) do { \
	(ptr)->next = (ptr); (ptr)->prev = (ptr); \
} while (0)


__inline__ void __list_add(AoListHead * nnew,AoListHead * prev,AoListHead * next)
{
	next->prev = nnew;
	nnew->next = next;
	nnew->prev = prev;
	prev->next = nnew;
}

//�����µ��б���head�ĺ���
__inline__ void AoListHeadAdd(AoListHead *nnew, AoListHead *head)
{
	__list_add(nnew,head, head->next);
}
//�����µĽڵ㵽head��ǰ��
// static __inline__ void AoListHeadAddTail(AoListHead *nnew,AoListHead *head)
// {
// 	__list_add(nnew, head->prev, head);
// }

__inline__ void __list_del(AoListHead * prev,AoListHead * next)
{
	next->prev = prev;
	prev->next = next;
}
//�ڵ�ɾ��
__inline__ void AoListHeadDel(AoListHead *entry)
{
	__list_del(entry->prev, entry->next);
}

__inline__ void AoListHeadDelInit(AoListHead *entry)
{
	__list_del(entry->prev, entry->next);
	AO_LIST_HEAD_INIT(entry); 
}

//�б��Ƿ��
__inline__ int AoListHeadEmpty(AoListHead *head)
{
	return head->next == head;
}

//�ڵ���
// static __inline__ void AoListHeadSplice(AoListHead *list,AoListHead *head)
// {
// 	AoListHead *first = list->next;

// 	if (first != list) {
// 		AoListHead *last = list->prev;
// 		AoListHead *at = head->next;

// 		first->prev = head;
// 		head->next = first;

// 		last->next = at;
// 		at->prev = last;
// 	}
// }


// static __inline__ int __list_cmp_same(const void *p1, const void *p2) { return p1 == p2; }

/* ���� */
// static __inline__ void AoListHeadAppend(AoListHead *head, AoListHead *nnew)
// {
// 	AoListHeadAdd((nnew), (head)->prev);
// }

/* ����. */
// static __inline__ void
// AoListHeadPrepend(AoListHead *head, AoListHead *nnew)
// {

// 	AoListHeadAdd(nnew, head);
// }

/* ˳����룬�ڱȽϺ�������ʱ���� */
#define AO_LIST_HEAD_INSERT(head, nnew, cmpfn)				\
do {								\
	struct AoListHead *__i;					\
	for (__i = (head)->next;				\
	     !cmpfn((nnew), (typeof (nnew))__i) && __i != (head);	\
	     __i = __i->next);					\
	AoListHeadAdd((AoListHead *)(nnew), __i->prev);		\
} while(0)


#endif

