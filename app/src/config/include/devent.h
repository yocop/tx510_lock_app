#ifndef _DEVICE_EVENT_H_
#define _DEVICE_EVENT_H_
#include  "aolisthead.h"

typedef enum device_event_state{
	DeviceEventStateInit=0,
	DeviceEventStateOnline,
	DeviceEventStateFail,
	DeviceEventStateUnkown,
	DeviceEventStateNone,
	DeviceEventStateMax
}device_event_state_t;

typedef enum device_event_type{
	DeviceEventTypeNone=0,
	DeviceEventTypeConfigUpdate,
	DeviceEventTypePasslogs,
	DeviceEventTypeOperation,
	DeviceEventType,
	DeviceEventTypeMax
}device_event_type_t;

typedef struct device_event{
	AoListHead				list;
//	AoListHead				link;
	void*					pTypeMemHeap;
	device_event_type_t		type;
	device_event_state_t	state;
	char*					name;
	void*					subscribe;
	unsigned char			data[0];
}device_event_t;





typedef char *(*device_event_cb_t)(void * data);

int device_event_init();
int device_event_end();
device_event_t * device_event_construct(char * name,device_event_type_t type);
int device_event_destruct(device_event_t * evt);
void * device_event_alloc(device_event_t * evt,int size);
int device_event_subscribe(device_event_type_t type,char * name,device_event_cb_t cb);
int device_event_notify(device_event_t * evt);
int device_event_set_data(device_event_t * evt,void * data);
int device_event_config_set_value(device_event_t * evt,char * key,char * value);
int device_event_config_get_string(device_event_t * evt,char * key,char * value);
#endif


