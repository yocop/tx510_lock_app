
#ifndef _AOCONFIG_H
#define _AOCONFIG_H

#include "aolisthead.h"

#define CONFIG_UPDATE			1

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

int  device_config_init();
void device_config_end();
int  device_config_set_value(char * root,char * section,char * name,const char * value);
int  device_config_get_value(char * root,char * section,char * name,char ** value);
#if defined(__cplusplus) || defined(c_plusplus)
}
#endif



#endif
