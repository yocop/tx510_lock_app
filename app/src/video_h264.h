#ifndef VIDEO_H264_H
#define VIDEO_H264_H

#ifdef __cplusplus
extern "C" {
#endif

void video_h264_init(void);
int video_h264_get_data(uint8_t *encoded_data, int max_len, int *is_iframe, long long *time_stamp);
// void video_h264_get_data_done(void);
void video_h264_seek_iframe(int timeout_ms);

#ifdef __cplusplus
}
#endif

#endif