/*
 * Copyright (C) 2019-2020 Alibaba Group Holding Limited
 */
#include <stdio.h>
#include <stdlib.h>

#include <aos/kv.h>
#include <aos/cli.h>
#include <yoc/partition.h>
#include "app_codec.h"

#define HELP_INFO "Usage:\tapp xxx xx\n"

static void cmd_app_func(char *wbuf, int wbuf_len, int argc, char **argv)
{
    if (argc < 2) {
        printf("%s\n", HELP_INFO);
        return;
    }

    if (0 == strcmp(argv[1], "codec_start") ) {
        // extern int g_dump_audio_data_offset;
        // g_dump_audio_data_offset = 0;
        app_codec_input_start();
    } else if (0 == strcmp(argv[1], "codec_stop")) {
        app_codec_input_stop();
    } else if (0 == strcmp(argv[1], "codec_output")) {
        // extern int es8311_codec_output_test();
        // es8311_codec_output_test();
    }
}

void cli_reg_cmd_app(void)
{
    static const struct cli_command cmd_info = {
        "app",
        "app tools",
        cmd_app_func
    };

    aos_cli_register_command(&cmd_info);
}
