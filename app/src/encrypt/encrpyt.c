#include <stdint.h>


int encBytes(uint8_t *bytes, int length, const uint8_t *key, int stride, uint8_t *out)
{
    if (!out)
        out = bytes;
    for(int index = 0; index < length; index++){
        out[index] = bytes[index] ^ key[index % stride];
        out[index] = ~out[index];
    }
    return 0;
}

int decBytes(uint8_t *bytes, int length, const uint8_t *key, int stride, uint8_t *out)
{
    if (!out)
        out = bytes;
    for(int index = 0; index < length; index++){
        bytes[index] = ~bytes[index];
        out[index] = bytes[index] ^ key[index % stride];
    }
    return 0;
}