
int encBytes(uint8_t* bytes, int length, const uint8_t* key, int stride, uint8_t* out);
int decBytes(uint8_t* bytes, int length, const uint8_t* key, int stride, uint8_t* out);