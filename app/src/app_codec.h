#ifndef APP_CODEC_H
#define APP_CODEC_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

int app_codec_init(void);
int app_codec_deinit(void);
int app_codec_input_start(void);
int app_codec_input_stop(void);
int app_codec_input_read(long long *time_stamp, uint8_t *data, int size);
// int es8311_codec_input_get_done(void);
void app_codec_output_start(void);
void app_codec_output_stop(void);
void app_codec_output_write(const uint8_t *data, uint32_t length);

int es8311_codec_get_test(void);

#ifdef __cplusplus
}
#endif

#endif