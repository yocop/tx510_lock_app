/*
 * Copyright (C) Tsingmicro Intelligent Technology Co., Limited
 */
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>
#include <aos/kernel.h>
#include "image_task.h"
#include "uart_task.h"
// #include "ipc_function.h"

#include "debug_perf.h"
#include "if_v.h"
#include "bootpara_deal.h"
#include "dpu.h"
#include "drv_dpu.h"

#include "aos/kv.h"
#include "user_task.h"
#include "drv_lcd_st7789.h"

#include "lvgl.h"
#include "lv_port_disp.h"
#include <math.h>

#include "debug_time.h"
#include "pic_crop.h"
#include "eeprom_read_data.h"
#include "face_result.h" 
#include "drv_isp.h"

#include "wrapper.h"

#include "board_sensor.h"
//#include "base2_720x1280.h"
// #include "A_base_720x1280.h"
#include "faceproc.h"

#include "board.h"
#include <fcntl.h>
#include <cx_facerecog.h>

#include "userFaceSample.h"
#include "usbhdemo.h"
#include "vfs.h"
#include "data_move.h"
#include "app_init.h"
#include "usrcmds.h"
#include "uart_task.h"
#include "cx_hal.h"
#include "posix_init.h"
#include "fatfs.h"
#include "audio_process.h"
#include "h264_vpu.h"
#include "app_codec.h"

#define USB_TRANSMIT_PIC_ENABLE 0
#define USB_HOST_ENABLED        1
#define USER_APP_IMAGE_SAVE_ENABLE  0
#define USER_MULTI_ANGLE_SWITCH     0	
#define SENSETIME_PROTOCOL_ENABLE     0	

#define FLASH_BOOT_ENABLE 		1
#define FACE_RECOG_RETRY_TIME 3
// #define WEIGHT_DATA_FROM_SDCARD

int ifv_debug_level = IFV_MSG_ERROR;

const char check_face_result[][30] = {
	"成功",
	"未检测到人脸",
	"检测到多个人脸",
	"请正对屏幕",
	"图像模糊",
	"人脸有遮挡",
	"2D活检未通过",
	"3D活检未通过",
	"未注册人员",
};

aos_sem_t g_sem_lv_ui_task_sync;
uint8_t bUIFlag = 0;
float fXmin = 0;
float fXmax = 0;
float fYmin = 0;
float fYmax = 0;


lv_coord_t Xmin = 0;
lv_coord_t Xmax = 0;
lv_coord_t Ymin = 0;
lv_coord_t Ymax = 0;


aos_timer_t  g_timer;
// static bool bFrameClear = false;
// static bool bTextClear = false;

extern uint32_t tRecogize;

extern void qw_isp_ae_lock(uint32_t isp_id);
extern void qw_isp_ae_unlock(uint32_t isp_id);
extern void qw_isp_ae_clear_lock(uint32_t isp_id);
extern uint32_t enroll_or_recognize_busy_flag;
extern int g_irSensorConvergent[];
extern aos_mutex_t g_aie_mutex;
int g_register_once_one_person = 0;   //0:去重功能关闭          1:去重功能开启
extern data_flow_func_control_t g_ifv_flow_cfg; 
extern void recognizeResultAck(int result,uint16_t id);
/*
#define QW_ISP_AE_UNLOCK(idx) \
   do{ \
        if(ISP_WORKMODE_IR == isp_instance[idx].mode) qw_isp_ae_unlock(idx); \
       }while(0);
*/

void clearAEConvergentStatus()
{
    memset(g_irSensorConvergent, 0, sizeof(uint32_t) * ISP_NUM);
//    QW_ISP_AE_UNLOCK(ISP_0);
//    QW_ISP_AE_UNLOCK(ISP_1);
//    QW_ISP_AE_UNLOCK(ISP_2);
}

// #define USER_APP_IR_DIR_ROOT FS_ROOT_PATH

// void userAppSaveIrToFile()
// {
//     int fd = -1;
//     uint8_t ucFilePath[128] = {0};
//     uint32_t ulIrFileIndex = 0;
//     unsigned long outSize = 0;
//     unsigned char *pJpegBuffer = NULL;  

//     sprintf((char *)ucFilePath, "%s/IR_%d.jpg", 
//             USER_APP_IR_DIR_ROOT, ulIrFileIndex);
                      
//     fd = aos_open((char *)ucFilePath, O_RDWR | O_CREAT);
//     if (fd < 0) {
//         printf("aos_open err.\n");
//         goto exit;
//     }
// 	extern void ir_to_jpeg(unsigned char *imageData,
//                     unsigned char **compressedBuffer,
//                     int imageWidth,
//                     int imageHeight,
//                     int channels,
//                     unsigned long *outSize);
//     ir_to_jpeg((unsigned char *)iva_2ir_get_buffer(1, 0),
//             &pJpegBuffer,
//             IFV_IR_PIC_WIDTH,
//             IFV_IR_PIC_HIGHT,
//             1,
//             &outSize);
            
//     printf("outSize : %ld \n",outSize);
    
//     if(aos_lseek(fd,0,SEEK_SET) < 0){
//     	printf("mv fd file pos err.\n");
//         goto exit;
//     }

//     if(aos_write(fd, (unsigned char *)pJpegBuffer, outSize) != outSize){
//     	printf("write picture err.\n");
//         goto exit;
//     }

//     printf("save picture %s.\n", ucFilePath);
// exit:
//     if(fd >= 0){
//         aos_close(fd);
//     }

//     if (pJpegBuffer)
//     {
//         aos_free(pJpegBuffer);
//     }
// }

static uint8_t recognize_count;
static uint8_t register_count;
int userGetModuleState()
{
    //return MS_STANDBY, MS_BUSY, MS_ERROR, ERROR.
    return ifvGetFlowState();

}

void userCmdStartStopIspRgb(int action) //0 stop, 1 start
{

	if_vRgbStreamPauseResume( action );
}

//    LED_LIGHT_TYPE_IR,
//    LED_LIGHT_TYPE_SPK,
void userCmdGetIrSpkSnapShot(int led_type)
{
	int len = 640 * 360;
	unsigned char *pBuffer = aos_malloc(len);
	
	if_vGetIrSpkSnapShot(2,2,led_type, pBuffer);
	
	printf("get snapshot pic.\n");
	aos_free(pBuffer);
}

extern int ipc_init(void);
extern void gpio_set(void);
int init()
{
	set_qspi_freq();
	QSPI_flash_init();
	ipc_init();
	gpio_set();

	// aos_mutex_new(&mutex_flash_rw);
    // aos_sem_new(&sem_data_move_over, 0);
	return 0;
}

typedef struct product_info{
	unsigned char sn[14];
	unsigned char hwid[3];
	unsigned char switchV;
	unsigned char hwboardid;
	unsigned int deviceType;
	unsigned char wifi_mac[6];
	unsigned char bt_mac[6];
	unsigned char eth_mac[6];

}product_info_s;


#define PRODUCT_PARAM_PARTITION_START        CSKY_QSPIMEM_BASE + 0x18000	 
#define PRODUCT_PARAM_PARTITION_SIZE         0x1000                  /* 4k bytes */


void setUsbClkEnble()
{
   volatile uint32_t *base;
   uint32_t temp;

   /* config_usb_clk_en */
   base = (uint32_t *)(CSKY_CLKGEN_BASE + USB_CLK_CFG);
   temp = *base;
   /* clkgen_usb_phy_rclk_gate_en */
   temp |= 0x1 << 5;
   /* clkgen_usb_hclk_gate_en */
   temp |= 0x1 << 6;
   temp |= 0x0 << 3;
   temp |= 0x1 << 3;
   *base = temp;

   //usb_phy_clock_de_reset
	base = (uint32_t *)(CSKY_RSTGEN_BASE + 0x10);
	*base |= 0x1 << 12;
}

void setUsbClkDisable()
{
	//close USB PHY
    volatile uint32_t *base;
    base = (uint32_t *)(CSKY_CLKGEN_BASE + USB_CLK_CFG);
    *base &= ~((1 << 5) + (1 << 6));
    *base &= ~(1 << 3);
    *base |= (1 << 3); //sync
	//drv_ae_pwr_down();
}


void drv_module_clk_config(void)
{
    volatile uint32_t *base;
	
//    /* open all mipi clk gate */
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + MIPI_CLK_EN_CFG);
//    *base = 0xffffffff;
//
//    /* aesys */
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + AE_CLK_EN_CFG);
//    *base = 0xffffffff;
//
//    /*efuse pad_ctl i2c0p uart_804 timer0 gpio0~3 adc uart0~1 usi0~6 wdt804*/
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + AP_CLK_EN_CFG0);
//    *base = 0xffffffff;
//
//    /*i2s0~1 i2c0_icclk uart_804_sclk timer0~3_clk uart0~1_sclk(work)*/
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + AP_CLK_EN_CFG1);
//    *base = 0xffffffff;
//
//    /*axi0 i2s0~1_pclk smid dsram0 isram rom damc0*/
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + AP_CLK_EN_CFG2);
//    *base = 0xffffffff;
//
//    /*crypto_aclk&hclk uart805_pclk pwm_pclk i2c1~4_pclk usi7~8_pclk timer1_pclk mail_pclk cvsys_aphb2_pclk*/
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + CV_CLK_EN_CFG0);
//    *base = 0xffffffff;
//
//    /* cvsys -> i2c1~4_icclk uart805_sclk time1~3_scclk cvsys_aclk dmac1_hclk cvsys_ahb1_hclk ck805_cclk*/
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + CV_CLK_EN_CFG1);
//    *base = 0xffffffff;
//
//    /* enable dpu clk */
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + DPU_CLK_CFG);
//    *base |= (0x03 << 8);
//
//    //Enalble USB PHY
//    base = (uint32_t *)(CSKY_CLKGEN_BASE + USB_CLK_CFG);
//    *base |= (1 << 5) + (1 << 6);
//    *base |= (1 << 3); //sync


    //close USB PHY
    base = (uint32_t *)(CSKY_CLKGEN_BASE + USB_CLK_CFG);
    *base &= ~((1 << 5) + (1 << 6));
	*base &= ~(1 << 3);
    *base |= (1 << 3); //sync

}

extern uint8_t *binary_image;
extern uint8_t register_by_ir;
extern sensor_para_t *sensor_para;
ifv_msg_pkg_t msg;
#if defined(AOS_BOARD_TX510_LOCK_GC) \
	|| defined(AOS_BOARD_TX510_LOCK_ST) \
	|| defined(AOS_BOARD_TX510_LOCK_HC) \
	|| defined(AOS_BOARD_TX5103C_LOCK_HC) \
    || defined(AOS_BOARD_TX510_LOCK_WFS) \
	|| defined(AOS_BOARD_TX510_LOCK_HV)  \
	|| defined(AOS_BOARD_TX510_ZHIXIN) \
	|| defined(AOS_BOARD_TX510_LOCK_DUALIR_ABHAM)  // Dual IR
data_flow_func_control_t g_app_flow_cfg =
{
    .source                 = 0,
    .sink                   = 0, 
    .preview_mode           = UVC_NONE_MODE,
	
	.guidefilter_enable     = 0,
	.detect_vis_enable  	= 0,
	.detect_nir_enable		= 1,
	.live_vis_enable		= 0,
	.live_nir_enable		= 1,
	.live_depth_face_enable = 0,
	.live_depth_luma_enable = 0,

	.jpeg_proc_enable		= 1,

	.recog_vis_enable		= 0,
	.recog_ir_enable		= 1,

	.cat_eye_enable         = 0,

	.display_enable			= 0,
	.display_ir				= 0,
	.display_vis			= 0,
	
	.source_ir_left			= 1,  //by camera view:default
	.source_ir_right		= 1,  //by camera view
	.source_vis				= 0,
	.source_spk				= 0,

	.depth_doubleIr_enable	= 0,
	
	.register_vis			= 0,
	.register_nir			= 1,
	.register_live			= 1,
	.register_pic			= 0,
	
	.enResIR  				= VEDEO_RES_640x360,
    .enResRGB               = VEDEO_RES_640x360,
    .ck805_enable			= 1,
    .facematch_thres		= 0.1437f,
	.save_ae_enable         = 1,
    .face_ae_ir             = 1,
	.reco_luma_adjust_ir	= 1,
	
	.mid_reset_flag			= 0,
	.mid_face_reset_flag    = 0,
	.mid_debug_mode_flag    = 0,
	.sensetime_protocol_enable = 0,
	
    .max_face_num           = FACE_FEATURE_MAX_NUM,
};
#elif defined(AOS_BOARD_TX510_LOCK_TRANGLE_FT) 
data_flow_func_control_t g_app_flow_cfg =
{
    .source                 = 0,
    .sink                   = 0, 
    .preview_mode           = UVC_NONE_MODE,
	
	.guidefilter_enable     = 0,
	.detect_vis_enable  	= 0,
	.detect_nir_enable		= 1,
	.live_vis_enable		= 0,
	.live_nir_enable		= 1,
	.live_depth_face_enable = 0,
	.live_depth_luma_enable = 0,

	.jpeg_proc_enable		= 1,

	.recog_vis_enable		= 0,
	.recog_ir_enable		= 1,

	.cat_eye_enable         = 0,

	.display_enable			= 0,
	.display_ir				= 0,
	.display_vis			= 0,
	
	.source_ir_left			= 1,  //by camera view:default
	.source_ir_right		= 1,  //by camera view
	.source_vis				= 0,
	.source_spk				= 0,

	.depth_doubleIr_enable	= 0,
	
	.register_vis			= 0,
	.register_nir			= 1,
	.register_live			= 1,
	.register_pic			= 0,
	
	.enResIR  				= VEDEO_RES_800x600,
    .enResRGB               = VEDEO_RES_640x360,
    .ck805_enable			= 1,
    .facematch_thres		= 0.1437f,
	.save_ae_enable         = 1,
    .face_ae_ir             = 1,
	.reco_luma_adjust_ir	= 0,
	
	.mid_reset_flag			= 0,
	.mid_face_reset_flag    = 0,
	.mid_debug_mode_flag    = 0,
	.sensetime_protocol_enable = 0,
	
    .max_face_num           = FACE_FEATURE_MAX_NUM,
    .mi_data_ayout          = DATA_LAYOUT_INTERLEAVED,
};
#elif defined(AOS_BOARD_TX510_EVB2_1)
data_flow_func_control_t g_app_flow_cfg =
{
    .source                  = 0,
    .sink                    = 0, 
    .preview_mode           = UVC_NONE_MODE,
	.detect_vis_enable  	= 0,
	.detect_nir_enable		= 1,
	.live_vis_enable		= 0,
	.live_nir_enable		= 1,
	.live_depth_face_enable = 0,
	.live_depth_luma_enable = 1, //0, //1,

	.jpeg_proc_enable		= 1,

	.recog_ir_enable		= 1,

	.mask_enable            = 0,

	.cat_eye_enable         = 0,

	.display_enable			= 1,
	.display_ir				= 0,
	.display_vis			= 0,
	
	.source_ir_left			= 1,  //by camera view:default
	.source_ir_right		= 0,  //by camera view
    .source_vis             = 1,
    .recog_vis_enable		= 0,
    .register_vis           = 0,
	.enResIR  				= VEDEO_RES_640x360,
	.source_spk				= 0,

	.depth_doubleIr_enable	= 0,

	.register_nir			= 1,
	.register_live			= 1,
	.register_pic			= 0,

    .enResRGB               = VEDEO_RES_640x360,
    .ck805_enable			= 1,
	.facematch_thres		= 0.1437f,
    .save_ae_enable         = 1,
    .face_ae_ir             = 0,
	.reco_luma_adjust_ir	= 0,
	
	.mid_reset_flag			= 0,
	.mid_face_reset_flag    = 0,
	.mid_debug_mode_flag    = 0,
	.sensetime_protocol_enable = 0,
	
    .max_face_num           = FACE_FEATURE_MAX_NUM,
};
#elif defined(AOS_BOARD_TX5103C_EVB2_1)
    data_flow_func_control_t g_app_flow_cfg =
    {
        .source                  = 0,
        .sink                    = 0, 
        .preview_mode           = UVC_NONE_MODE,
        .detect_vis_enable      = 0,
        .detect_nir_enable      = 1,
        .live_vis_enable        = 0,
        .live_nir_enable        = 1,
        .live_depth_face_enable = 0,
        .live_depth_luma_enable = 1, //0, //1,

		.jpeg_proc_enable		= 1,
    
        .recog_ir_enable        = 1,
    
        .mask_enable            = 0,

		.cat_eye_enable         = 0,   

        .display_enable         = 1,
        .display_ir             = 0,
        .display_vis            = 0,
        
        .source_ir_left         = 1,  //by camera view:default
        .source_ir_right        = 0,  //by camera view
        .source_vis             = 1,
        .recog_vis_enable       = 0,
        .register_vis           = 0,
        .enResIR                = VEDEO_RES_640x360,
        .source_spk             = 0,

		.depth_doubleIr_enable	= 0,
    
        .register_nir           = 1,
        .register_live          = 1,
        .register_pic           = 0,
    
        .enResRGB               = VEDEO_RES_640x360,
        .ck805_enable           = 1,
        .facematch_thres        = 0.1437f,
		.save_ae_enable         = 0,
		.face_ae_ir             = 1,
		.reco_luma_adjust_ir	= 0,
		
		.mid_reset_flag			= 0,
		.mid_face_reset_flag    = 0,
		.mid_debug_mode_flag    = 0,
		.sensetime_protocol_enable = 0,
		
		.max_face_num           = FACE_FEATURE_MAX_NUM,
};

#elif defined(AOS_BOARD_TX510_EVB) || defined(AOS_BOARD_TX510_EVB2) \
	|| defined(AOS_BOARD_TX510_LOCK_NC) \
	|| defined(AOS_BOARD_TX510_LOCK_SL3D_ABHAM) // Structure Light
data_flow_func_control_t g_app_flow_cfg =
{
    .source                  = 0,
    .sink                    = 0, 
    .preview_mode           = UVC_NONE_MODE,
	.detect_vis_enable  	= 0,
	.detect_nir_enable		= 1,
	.live_vis_enable		= 0,
	.live_nir_enable		= 1,
	.live_depth_face_enable = 0,
	.live_depth_luma_enable = 1, //0, //1,

	.jpeg_proc_enable		= 1,

	.recog_ir_enable		= 1,

	.mask_enable            = 0,

	.cat_eye_enable         = 0,

	.display_enable			= 1,
	.display_ir				= 0,
	.display_vis			= 0,
	
	.source_ir_left			= 1,  //by camera view:default
	.source_ir_right		= 0,  //by camera view
#if defined(MINI_STRUCTURE_LIGHT_MODULE)
	.source_vis				= 0,
	.recog_vis_enable		= 1,
    .register_vis           = 0,
	.enResIR  				= VEDEO_RES_720P,
#else
    .source_vis             = 1,
    .recog_vis_enable		= 1,
    .register_vis           = 1,
	.enResIR  				= VEDEO_RES_640x360,
#endif
	.source_spk				= 1, //0, //1,

	.register_nir			= 1,
	.register_live			= 1,
	.register_pic			= 0,
	    
    .enResRGB               = VEDEO_RES_640x360,
    .ck805_enable			= 1,
	.facematch_thres		= 0.1437f,
    .save_ae_enable         = 1,
    .face_ae_ir             = 1,
	.reco_luma_adjust_ir	= 0,
	
	.mid_reset_flag			= 0,
	.mid_face_reset_flag    = 0,
	.mid_debug_mode_flag    = 0,
	.sensetime_protocol_enable = 0,
	
    .max_face_num           = FACE_FEATURE_MAX_NUM,
};
#elif defined(AOS_BOARD_TX510_LOCK_ZC) // Single IR
data_flow_func_control_t g_app_flow_cfg =
{
    .source                 = 0,
    .sink                   = 0, 
    .preview_mode           = UVC_NONE_MODE,
	
	.guidefilter_enable     = 0,
	.detect_vis_enable  	= 0,
	.detect_nir_enable		= 1,
	.live_vis_enable		= 0,
	.live_nir_enable		= 1,
	.live_depth_face_enable = 0,
	.live_depth_luma_enable = 0,

	.jpeg_proc_enable		= 1,

	.recog_vis_enable		= 0,
	.recog_ir_enable		= 1,

	.mask_enable            = 0,
	
	.cat_eye_enable         = 0,

	.display_enable			= 1,
	.display_ir				= 0,
	.display_vis			= 0,
	
	.source_ir_left			= 1,  //by camera view:default
	.source_ir_right		= 0,  //by camera view
	.source_vis				= 1,
	.source_spk				= 0,
	
	.register_vis			= 0,
	.register_nir			= 1,
	.register_live			= 1,
	.register_pic			= 0,
	
	.enResIR  				= VEDEO_RES_640x360,
    .enResRGB               = VEDEO_RES_640x360,
    .ck805_enable			= 1,
    .facematch_thres		= 0.1437f,
    .save_ae_enable         = 1,
    .face_ae_ir             = 1,
	.reco_luma_adjust_ir	= 0,
	
	.mid_reset_flag			= 0,
	.mid_face_reset_flag    = 0,
	.mid_debug_mode_flag    = 0,
	.sensetime_protocol_enable = 0,
	
        .max_face_num           = FACE_FEATURE_MAX_NUM,
    };
#elif defined(AOS_BOARD_TX510_TOF) 
data_flow_func_control_t g_app_flow_cfg =
{
    .source                 = 0,
    .sink                   = 0, 
    .preview_mode           = UVC_NONE_MODE,
	
	.guidefilter_enable     = 0,
	.detect_vis_enable  	= 0,
	.detect_nir_enable		= 1,
	.live_vis_enable		= 0,
	.live_nir_enable		= 1,
	.live_depth_face_enable = 0,
	.live_depth_luma_enable = 0,

	.jpeg_proc_enable		= 0,

	.recog_vis_enable		= 0,
	.recog_ir_enable		= 1,

	.mask_enable            = 0,
	.cat_eye_enable         = 0,

	.display_enable			= 0,
	.display_ir				= 0,
	.display_vis			= 0,
	
	.source_ir_left			= 0,  //by camera view:default
	.source_ir_right		= 0,  //by camera view
	.source_ir_tof          = 1,
	.source_vis				= 0,
	.source_spk				= 0,
	
	.register_vis			= 0,
	.register_nir			= 1,
	.register_live			= 1,
	.register_pic			= 0,
	
	.enResIR  				= VEDEO_RES_640x241x5,
    .enResRGB               = VEDEO_RES_640x241x5,
    .ck805_enable			= 1,
    .facematch_thres		= 0.1437f,
    .save_ae_enable         = 0,
    .face_ae_ir             = 0,
	.reco_luma_adjust_ir	= 0,
	
	.mid_reset_flag			= 0,
	.mid_face_reset_flag    = 0,
	.mid_debug_mode_flag    = 0,
	.sensetime_protocol_enable = 0,
	
    .max_face_num           = FACE_FEATURE_MAX_NUM,
};
#else
#error "No Configuration"
#endif

extern void registerResultAck(int result,uint16_t id);
void face_register_cb(cx_fr_register_result_t *result)
{
	int res = result->success ? 0 : 1;

	registerResultAck(res, result->user_id);

	if(result->success)
	{
		//lv_clear_text();
		//lv_write_text_id_by_lable(10,50,"sucess", (c(CmdRegisterCBinfo_S *)pBuff)->id); 
		cx_fr_user_info_t user_info = {"yohn", 0};			// add user spec info
	
		cx_facerecog_add_user_info(result->user_id, &user_info);
		lv_app_notimce_add(0,0, "注册成功 :    ID = %d", result->user_id); 
		register_count = 0;
	}
	else
	{
		if(1){//(false == g_usbPicSaveEnable){
			register_count++;
			if(register_count < FACE_RECOG_RETRY_TIME){
				ifv_msg_pkg_t msg;
    			msg.cmd = APP2IFV_REGISTER_CMD;
    			ifv_post_msg(msg);
			} else{
				clearAEConvergentStatus();
                lv_app_notimce_add(0,0,"注册失败: %s",check_face_result[res]);
				register_count = 0;
			}
		}
		else{
            clearAEConvergentStatus();
			lv_app_notimce_add(0,0,"注册失败: %s",check_face_result[res]);
		}
	}
}
extern uint8_t g_imageAcquisitionFlag;
void face_verify_cb(cx_fr_verify_result_t *result)
{
	int res = result->result;

	printf("face verify result %d\n", res);
	
	if(res == CX_VERIFY_RESULT_SUCCESS)
	{
		//lv_clear_text();
		//lv_write_text_id_time_by_lable(10,0,"sucess", ((CmdRecognizeCBinfo_S *)pBuff)->id, tRecogize); 
		recognizeResultAck(res, result->user_id);
		lv_app_notimce_add(0,0,"验证通过:   ID=%d %dms", result->user_id, tRecogize);
		//lv_app_notimce_add(0,0,"验证通过: %d ", ((CmdRecognizeCBinfo_S *)pBuff)->id);
		tRecogize = 0;
		recognize_count = 0;
		
		clearAEConvergentStatus();
	}
	else
	{
	    clearAEConvergentStatus();
		if(1){//(false == g_usbPicSaveEnable){
			recognize_count++;
			if(recognize_count < FACE_RECOG_RETRY_TIME){
				ifv_msg_pkg_t msg;
    			msg.cmd = APP2IFV_RECOGNIZE_CMD;
    			ifv_post_msg(msg);
			} else{
				recognizeResultAck(res, result->user_id);
                lv_app_notimce_add(0,0,"验证失败: %s",check_face_result[res]);
				recognize_count = 0;
				enroll_or_recognize_busy_flag = 0;
			}
		}
		else{
            recognizeResultAck(res, result->user_id);
			lv_app_notimce_add(0,0,"验证失败: %s",check_face_result[res]);
		}
		//just for image Acquisition
		if (g_imageAcquisitionFlag & 0x1){
			if (g_imageAcquisitionFlag & 0x2){
				g_imageAcquisitionFlag = 0;
				return;
			}
			ifv_msg_pkg_t msg;
			msg.cmd = APP2IFV_RECOGNIZE_CMD;
			ifv_post_msg(msg);  
			return;
		}
	}
}


#include <k_api.h>
int aos_task_delete(char *name)
{
    int ret;
    ktask_t *task;

    if (name == NULL) {
        return -EINVAL;
    }

    task = krhino_task_find(name);
    if (task == NULL) {
        return -EINVAL;
    }

    ret = (int)krhino_task_dyn_del(task);
    if (ret == RHINO_SUCCESS) {
        return 0;
    }

    return ret;
}

void app_cli_init()
{
	extern void cli_reg_cmd_iperf(void);
	extern void cli_reg_cmd_ping(void);
	extern void cli_reg_cmd_free(void);
	extern void cli_reg_cmd_ps();
	extern void cli_reg_cmd_lv(void);
	extern void cli_reg_cmd_kvtool(void);
	extern void cli_reg_cmd_fr(void);
	extern void cli_reg_cmd_sysinfo(void);
	extern void cli_reg_cmd_ntp(void);

    fs_cmds_cli_register();
    cli_reg_cmd_iperf();
    cli_reg_cmd_ping();
    cli_reg_cmd_free();
    cli_reg_cmd_ps();
    cli_reg_cmd_lv();
	cli_reg_cmd_kvtool();
	net_cmds_cli_register();
	cli_reg_cmd_fr();
	cli_reg_cmd_sysinfo();
	cli_reg_cmd_ntp();
}


int main()
{
    int ret = 0;
	float ir_expo;

    // must call this func when use c++
    posix_init();
    // must call this func when use c++
	extern void cxx_system_init(void);
    cxx_system_init();

	extern void board_yoc_init(void);
	board_yoc_init();
	printf("cpu0 freq:%d cpu1 freq:%d\n", drv_get_cpu_freq(0), drv_get_cpu_freq(1));
	printf("AHB0 freq:%d AHB1 freq:%d\n", drv_get_ahb_freq(0), drv_get_ahb_freq(1));
	printf("APB0 freq:%d APB1 freq:%d APB2 freq:%d\n", drv_get_apb_freq(0), drv_get_apb_freq(1), drv_get_apb_freq(2));
	
	printf("App! date %s, time %s \r\n", __DATE__, __TIME__); //This is Version Tag, Please do not delete this note yyds

	aos_vfs_init();
	app_cli_init();

#if defined(AOS_COMP_LITTLEFS) && defined(LITTLEFS_EXAMPLE)
	// lfs_official_modify_demo();
	lfs_example_cmds_cli_register();
	lfs_api_test_cmds_cli_register();
#endif

#if defined(AOS_COMP_LITTLEFS)
	lfs_tools_cmds_cli_register();
#endif

#ifdef AOS_COMP_FATFS
	fatfs_register();
#endif

#ifdef AOS_COMP_KV
	ifv_getIspExpValueKv(&ir_expo);
	extern int if_vPreviewModeGet();
	if_vPreviewModeGet();
#endif

#if FLASH_BOOT_ENABLE
    first_time_bootup();
#ifdef WEIGHT_DATA_FROM_SDCARD
	weight_data_move_from_sdcard(NULL);
#else
	flash_data_move_task(NULL);
#endif
#endif
	extern void hw_init(void);
	hw_init();
	

#define UART_TASK_PRI 35
    aos_task_t uart_task_thread;
    ret = aos_task_new_ext(&uart_task_thread, "uart_task", uart_task, NULL, 10*1024, UART_TASK_PRI);

#if FLASH_BOOT_ENABLE
#ifndef WEIGHT_DATA_FROM_SDCARD
	check_flash_data_move_done();
#endif
#endif

	cx_fr_config_t settings;
	settings.info_save_path = "/sdcard/user_info";
	settings.alg_spec = (void *)&g_app_flow_cfg;

	cx_facerecog_init(&settings, NULL, face_register_cb, face_verify_cb);

	if(g_app_flow_cfg.display_enable)
		lv_gui_init();

#if USER_MULTI_ANGLE_SWITCH
	user_face_initial_sample();
#endif
 
	if_v_cli_register();

#if FLASH_BOOT_ENABLE
#ifndef WEIGHT_DATA_FROM_SDCARD
     check_memory_data_consistency();
#endif
#endif

	extern void app_fota_init(void);
	app_fota_init();

	extern void cx_app_init(void);
	cx_app_init();
	h264_vpu_init();
	app_audio_init();
	app_codec_output_start();

	extern int init_usb_tsk(void);
	init_usb_tsk();

	//创建按键检测任务
    extern void button_check_entry(void *paras);
    aos_task_t button_thread;
    ret = aos_task_new_ext(&button_thread, "button_check", button_check_entry, NULL, 10*1024, 50);
    if (ret != 0) {
		printf("button task create failed\r\n");
    }
}