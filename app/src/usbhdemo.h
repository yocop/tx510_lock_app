#ifndef _USBHDEMO_H_
#define _USBHDEMO_H_
#define SMXUSBH 1
#define SMXUSBH_DEMO
//#define SMXUSBD_ON 1


extern int USBHDemo(void);
extern int USBHDemoStart(void);
extern int USBHDemoStop(void);
void usb_save_double_ir_pic(int32_t algoType, int32_t result, uint8_t *irData_0, int32_t dataLen_0, uint8_t *irData_1, int32_t dataLen_1);
#endif
