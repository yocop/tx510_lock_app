#ifndef TEST_MJPG_H
#define TEST_MJPG_H
#include <aos/kernel.h>



extern int g_jpeg_Size;
extern int g_usb_send_flag;
extern uint32_t VPU_OUT_UVC_BASE;
//extern char jpg_out_addr[640*480]; 
extern char *jpg_out_addr; 
extern int usb_vpu_on;


#define VPU_NUM 2
enum _VPU_JPG{
	JPG_IN,
	JPG_OUT,
	JPG_NUM
};



typedef struct _Link
{
	unsigned char *buf;
	int 	len;	//this node valid size
	int 	dirty;	//dirty=1 buffer is full. dirty=0 buffer is empty.
}VpuLinkNode;

typedef struct _LinkQueue
{
	unsigned int node_num_;
	unsigned int link_size_;
    int front;
    int rear;
    int count;
	char *headptr;
	VpuLinkNode *link;
}VpuLinkQueue;

typedef struct _VpuTsmPrivate{	
	VpuLinkQueue queue[VPU_NUM];
	aos_mutex_t mutex[VPU_NUM];
	aos_sem_t sem[VPU_NUM];

	
}VpuPrivate_t;

VpuPrivate_t VpuPrivate;




int VpuQueueInit(VpuLinkQueue *Q, int node_num, int link_size);
int VpuQueueFull(VpuLinkQueue *Q);
int VpuQueueEmpty(VpuLinkQueue *Q);
int VpuQueueSendFull(VpuLinkQueue *Q, VpuLinkNode *Node, int len);
int VpuQueueSendEmpty(VpuLinkQueue *Q, VpuLinkNode *Node, int len);
void VpuQueueDestroy(VpuLinkQueue *Q);
VpuLinkNode* VpuGetEmptyNode(VpuLinkQueue *Q);
VpuLinkNode* VpuGetFullNode(VpuLinkQueue *Q);
// static int vpu_queue_clear_dirty();




#endif

