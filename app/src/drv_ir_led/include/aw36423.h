/**
 * File Name: aw36423.h 
 *
 * Copyright (C) 2020 Fortsense Corporation. <http://www.fortsense.com>&gt
 *
 * Author:  Linsh 
 *
 * Date: 2021/04/21
 *
 * Project: tx510_bf2253
 *
 * Description:
 *
 * HISTORY
 *
 **/
#ifndef __AW36423_H__
#define __AW36423_H__

#if defined (__cplusplus)
extern "C" {
#endif /* __cplusplus */

#include <stdio.h>
#include "iic_function.h"

#define FS_AW36423_CHIP_ID                     0x17

#define FS_AW36423_CHIP_ID_REG                 0x00
#define FS_AW36423_ENABLE_REG                  0x01
#define FS_AW36423_IVFM_REG                    0x02
#define FS_AW36423_LED1_FLASH_BRIGHTNESS_REG   0x03
#define FS_AW36423_LED1_TORCH_BRIGHTNESS_REG   0x04
#define FS_AW36423_LED2_FLASH_BRIGHTNESS_REG   0x05
#define FS_AW36423_LED2_TORCH_BRIGHTNESS_REG   0x06
#define FS_AW36423_FLASH_TORCH_TIMING_REG      0x0b
#define FS_AW36423_FLAGS_REG                   0x0f
#define FS_AW36423_DEVICE_ID_REG               0x10  
#define LED_STROBE

typedef enum
{
    LED_TORCH_MODE = 0,
    LED_FLASH_MODE = 1,
    LED_IR_MODE    = 2,
} led_mode_t;

int ir_led_init(i2c_dev_t *i2c, uint32_t dev_addr);

// int ir_led_finalize();

int32_t led_check_enable(void);

int32_t led_read_flag(uint32_t reg);

int ir_led_power_on(void);

int ir_led_power_off(void);


// int speckle_power_on(void);
// int speckle_power_on_outside(void);

// int32_t led_config_reglist(uint32_t addr, uint32_t data);

// uint32_t led_lookup_reg(uint32_t addr);

#if defined (__cplusplus)
}
#endif /* __cplusplus */

#endif
