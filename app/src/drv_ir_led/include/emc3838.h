#ifndef _IR_LED_H_
#define _IR_LED_H_

#include <stdio.h>
#include "iic_function.h"

#define IRLED_REG_CHIPID      0x00
#define IRLED_REG_ENABLE      0x01
#define IR_CURRENT_REG        0x05
#define SPK_CURRENT_REG       0x03
#define TIMING_REG            0x0b
#define IRLED_REG_FLAGS       0x0F
#define CURRENT_MUL           3.91f
#define CURRENT_ADD           3.91f
#define LED_STROBE

typedef enum
{
    LED_NO_NEED_CONFIG = 0,
    LED_NEED_CONFIG = 1,
} led_config_t;


extern led_config_t g_led_config;

int ir_led_init(i2c_dev_t *i2c, uint32_t dev_addr);

int ir_led_finalize();

int32_t led_check_enable(void);

int32_t led_read_flag(uint32_t reg);

int ir_led_power_on(void);

int ir_led_power_off(void);

int speckle_power_on(void);

int32_t led_config_reglist(uint32_t addr, uint32_t data);

uint32_t led_lookup_reg(uint32_t addr);

#endif
