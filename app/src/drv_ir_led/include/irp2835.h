/******************************************************************************
 * @file     aw99703 i2c driver, control backlight of mipi lcd
 * @brief    header file for dpu driver
 ******************************************************************************/

#ifndef _IRP2835_H_
#define _IRP2835_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define IRLED_REG_CHIPID      0x00
#define IRLED_REG_ENABLE      0x01
#define IR_CURRENT_REG        0x03
#define SPK_CURRENT_REG       0x04
#define TIMING_REG            0x08
#define IRLED_REG_FLAGS       0x0A
#define CURRENT_MUL           7.83f
#define CURRENT_ADD           3.91f
//#define LED_STROBE

typedef enum
{
    LED_NO_NEED_CONFIG = 0,
    LED_NEED_CONFIG = 1,
} led_config_t;

extern led_config_t g_led_config;

int ir_led_finalize();
int32_t led_read_flag(uint32_t reg);
int speckle_power_on(void);
int speckle_power_on_outside(void);
int32_t led_config_reglist(uint32_t addr, uint32_t data);
uint32_t led_lookup_reg(uint32_t addr);

extern int ir_led_init();
extern int ir_led_power_on();
extern int ir_led_power_off();

#ifdef __cplusplus
}
#endif

#endif /* _DRV_DPU_H_ */

