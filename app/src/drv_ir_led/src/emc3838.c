#include <stdio.h>
#include "iic_function.h"
#include "emc3838.h"
#include "reg_deal.h"



i2c_dev_t i2c_ir_led;
led_config_t g_led_config = LED_NO_NEED_CONFIG;

regval_list irled_start[] = {
	{0x01 ,0x32}, //LED1:speckle;LED2~LED4,ir
	{0x02 ,0xe1},
    {SPK_CURRENT_REG ,0x7f},
    {IR_CURRENT_REG ,0x7f},
	//{0x07 ,0xcd},
    //{0x08 ,0x7f},
	//{0x09 ,0xcd},
    //{0x0a ,0x7f},
	{0x0b ,0x0f}, //0x9a
};
const  regval_list irled_open[] = {
	{0x01 ,0x32}, //LED1:speckle;LED2~LED4,ir
};


const  regval_list irled_stop[] = {
	{0x01 ,0x00},
};

const  regval_list speckle_start[] = {
	{0x01 ,0x31}, //32
	//{0x02 ,0xe1},
	//{0x03 ,0x7f},
	//{0x05 ,0x7f},
	//{0x0b ,0x1f},
};

static int32_t check_id_irled(i2c_dev_t *i2c)
{
    uint8_t data = 0;
    int32_t ret = iic_read(i2c, IRLED_REG_CHIPID, &data);
    //printf("ir led id: %02x \n",data);
    if (ret < 0 || data != 0x17)
        return -1;

    return ret;
}

int ir_led_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
	int32_t ret;
#if 0
    if (camera1_init_flag == 0)
    {
        example_pin_camera_iic1_init();
    }

	i2c_ir_led.port = IIC_PORT_USI_11;
    i2c_ir_led.config.dev_addr = CK_IIC_SLAVE_ADDR_IRLED;
    PRINTF_LOG("ir led init \n");
    ret = hal_i2c_init(&i2c_ir_led);
    if (ret < 0) {
        printf("ir led i2c init failed \n");
        return ret;
    }
#endif
	memcpy(&i2c_ir_led, i2c, sizeof(i2c_dev_t));
	i2c_ir_led.config.dev_addr = dev_addr;
    ret = check_id_irled(&i2c_ir_led);
    if (ret < 0) {
        printf("ir led check id fail\n");
        return ret;
    }
    return ret;
}

int ir_led_finalize()
{
	return 0;
}

int32_t led_check_enable(void)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, IRLED_REG_ENABLE, &data);
    if (ret < 0)
        return ret;
    return (data & 0x30); //According to DS, all mode bits in enable register should be cleard after flash timeout
}

int32_t led_read_flag(uint32_t reg)
{
	uint8_t data = 0;
	int32_t ret = iic_read_r8_d8(&i2c_ir_led, reg, &data);
	//printf("ir flag %02x \n",data);
	if (ret < 0)
		return ret;
	return data;
}

int ir_led_power_on(void)
{
	int32_t ret = 0;
    //ret = iic_write_array(pcsi_iic_3, (struct regval_list *)irled_start, ARRAY_SIZE(irled_start));
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)irled_start, ARRAY_SIZE(irled_start));
    if (ret < 0) {
        return ret;
    }
	//iic_read_array_r8_d8(&i2c_ir_led,(regval_list *)irled_start, ARRAY_SIZE(irled_start));
    return ret;
}

int ir_led_power_off(void)
{
	int32_t ret;
	ret = iic_write_array_r8_d8(&i2c_ir_led,  (regval_list *)irled_stop, ARRAY_SIZE(irled_stop));
	if (ret < 0) {
		printf("ir led close failed \n");

	}
	return ret;
}

int speckle_power_on(void)
{
    int32_t ret;
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));
    if (ret < 0) {
		printf("ir led close failed \n");

	}
	return iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));	
}


int32_t led_config_reglist(uint32_t addr, uint32_t data)
{
    uint32_t array_size = 0;
    int32_t ret;
    
    array_size = ARRAY_SIZE(irled_start);
    config_reglist(irled_start, array_size, addr, data);

    return 0;
}


uint32_t led_lookup_reg(uint32_t addr)
{
    uint32_t array_size = 0;
    
    array_size = ARRAY_SIZE(irled_start);
    return lookup_reg(irled_start, array_size, addr);
}