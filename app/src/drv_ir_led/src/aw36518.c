#include <stdio.h>
#include "iic_function.h"
#include "AW36518.h"
#include "reg_deal.h"

i2c_dev_t i2c_ir_led;
led_config_t g_led_config = LED_NO_NEED_CONFIG;

regval_list irled_start[] = {
    {0x01 ,0x0f}, //flash mode
    {0x08 ,0x1c},
    {0x03 ,0x28}, //flash brightness 0x2a
};
const  regval_list irled_open[] = {
	{0x01 ,0x32}, //LED1:speckle;LED2~LED4,ir
};

const  regval_list irled_stop[] = {
	{0x01 ,0x00},
};

const  regval_list speckle_start[] = {
	{0x01 ,0x31}, //32

};

static int32_t check_id_irled(i2c_dev_t *i2c)
{
    uint8_t data = 0;
    int32_t ret = iic_read(i2c, IRLED_REG_CHIPID, &data);
    printf("ir led id: %02x \n",data);
    if (ret < 0 || data != 0x30)
        return -1;

    return ret;
}

int ir_led_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
	int32_t ret;
	memcpy(&i2c_ir_led, i2c, sizeof(i2c_dev_t));
	i2c_ir_led.config.dev_addr = dev_addr;
    ret = check_id_irled(&i2c_ir_led);
    if (ret < 0) {
        printf("ir led check id fail\n");
        return ret;
    }
    return ret;
}

int ir_led_finalize()
{
	return 0;
}

int32_t led_check_enable(void)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, IRLED_REG_ENABLE, &data);
    if (ret < 0)
        return ret;
    return (data & 0x30); //According to DS, all mode bits in enable register should be cleard after flash timeout
}

int32_t led_read_flag(uint32_t reg)
{
	uint8_t data = 0;
	int32_t ret = iic_read_r8_d8(&i2c_ir_led, reg, &data);
	if (ret < 0)
		return ret;
	return data;
}

int ir_led_power_on(void)
{
	int32_t ret = 0;
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)irled_start, ARRAY_SIZE(irled_start));
    if (ret < 0) {
        return ret;
    }
    return ret;
}

int ir_led_power_off(void)
{
	int32_t ret;
	ret = iic_write_array_r8_d8(&i2c_ir_led,  (regval_list *)irled_stop, ARRAY_SIZE(irled_stop));
	if (ret < 0) {
		printf("ir led close failed \n");

	}
	return ret;
}

int speckle_power_on(void)
{
    int32_t ret;
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));
    if (ret < 0) {
		printf("ir led close failed \n");

	}
	return iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));	
}


int speckle_power_on_outside(void)
{
    return 0;
}

int32_t led_config_reglist(uint32_t addr, uint32_t data)
{
    uint32_t array_size = 0;
    int32_t ret;
    
    array_size = ARRAY_SIZE(irled_start);
    config_reglist(irled_start, array_size, addr, data);

    return 0;
}

uint32_t led_lookup_reg(uint32_t addr)
{
    uint32_t array_size = 0;
    
    array_size = ARRAY_SIZE(irled_start);
    return lookup_reg(irled_start, array_size, addr);
}
