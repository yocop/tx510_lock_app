#include <stdio.h>
#include "iic_function.h"
#include "am36515.h"
#include "reg_deal.h"

#define CURRENT_500MA   0x3F
#define CURRENT_400MA   0x32
#define CURRENT_300MA   0x26
#define CURRENT_250MA   0x1F
#define LED_FLASH_BRIGHTNESS_CURRENT    CURRENT_500MA

i2c_dev_t i2c_ir_led;
led_config_t g_led_config = LED_NO_NEED_CONFIG;

regval_list irled_start[] = {    
#if defined(LED_STROBE)
	{0x01 ,0x2D}, //LED1:LED2  ir
#else    
	{0x01 ,0x0D}, //LED1:speckle;LED2~LED4,ir  //0x2D
#endif    
	
	{0x02 ,0x01},

    {0x03, LED_FLASH_BRIGHTNESS_CURRENT}, 
  
};
const  regval_list irled_open[] = {
#if defined(LED_STROBE)    
    {0x01 ,0x2D},
#else
	{0x01 ,0x0D}, //LED1:speckle;LED2~LED4,ir  0x33
#endif    
};


const  regval_list irled_stop[] = {
#if defined(LED_STROBE)    
    {0x01 ,0x00},
#else    
	{0x01 ,0x00},
#endif    
};

const  regval_list speckle_start[] = {
#if defined(LED_STROBE)  
    {0x01 ,0x2D},
#else    
	{0x01 ,0x23}, //32
#endif    
};

static int32_t check_id_irled(i2c_dev_t *i2c)
{
    uint8_t data = 0;
    int32_t ret = iic_read(i2c, IRLED_REG_CHIPID, &data);
    printf("ir led id: %02x \n",data);
    if (ret < 0 || data != 0x30)
        return -1;

    return ret;
}

int ir_led_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
	int32_t ret;
	char *str;
    uint32_t brightness = 0;
    
	memcpy(&i2c_ir_led, i2c, sizeof(i2c_dev_t));
	i2c_ir_led.config.dev_addr = dev_addr;
    ret = check_id_irled(&i2c_ir_led);
    if (ret < 0) {
        printf("ir led check id fail\n");
        return ret;
    }
#if 0 // Uncomment these codes after configuration management moving to component directory
    ret = device_config_get_value("setting","camera_paras","LED_NIR_BRIGHTNESS",&str);
    brightness = atoi(str);
    ir_led_config_brightness(brightness);
#endif
    return ret;
}

int ir_led_finalize()
{
	return 0;
}

int32_t led_check_enable(void)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, IRLED_REG_ENABLE, &data);
    if (ret < 0)
        return ret;
    
    return (data & 0x30); //According to DS, all mode bits in enable register should be cleard after flash timeout
}

int32_t led_read_flag(uint32_t reg)
{
	uint8_t data = 0;
	int32_t ret = iic_read_r8_d8(&i2c_ir_led, reg, &data);
	//printf("ir flag %02x \n",data);
	if (ret < 0)
		return ret;
	return data;
}

int ir_led_power_on(void)
{
	int32_t ret = 0;
    //ret = iic_write_array(pcsi_iic_3, (struct regval_list *)irled_start, ARRAY_SIZE(irled_start));
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)irled_start, ARRAY_SIZE(irled_start));
    if (ret < 0) {
        return ret;
    }
	//iic_read_array_r8_d8(&i2c_ir_led,(regval_list *)irled_start, ARRAY_SIZE(irled_start));
    return ret;
}

int ir_led_power_off(void)
{
	int32_t ret;
	ret = iic_write_array_r8_d8(&i2c_ir_led,  (regval_list *)irled_stop, ARRAY_SIZE(irled_stop));
	if (ret < 0) {
		printf("ir led close failed \n");

	}
	return ret;
}

int speckle_power_on(void)
{
    int32_t ret;
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));
    if (ret < 0) {
		printf("ir led close failed \n");

	}
	return iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));	
}


int32_t led_config_reglist(uint32_t addr, uint32_t data)
{
    uint32_t array_size = 0;
    int32_t ret;
    
    array_size = ARRAY_SIZE(irled_start);
    config_reglist(irled_start, array_size, addr, data);

    return 0;
}


uint32_t led_lookup_reg(uint32_t addr)
{
    uint32_t array_size = 0;
    
    array_size = ARRAY_SIZE(irled_start);
    return lookup_reg(irled_start, array_size, addr);
}

int32_t ir_led_config_brightness(uint32_t brightness)
{
    uint32_t brightness_reg = 0;
    
    brightness_reg = brightness*0xff/200;
    for(int32_t i = 0; i < sizeof(irled_start); i++) 
    {
        if ((0x3 == irled_start[i].addr)
            || (0x5 == irled_start[i].addr))
        {
            irled_start[i].data = brightness_reg;
        }
    }
}

int speckle_power_on_outside(void)
{
    return 0;
}

