#include <stdio.h>
#include "iic_function.h"
#include "lm3644.h"
#include "reg_deal.h"



i2c_dev_t i2c_ir_led;
led_config_t g_led_config = LED_NO_NEED_CONFIG;

regval_list irled_start[] = {    
#if defined(LED_STROBE)
	{0x01 ,0x21}, //LED1:speckle;LED2~LED4,ir
// #elif defined(LED_TORCH)
//     {0X01 ,0X11},    //LED1:torch
#else 
	{0x01 ,0x0d}, //LED1:speckle;LED2~LED4,ir
#endif    
	// {0x01 ,0x09}, //LED1:speckle;LED2~LED4,ir
	{0x02 ,0x01},
    {IR_CURRENT_REG, 0x3f}, //0x3f 500mA , 0x7f 1000mA
    {SPK_CURRENT_REG, 0x3f}, //0xcc 1600mA
// #if defined(LED_TORCH)
//     {0x05, 0x3f}, //0x3f 500mA , 0x7f 1000mA
//     {0x06, 0x3f}, //0xcc 1600mA
// #endif
    //{0x05 ,0x7f},
	//{0x07 ,0xcd},
    //{0x08 ,0x7f},
	//{0x09 ,0xcd},
    //{0x0a ,0x7f},
	{0x08 ,0x0f}, //0x9ac
};
const  regval_list irled_open[] = {
#if defined(LED_STROBE)
    {0x01 ,0x21},
// #elif defined(LED_TORCH)
//     {0X01 ,0X11},    //LED1:torch
#else
	{0x01 ,0x0d}, //LED1:speckle;LED2~LED4,ir
#endif    
};


const  regval_list irled_stop[] = {
#if defined(LED_STROBE)    
    {0x01 ,0x20},
// #elif defined(LED_TORCH)
//     {0X01 ,0X10},    //LED1:torch
#else    
	{0x01 ,0x00},
#endif    
};

const  regval_list speckle_start[] = {
#if defined(LED_STROBE)  
    {0x01 ,0x22},
// #elif defined(LED_TORCH)
//     {0X01 ,0X12},    //LED2:torch
#else    
	{0x01 ,0x0e}, //32
#endif    
    {SPK_CURRENT_REG, 0x3f}, //0x3f 500mA

#if defined(LED_TORCH)
    {0x06, 0x3f}, //0xcc 1600mA
#endif
	//{0x02 ,0xe1},
	//{0x03 ,0x7f},
	//{0x05 ,0x7f},
	//{0x0b ,0x1f},
};

const  regval_list speckle_start_outside[] = {
#if defined(LED_STROBE)  
    {0x01 ,0x22},
#elif defined(LED_TORCH)
    {0X01 ,0X12},    //LED2:torch
#else    
	{0x01 ,0x0e}, //32
#endif    
    {SPK_CURRENT_REG, 0xcc}, //0xcc 1600mA
	//{0x02 ,0xe1},
	//{0x03 ,0x7f},
	//{0x05 ,0x7f},
	//{0x0b ,0x1f},
};

const  regval_list speckle_torch_start[] = {
    {0x01 ,0x09},
    //{SPK_CURRENT_REG, 0x3f}, //0xcc 1600mA
	//{0x02 ,0xe1},
	//{0x03 ,0x7f},
	//{0x05 ,0x7f},
	//{0x0b ,0x1f},
};

static int32_t check_id_irled(i2c_dev_t *i2c)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(i2c, IRLED_REG_DEVICEID, &data);
    //printf("ir led id: %02x \n",data);
    if (ret < 0 || data != 0x04)
        return -1;

    return ret;
}

int ir_led_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
	int32_t ret;
#if 0
    if (camera1_init_flag == 0)
    {
        example_pin_camera_iic1_init();
    }

	i2c_ir_led.port = IIC_PORT_USI_11;
    i2c_ir_led.config.dev_addr = CK_IIC_SLAVE_ADDR_IRLED;
    PRINTF_LOG("ir led init \n");
    ret = hal_i2c_init(&i2c_ir_led);
    if (ret < 0) {
        printf("ir led i2c init failed \n");
        return ret;
    }
#endif
	memcpy(&i2c_ir_led, i2c, sizeof(i2c_dev_t));
	i2c_ir_led.config.dev_addr = dev_addr;
    ret = check_id_irled(&i2c_ir_led);
    if (ret < 0) {
        printf("ir led check id fail\n");
        return ret;
    }
    return ret;
}

int ir_led_finalize()
{
	return 0;
}

int32_t led_check_enable(void)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, IRLED_REG_ENABLE, &data);
    if (ret < 0)
        return ret;
    
    return (data & 0x0c); //According to DS, all mode bits in enable register should be cleard after flash timeout
}

int32_t led_read_flag(uint32_t reg)
{
	uint8_t data = 0;
	int32_t ret = iic_read_r8_d8(&i2c_ir_led, reg, &data);
	//printf("ir flag %02x \n",data);
	if (ret < 0)
		return ret;
	return data;
}

static int on_stat = 0;
int ir_led_power_on(void)
{
	int32_t ret = 0;
	
	if(on_stat){
		return 1;
	}
	
    //ret = iic_write_array(pcsi_iic_3, (struct regval_list *)irled_start, ARRAY_SIZE(irled_start));
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)irled_start, ARRAY_SIZE(irled_start));
    if (ret < 0) {
        return ret;
    }

	on_stat = 1;
	//iic_read_array_r8_d8(&i2c_ir_led,(regval_list *)irled_start, ARRAY_SIZE(irled_start));
    return ret;
}

int ir_led_power_off(void)
{
	int32_t ret;
	ret = iic_write_array_r8_d8(&i2c_ir_led,  (regval_list *)irled_stop, ARRAY_SIZE(irled_stop));
	if (ret < 0) {
		printf("ir led close failed \n");
	}

	on_stat = 0;
	return ret;
}

int speckle_power_on(void)
{
    int32_t ret; 
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));
    if (ret < 0) {
		printf("ir led close failed \n");
	}
	return ret;
}

int speckle_torch_power_on(void)
{
    int32_t ret; 
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_torch_start, ARRAY_SIZE(speckle_torch_start));
    if (ret < 0) {
		printf("ir led close failed \n");
	}
	return ret;
}

int speckle_power_on_outside(void)
{
    int32_t ret; 
    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start_outside, ARRAY_SIZE(speckle_start_outside));
    if (ret < 0) {
		printf("ir led close failed \n");
	}
	return ret;
}


int32_t led_config_reglist(uint32_t addr, uint32_t data)
{
    uint32_t array_size = 0;
    int32_t ret;
    
    array_size = ARRAY_SIZE(irled_start);
    config_reglist(irled_start, array_size, addr, data);

    return 0;
}


uint32_t led_lookup_reg(uint32_t addr)
{
    uint32_t array_size = 0;
    
    array_size = ARRAY_SIZE(irled_start);
    return lookup_reg(irled_start, array_size, addr);
}