#include <stdio.h>
#include "irp2835.h"
#include "pinmux.h"
#include "drv_gpio.h"
#include "drv_delay.h"
#include "iic_function.h"


#define USI0_NSS   PA23
gpio_pin_handle_t led_power_pin = NULL;

i2c_dev_t i2c_ir_led;
led_config_t g_led_config = LED_NO_NEED_CONFIG;

int ir_led_init(void)
{
    int ret;

	drv_pinmux_usi_nss_ctrl_type_config(USI_INDEX_0, GPIO_CTRL);
    led_power_pin = csi_gpio_pin_initialize(USI0_NSS, NULL);
    if(led_power_pin == NULL) {
        printf("csi_gpio_pin_initialize() error.\r\n");
        return -1;
    }
    ret = csi_gpio_pin_config_direction(led_power_pin, GPIO_DIRECTION_OUTPUT);
    //csi_gpio_pin_uninitialize(led_power_pin);

    return 0;
}

int ir_led_power_on(void)
{
    int ret;

    ret |= csi_gpio_pin_write(led_power_pin, 1);
    if (ret) {
        printf("cfg or write pin error.\r\n");
        return -1;        
    }
    
    return 0;
}

int ir_led_power_off(void)
{
    int ret;

    ret |= csi_gpio_pin_write(led_power_pin, 0);
    if (ret) {
        printf("cfg or write pin error.\r\n");
        return -1;        
    }
    
    return 0;
}

int ir_led_finalize()
{
	return 0;
}

int32_t led_check_enable(void)
{
    return 0;
}

int32_t led_read_flag(uint32_t reg)
{
    return 0;
}

int speckle_power_on(void)
{
    return 0;
}

int speckle_power_on_outside(void)
{
    return 0;
}


int32_t led_config_reglist(uint32_t addr, uint32_t data)
{
    return 0;
}


uint32_t led_lookup_reg(uint32_t addr)
{
    return 0;
}

