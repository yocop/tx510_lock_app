/**
 * File Name: aw36423.c
 *
 * Copyright (C) 2020 Fortsense Corporation. <http://www.fortsense.com>&gt
 *
 * Author:  Linsh 
 *
 * Date: 2021/04/21
 *
 * Project: tx510_bf2253
 *
 * Description:
 *
 * HISTORY
 *
 **/
/**
* @file    aw36423.c
*
* @brief   
*
*****************************************************************************/
/******************************************************************************
* 0.#include declarations
*****************************************************************************/

/******************************************************************************
* 1.local macro definitions
*****************************************************************************/

/******************************************************************************
* 2.local type definitions
*****************************************************************************/

/******************************************************************************
* 3.local variable declarations
*****************************************************************************/

/******************************************************************************
* 4.local function prototypes
*****************************************************************************/

/******************************************************************************
* 5.function define
*****************************************************************************/
#include <stdio.h>
#include "iic_function.h"
#include "aw36423.h"
#include "reg_deal.h"

i2c_dev_t i2c_ir_led;

static int32_t check_id_irled(i2c_dev_t *i2c)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(i2c, FS_AW36423_CHIP_ID_REG, &data);
    printf("ir led id: %02x \n",data);
    if (ret < 0 || data != 0x17)
    {
        return -1;
    }
    return ret;
}

int ir_led_init(i2c_dev_t *i2c, uint32_t dev_addr)
{
    int32_t ret;
    memcpy(&i2c_ir_led, i2c, sizeof(i2c_dev_t));
    i2c_ir_led.config.dev_addr = dev_addr;
    ret = check_id_irled(&i2c_ir_led);
    if (ret < 0)
    {
        printf("ir led check id fail\n");
        return ret;
    }
    return ret;
}

int32_t led_check_enable(void)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, FS_AW36423_ENABLE_REG, &data);
    if (ret < 0)
    {
        return ret;
    }
    
    return (data & 0x0c); //According to DS, all mode bits in enable register should be cleard after flash timeout
}

int32_t led_read_flag(uint32_t reg)
{
    uint8_t data = 0;
    int32_t ret = iic_read_r8_d8(&i2c_ir_led, reg, &data);
    //printf("ir flag %02x \n",data);
    if (ret < 0)
    {
        return ret;
    }
    
    return data;
}

int fs_aw36423_ctl(led_mode_t mode, int current_ma, uint8_t on)
{
    int ret = 0;
    unsigned char reg = 0;
    unsigned char buf = 0x00;
    //printf("%s enter mode = %d on=%d\n", __func__,mode, on);
    //ret = led_read_flag(FS_AW36423_FLAGS_REG);
    if (on) {
        if (mode == LED_TORCH_MODE) {
            reg = 0x22;
            ret = iic_write_r8_d8(&i2c_ir_led, FS_AW36423_LED1_TORCH_BRIGHTNESS_REG, current_ma);

            if (ret < 0) {
                printf("%s set failed ret = %d\n", __func__, ret);
                return ret;
            }

            buf = 0xFF;//0x00
            ret = iic_write_r8_d8(&i2c_ir_led, FS_AW36423_FLASH_TORCH_TIMING_REG, buf);

            if (ret < 0) {
                printf("%s set failed ret = %d\n", __func__, ret);
                return ret;
            }
        }
        else if (mode == LED_FLASH_MODE) {
            reg = 0x71;     //led 1 enable
            ret =  iic_write_r8_d8(&i2c_ir_led, FS_AW36423_LED1_FLASH_BRIGHTNESS_REG, current_ma); 
            buf = 0xFF;
            ret = iic_write_r8_d8(&i2c_ir_led, FS_AW36423_FLASH_TORCH_TIMING_REG, buf);

            if (ret < 0) {
                printf("%s set failed ret = %d\n", __func__, ret);
                return ret;
            }
        }
        else if (mode == LED_IR_MODE) {
            reg = 0x13;
            ret =  iic_write_r8_d8(&i2c_ir_led, FS_AW36423_LED1_FLASH_BRIGHTNESS_REG, current_ma); 
            buf = 0xFF;//0x00
            ret = iic_write_r8_d8(&i2c_ir_led, FS_AW36423_FLASH_TORCH_TIMING_REG, buf);

            if (ret < 0) {
                printf("%s set failed ret = %d\n", __func__, ret);
                return ret;
            }
        }
    }
    else {
        reg = 0x20;
    }

    ret = iic_write_r8_d8(&i2c_ir_led, FS_AW36423_ENABLE_REG, reg);
    if (ret < 0) {
        printf("ir led open error \n");
    }
    return ret;
}

int ir_led_power_on(void)
{
    int32_t ret = 0;

    //fs_aw36423_ctl(LED_FLASH_MODE,0x7f, 1);
    fs_aw36423_ctl(LED_FLASH_MODE,0x4b, 1);

    if (ret < 0) {
        printf("ir led open error \n");
    }
    return ret;
}

int ir_led_power_off(void)
{
    int32_t ret;

    ret = fs_aw36423_ctl(LED_FLASH_MODE,0x4b, 0);
    if (ret < 0) {
        printf("ir led off error \n");
    }
    return ret;
}

int speckle_power_on(void)
{
    int32_t ret; 
//    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start, ARRAY_SIZE(speckle_start));
//    if (ret < 0) {
//		printf("ir led close failed \n");
//	}
	return ret;
}

int speckle_power_on_outside(void)
{
    int32_t ret; 
//    ret = iic_write_array_r8_d8(&i2c_ir_led, (regval_list *)speckle_start_outside, ARRAY_SIZE(speckle_start_outside));
//    if (ret < 0) {
//		printf("ir led close failed \n");
//	}
	return ret;
}

int32_t led_config_reglist(uint32_t addr, uint32_t data)
{
    uint32_t array_size = 0;
    int32_t ret;
    
//    array_size = ARRAY_SIZE(irled_start);
//    config_reglist(irled_start, array_size, addr, data);

    return 0;
}

uint32_t led_lookup_reg(uint32_t addr)
{
//    uint32_t array_size = 0;
//    
//    array_size = ARRAY_SIZE(irled_start);
//    return lookup_reg(irled_start, array_size, addr);
}
