#ifndef H264_VPU_H
#define H264_VPU_H

typedef enum {
    H264_TYPE_IFRAME,
    H264_TYPE_PFRAME,
    H264_TYPE_SPS,
} h264_frame_type_e;

int h264_vpu_init(void);
int h264_vpu_encode_start(void);
int h264_vpu_encode_stop(void);
int h264_vpu_deinit(void);
int h264_vpu_get_frame(h264_frame_type_e *type, uint8_t *data, int max_size, long long *time_stamp);
int h264_vpu_seek_iframe(void);
// void h264_vpu_get_frame_done(void);
int h264_vpu_available_frame(void);

#endif