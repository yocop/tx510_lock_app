#include <string.h>
#include <aos/cli.h>
#include "aos/kernel.h"
#include "time.h"
#include <stdio.h>
#include "linkvisual_client.h"
#include "cx_facerecog.h"

void cmd_fr_test(char *wbuf, int wbuf_len, int argc, char **argv)
{
    if (argc < 2) {
        printf("fr del/clear/list\n");
        return;
    }

    if(strcmp(argv[1], "del") == 0) {
        if (argc < 3) {
            printf("fr del <id>\n");
            return;
        }

        int id = atoi(argv[2]);
        cx_facerecog_user_delete(CX_USER_DEL_SINGLE, id);

    } else if(strcmp(argv[1], "list") == 0) {
        int user_ids[100];
        int cnt = cx_facerecog_get_all_user_id(100, user_ids);

        printf("user ids:");
        for (int i = 0; i < cnt; i++) {
            printf(" %d", user_ids[i]);
        }
        printf("\n");
    } else if(strcmp(argv[1], "clear") == 0) {
        cx_facerecog_user_delete(CX_USER_DEL_ALL, 0);
    }
}

void cli_reg_cmd_fr(void)
{
    static const struct cli_command cli_fr = {"fr", "fr test", cmd_fr_test};
    aos_cli_register_command(&cli_fr);
}

/*
    fr del <id>                     删除<id>用户
    fr clear                        删除所有用户
    fr list                         list所有用户id
*/