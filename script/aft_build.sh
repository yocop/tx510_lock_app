#!/bin/sh
BASE_PWD=`pwd`
MK_GENERATED_PATH=generated
echo $PATH_TX510_CPU0_EVB2

BOARD_PATH=$PATH_TX510_CPU0_EVB2

echo "[INFO] Generated output files ..."

rm -fr $MK_GENERATED_PATH
mkdir -p $MK_GENERATED_PATH/data/

OBJCOPY=csky-elf-noneabiv2-objcopy

#output yoc.bin
ELF_NAME=`ls Obj/*.elf`
$OBJCOPY -O binary $ELF_NAME yoc.bin
cp yoc.bin generated/data/prim
# ./script/csky_pack_win/Image.exe -i ALGO_V1.4_D01W13_210202.elf -c script/csky_pack_win/prim.ini -o generated/data/prim
# rm ck805_ddr.bin ck805_dtcm.bin
	
#Prepare bin

BOOTIMG_PATH="${BOARD_PATH}/bootimgs"
cp ${BOOTIMG_PATH}/boot $MK_GENERATED_PATH/data/boot
cp ${BOOTIMG_PATH}/bparam $MK_GENERATED_PATH/data/bparam
cp ${BOOTIMG_PATH}/805dtcm $MK_GENERATED_PATH/data/805dtcm
cp ${BOOTIMG_PATH}/805ddr $MK_GENERATED_PATH/data/805ddr
cp ${BOOTIMG_PATH}/weight $MK_GENERATED_PATH/data/weight
# cp ${BOOTIMG_PATH}/m0flash.bin $MK_GENERATED_PATH/data/m0flash
cp "$BOARD_PATH/configs/config.yaml" $MK_GENERATED_PATH/data/

#Create Image
PRODUCT_BIN="${BOARD_PATH}/configs/product.exe"
${PRODUCT_BIN} image $MK_GENERATED_PATH/images.zip -i $MK_GENERATED_PATH/data
${PRODUCT_BIN} image $MK_GENERATED_PATH/images.zip -l -b -p
${PRODUCT_BIN} image $MK_GENERATED_PATH/images.zip -e $MK_GENERATED_PATH -x 
# rm -fr $MK_GENERATED_PATH/data

