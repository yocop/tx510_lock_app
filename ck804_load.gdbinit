target remote 127.0.0.1:1025
#reset
lo

#set *0xfff200d0 &= 0xf0ffffff
#set *0xfff200d0 |= (1<<26)
#set *0xfff200d0 |= (1<<27)
#set * 0xfff200d0 = 0x0c400c40


#b vector.s:48

### Below lines are the cmds in test cases ###
# For DPU test
#   restore tests/driver/src/test_dpu/test_800_480_csky.rgba8888 binary 0xA00000
#   restore tests/driver/src/test_dpu/test_800_480_csky_1.rgba8888 binary 0xC00000
#   restore tests/driver/src/test_dpu/test_800_480_csky.rgblut8 binary 0xE00000
#
# For VPU test
#   restore tests/driver/src/test_vpu/csky_ad_320x240_yuv420p_10frames.yuv binary 0x2000000
#   Find the log such as "dump binary memory h264.es ..." and execute it in gdb

#reset 805
#set *0xfff21004 = 0x1bff7
b trap
